FROM node:latest
WORKDIR /app
COPY package.json .
RUN npm install --force
COPY . .
RUN npm run build
COPY . .
EXPOSE 4120
CMD [ "npm", "start" ]
