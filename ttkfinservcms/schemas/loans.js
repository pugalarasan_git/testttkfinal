import { defineField, defineType } from 'sanity'

export default defineType({
    name: 'loans',
    title: 'Bank Loans',
    type: 'document',
    fields: [
        defineField({
            name: 'name',
            title: 'Name',
            type: 'string',
        }),
        defineField({
            name: 'loanDetails',
            title: 'Loan Details',
            type: 'array',
            of: [
                {
                    type: 'object',
                    fields: [
                        {
                            name: 'bankName',
                            title: 'Bank Name',
                            type: 'string'
                        },
                        {
                            name: 'interestRate',
                            title: 'Loan Percentage',
                            type: 'string'
                        }
                    ]
                }
            ]
        }),
        defineField({
            name: 'NBFCloanDetails',
            title: 'NBFC Loan Details',
            type: 'array',
            of: [
                {
                    type: 'object',
                    fields: [
                        {
                            name: 'bankName',
                            title: 'Bank Name',
                            type: 'string'
                        },
                        {
                            name: 'interestRate',
                            title: 'Loan Percentage',
                            type: 'string'
                        }
                    ]
                }
            ]
        })
    ],
})
