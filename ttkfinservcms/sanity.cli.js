import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'fnw5rzor',
    dataset: 'production'
  }
})
