import React from 'react'
import { Grid, Paper, Typography,Button} from '@mui/material';
import { ElectricWaveDown, ElectricWaveUp, Threed,WhatsApp } from "../images/Vector";
import { TextField, InputAdornment } from '@mui/material';
import { AccountCircle } from '@mui/icons-material';

const Whatsapp = () => {
  return (
    <>
       <Grid container spacing={3} bgcolor="#0A1535" color="white">
      <Grid item xs={6} sm={6}>
        <ElectricWaveUp/>
        
          <Typography variant="h4">Contact us Through WhatsApp</Typography>
         
          <TextField
            label="Username"
        
            fullWidth
            InputProps={{
            startAdornment: (
          <InputAdornment position="start">
            <WhatsApp />
          </InputAdornment>
                ),
            }}
           />
         <Button  sx={{color:"white", borderColor:"white", padding:"12px"}}>
            Outlined
            </Button>
            <ElectricWaveDown/>

       
      </Grid>
      <Grid item xs={6} sm={6}>
      <Threed />
         
      </Grid>
    </Grid>
    </>
  )
}

export default Whatsapp