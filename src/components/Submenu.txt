import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';

import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const pages = ['Products', 'Pricing', 'Blog'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

function ResponsiveAppBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
 
      <div>
  
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Select
            value={age}
            onChange={handleChange}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
       
        <FormHelperText>Without label</FormHelperText>
      </FormControl>
    </div>
  );
  }
export default ResponsiveAppBar;


































import React, { useState } from "react";
import Carousel from "better-react-carousel";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Box } from "@mui/system";
import { FaRegCalendarAlt } from "react-icons/fa";
import Typography from '@mui/material/Typography'
import { Button } from "@mui/material";

const CustomCarousel = (props) => {
  const [activeDiv, setActiveDiv] = useState(11);

  function scrollCarousel(direction) {
    // alert("scrolled...")
    const carousel = document.getElementById("carousel");
    if (direction < 0) {
      carousel.scrollBy(-300, 0);
    } else {
      carousel.scrollBy(300, 0);
    }
  }

  const cardData = [
    {
      id: 11,
      title: "Personal Loan",
      content:
        "Whether, you want to meet lifestyle goals or any personal exigency, If requirement is immediate, so should be the loan options. We have unique personal loan products to match your perfect requirement.  Customized loan offering, Higher loan amount and tenure, Flexible repayment plans",
      button: "Learn More About Xvigil",
    },
    {
      id: 22,
      title: "EMI Free Loan",
      content:
        "EMI Free Loan Even after taking a personal loan, one wants the least impact on the salary outgo. Our feature rich customized personal loan helps lower your monthly cash outflow by 40%. Enjoy interest only payments on a monthly basis & bullet payments towards principal every 3/6 months.  Lower monthly installments,   Higher loan amount and tenure,  Free part-payments",
      button: "Learn More About Xvigil",
    },
    {
      id: 33,
      title: "Afterpay Merchant",
      content:
        "A product specially designed to meet the needs of businesses. A convenient financing option that extends a short term credit line of up to 5Lacs for tenures from 7 to 30 days. Easy onboarding, Higher Credit Line   ",
      button: "Learn More About Xvigil",
    },
    {
      id: 44,
      title: "e-Bike Loan",
      content:
        "EMI Free Loan  Now explore from a range of electric two wheelers and ride home one of your choice! Pay in easy EMIs across a tenure as long as 36 months. LoanTap’s electric two-wheeler loan helps fund e bikes in the range of INR 20,000 to INR 2,00,000.  Quick Sanction,    Disbursal within 2 hrs*",
      button: "Learn More About Xvigil",
    },
    {
      id: 55,
      title: "Loan Against Securities",
      content:
        "EMI Free Loan  Loan Against Securities  Loans against securities are structured to enable individuals or businesses to obtain funds by utilizing their current investment portfolio, including stocks, bonds, or mutual funds, as a form of collateral.  Interest rate @ 9% p.a. only.  Hassle-free online process,  Quick disbursals",
      button: "Learn More About Xvigil",
    },
  ];

  const companies = [
    "Personal Loan",
    "EMI Free Loan",
    "Afterpay Merchant",
    "e-Bike Loan",
    "Loan Against Securities",
  ];
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  function scrollToDiv(divId) {
    setActiveDiv(divId);
    const divToScroll = document.getElementById(divId);
    const scrollContainer = document.getElementById("carousel");
    const scrollTo = divToScroll.offsetLeft;

    scrollContainer.scrollTo({
      left: scrollTo,
      behavior: "smooth",
    });
  }
  return (
    <div className="no-scrollbar h-fit flex flex-col mb-24  py-10 max-h-fit  shadow-[0_10px_60px_-15px_rgba(0,0,0,0.3)] mx-auto rounded-2xl border-purple-600 w-[95vw]">
      <div className="hidden lg:flex w-full mb-5 relative pb-14 md:pb-0 items-center  h-fit md:justify-center justify-start gap-7">
        <div className="flex  h-fit px-10  max-w-full justify-evenly mx-auto flex-row md:gap-5 items-stretch ">
          {cardData.map((data) => (
            <button
              className={` ${
                activeDiv == data.id
                  ? "bg-white text-[#593B8A] "
                  : "bg-[#593B8A] text-white "
              }   text-lg rounded-xl font-medium border-2 border-[#593B8A] flex-1`}
              onClick={() => scrollToDiv(data.id.toString())}
            >
              {data.title}
            </button>
          ))}
        </div>
      </div>
      <div
        id="carousel"
        className="flex  no-scrollbar snap-mandatory snap-x scroll scroll-smooth  flex-row md:h-3/4  overflow-scroll w-full "
      >
        {cardData.map((data) => (
          <div
            id={data.id}
            className="md:h-full  px-5 py-4 snap-start min-w-full flex flex-col items-center justify-center"
          >
            <button
              className={` ${
                activeDiv == data.id ? "bg-white md:bg-[#593B8A] md:text-[#593B8A]" : "md:bg-[#593B8A]"
              } lg:hidden rounded-xl border w-full mb-4 md:py-5`}
              onClick={() => scrollToDiv(data.id.toString())}
            >
              {data.title}
            </button>
            <div className="rounded-2xl overflow-hidden bg-gray-500/10 flex w-full flex-col md:flex-row h-[50vh] items-center justify-center pb-5 md:py-5 text-left">
              <div
                id="image"
                className="w-full h-full overflow-hidden lg:w-1/4 bg-gray-500/60 md:rounded-3xl mx-3 "
              >
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0yJA33ouRt8JW93UhEjP-W2-1YPS0LDIHIg&usqp=CAU"
                  alt="loan image"
                  className="h-full object-cover w-full"
                />
              </div>
              <div className="p-0 px-5 md:p-10 lg:w-3/4 text-left">
               <Typography variant="body1"  color='#6e7191' fontSize='20px'> {data.content}</Typography>
                <div className="flex flex-row  justify-start items-center mt-5 gap-5">
                <Button variant="contained" sx={{bgcolor:"#593B8A",color:'white',padding:'12px', "&:hover": { bgcolor: "white", color: "#5C31A6" },fontWeight:'bold'}}>  Apply Now</Button>
                <Button variant="outlined" sx={{color:"#593B8A",padding:'12px',fontWeight:'bold'}} >  Explore Product</Button>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>

      <div className="flex w-1/4 mt-10 gap-1 md:gap-10 mx-auto max-w-full   flex-row justify-between items-center">
        {cardData.map((data) => (
          <button
            key={data.id}
            className={` ${
              activeDiv == data.id
                ? "bg-white border-2 border-[#593B8A]"
                : " bg-[#593B8A] "
            } m-0 p-0 flex-1 h-2`}
            onClick={() => scrollToDiv(data.id.toString())}
          ></button>
        ))}
      </div>
    </div>
  );
};

export default CustomCarousel;




