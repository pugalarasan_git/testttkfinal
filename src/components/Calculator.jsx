import React, { useState } from 'react';
import {
  TextField,
  Button,
  Typography,
  Container,
  Grid,
  Paper,
  Slider,
} from '@mui/material';

const LoanEMICalculator = () => {
  const [loanAmount, setLoanAmount] = useState(100000); // Default value
  const [interestRate, setInterestRate] = useState(5); // Default value
  const [loanTenure, setLoanTenure] = useState(12); // Default value
  const [emi, setEmi] = useState('');

  const calculateEMI = () => {
    // Calculate EMI logic here based on loanAmount, interestRate, and loanTenure
    const principal = parseFloat(loanAmount);
    const rate = parseFloat(interestRate) / 100 / 12; // Monthly interest rate
    const tenure = parseInt(loanTenure);
    const emiAmount = (
      (principal * rate * Math.pow(1 + rate, tenure)) /
      (Math.pow(1 + rate, tenure) - 1)
    ).toFixed(2);

    setEmi(emiAmount);
  };

  return (
    <Container maxWidth="sm">
      <Paper elevation={3} sx={{ padding: 3 }}>
        <Typography variant="h4" gutterBottom>
          Loan EMI Calculator
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography id="loan-amount-slider" gutterBottom>
              Loan Amount: {loanAmount} INR
            </Typography>
            <Slider
              value={loanAmount}
              onChange={(e, newValue) => setLoanAmount(newValue)}
              min={10000}
              max={1000000}
              step={10000}
              aria-labelledby="loan-amount-slider"
            />
          </Grid>
          <Grid item xs={12}>
            <Typography id="interest-rate-slider" gutterBottom>
              Interest Rate: {interestRate}%
            </Typography>
            <Slider
              value={interestRate}
              onChange={(e, newValue) => setInterestRate(newValue)}
              min={1}
              max={20}
              step={0.25}
              aria-labelledby="interest-rate-slider"
            />
          </Grid>
          <Grid item xs={12}>
            <Typography id="loan-tenure-slider" gutterBottom>
              Loan Tenure: {loanTenure} Months
            </Typography>
            <Slider
              value={loanTenure}
              onChange={(e, newValue) => setLoanTenure(newValue)}
              min={6}
              max={60}
              step={6}
              aria-labelledby="loan-tenure-slider"
            />
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" onClick={calculateEMI}>
              Calculate EMI
            </Button>
          </Grid>
          {emi && (
            <Grid item xs={12}>
              <Typography variant="h6">
                EMI Amount: {emi} INR
              </Typography>
            </Grid>
          )}
        </Grid>
      </Paper>
    </Container>
  );
};

export default LoanEMICalculator;
