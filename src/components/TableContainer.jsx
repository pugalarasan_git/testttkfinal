import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

function createData(type, charge) {
  return { type, charge };
}

const rows = [
  createData("Charge Type", "Rate of Interest"),
  createData("Charge", "10%"),
];

export default function AccessibleTable() {
  return (
    <div className="w-3/4 lg:w-1/2 mt-10">
      <TableContainer component={Paper} sx={{ bgcolor: "#faf5ff" }}>
        <Table sx={{ minWidth: 650 }} aria-label="caption table">
          <TableHead>
            <TableRow>
              <TableCell
                sx={{
                  color: "#5c31a6",
                  fontWeight: "bolder",
                  fontSize: { xs: 10, md: 30 },
                  
                }}
                // align="left"
              >
                Type
              </TableCell>
              <TableCell
                sx={{
                  color: "#5c31a6",
                  fontWeight: "bolder",
                  fontSize: { xs: 10, md: 30 },
                }}
                align="left"
              >
                Charge
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell
                  sx={{
                    color: "#5c31a6",
                    fontWeight: "bolder",
                    fontSize: { xs: 10, md: 30 },
                  }}
                  component="th"
                  scope="row"
                >
                  {row.type}
                </TableCell>
                <TableCell
                  sx={{
                    color: "#5c31a6",
                    fontWeight: "bolder",
                    fontSize: { xs: 10, md: 30 },
                  }}
                  component="th"
                  scope="row"
                >
                  {row.charge}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
