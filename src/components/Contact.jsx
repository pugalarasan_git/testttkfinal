import React, { useEffect, useState } from "react";
import {
  TextField,
  Button,
  Typography,
  Container,
  Grid,
  useMediaQuery,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import axios from "axios";
import { FaEnvelope, FaFacebook, FaMobile, FaPhone } from "react-icons/fa";
import { IconButton } from "@mui/material";
// import { google } from "googleapis";

const ContactForm = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phoneNumber: "",
    message: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    // You can handle form submission logic here, e.g., sending data to a server or API.
    console.log("Form submitted:", formData);
    try {
      let response = await axios.post(
        "https://en1tcit0bkeil.x.pipedream.net/",
        formData
      );
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <div
      id="ContactForm"
      className="py-24 md:my-16 md:pl-16 flex flex-col  lg:gap-10 md:flex-row  items-center justify-center"
    >
      <div className="h-full  gap-10 px-10 md:px-0 md:w-1/2 justify-start  lg:mx-10 rounded-2xl">
        <Typography
          color="#5C31A6"
          fontSize="48px"
          fontWeight="bold"
          sx={{ fontSize: { xs: "34px", md: "40px" } }}
        >
          Contact Us.
        </Typography>
        <Typography
          color="#5C31A6"
          fontSize="40px"
          fontWeight="bold"
          sx={{ fontSize: { xs: "26px", md: "30px" } }}
        >
          Let's grow your net worth together
        </Typography>
        <Typography
          variant="body1"
          fontSize="20px"
          color="#6e7191"
          mt={2}
          sx={{ fontSize: { xs: "16px", md: "20px" } }}
        >
          We offer personalized financial guidance for families and firms. We
          understand that each client has unique needs, and we tailor our
          services accordingly.
        </Typography>

        <Typography
          variant="body1"
          fontSize="20px"
          color="#6e7191"
          mt={2}
          sx={{ fontSize: { xs: "16px", md: "20px" } }}
        >
          Contact us today to learn more. We would be happy to answer any
          questions you have and help you get started on your financial journey.
        </Typography>

        <Grid container>
          <Grid item xs={12} md={12} display="flex" mt={2}>
            <IconButton sx={{ p: 0, color: "#5C31A6" }}>
              {" "}
              <FaPhone />{" "}
            </IconButton>
            <Typography
              variant="body1"
              fontSize="20px"
              color="#6e7191"
              ml={2}
              sx={{ fontSize: { xs: "16px", md: "20px" } }}
            >
              Call: +91 76039 31290
            </Typography>
          </Grid>
        </Grid>

        <Grid item xs={12} md={12} display="flex" mt={2}>
          <IconButton sx={{ p: 0, color: "#5C31A6" }}>
            {" "}
            <FaEnvelope />{" "}
          </IconButton>

          <Typography
            variant="body1"
            fontSize="20px"
            color="#6e7191"
            ml={2}
            sx={{ fontSize: { xs: "16px", md: "20px" } }}
          >
            Email: loans@ttkfinserv.com
          </Typography>
        </Grid>
      </div>
      <div className="md:mx-32 px-10 md:px-0 mt-5 md:mt-0 md:w-1/2">
        <form onSubmit={handleSubmit}>
          <Grid container spacing={isSmallScreen ? 1 : 2}>
            <Grid item xs={12}>
              <TextField
                label="Name"
                name="name"
                fullWidth
                value={formData.name}
                onChange={handleChange}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Email"
                name="email"
                fullWidth
                type="email"
                value={formData.email}
                onChange={handleChange}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Phone Number"
                name="phoneNumber"
                fullWidth
                type="tel"
                value={formData.phoneNumber}
                onChange={handleChange}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Message"
                name="message"
                fullWidth
                multiline
                rows={4}
                value={formData.message}
                onChange={handleChange}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                type="submit"
                sx={{
                  color: "white",
                  bgcolor: "#5C31A6",
                  px: 5,
                  py: 2,
                  "&:hover": { bgcolor: "#8440e6" },
                }}
                fullWidth
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </div>
  );
};

export default ContactForm;
