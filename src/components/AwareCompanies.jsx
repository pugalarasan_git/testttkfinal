import React from 'react';
import { Grid, Paper,Container, Typography } from '@mui/material';
import { Cake } from '../images/Vector';


const cakes = Array.from({ length: 10 });

function FiveColumnLayout() {
  return (
    <>
    <Container>

        <Typography variant="h4" textAlign="center" color="initial" mb={8}>Trusted by leading security-aware companies organizations across the world</Typography>

        
        <Grid container spacing={12}>
      {cakes.map((_, index) => (
        <Grid key={index}  item  xs={6}   md={4} lg={4}   xl={3}
             sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Cake />
        </Grid>
      ))}
    </Grid>


  
    </Container>
    </>
  );
}

export default FiveColumnLayout;
