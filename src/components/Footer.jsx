import React from 'react';
import { Grid, Typography, Avatar, IconButton,  Box  } from '@mui/material';

import Container from '@mui/material/Container';
import { FaFacebook, FaInstagram, FaLinkedin, FaTwitter } from 'react-icons/fa';
import darkLogo from "../images/skandaDark.svg"



const FooterEnd = () => {
  return (
   
    <Container>
    <Grid container  mt={5}  spacing={2} >
      <Grid  item xs={12} sm={4} xl={4} >
      <Box >
        <img src={darkLogo} alt=""  className='w-32 h-14'/>
        </Box>
      </Grid>
      <Grid item xs={12} sm={4}>
      <Box  >
        <Typography textAlign="center" color='#6e7191' fontSize='18px'>A contextual AI company that predicts Cyber Threats.</Typography>
        </Box>
      </Grid>
      <Grid item xs={12} sm={4}>
      <Box  >
        <IconButton>
          <Box color='#4a3aff'>
          <FaFacebook />
          </Box>
        </IconButton>
        <IconButton>
        <Box color='#4a3aff'>
          <FaTwitter />
          </Box>
        </IconButton>
        <IconButton>
        <Box color='#4a3aff'>
          <FaInstagram />
          </Box>
        </IconButton>
        <IconButton>
        <Box color='#4a3aff'>
          <FaLinkedin />
          </Box>
        </IconButton>
        </Box>
      </Grid>
    </Grid>


    </Container>
 
  );
};

export default FooterEnd;
