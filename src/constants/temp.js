

export const data = [

    // personal loan
    {
        "name": "Personal Loan",
        "path": "personalLoan",
        "imageLink": "https://www.idfcfirstbank.com/content/dam/idfcfirstbank/images/blog/personal-loan/personal-loan-eligibility-criteria-717x404.jpg",
        "titleTagline": "Get Quick and Flexible Personal Loans",
        "description": "Personal Loan provides better instant loan solutions to fund your marriage, medical, and travel expenses. There is more to it; you can now avail of an online personal loan for home renovation, education, and car purchases. Read on to know more about personal loan lenders, offers, interest rates and EMI options.",
        "add": [
            "Quick approval process with minimal documentation",
            "Flexible loan amount and tenure options",
            "No collateral or security required"
        ],
        "benefits": [
            "Financial Flexibility: Use the loan amount for any purpose you need",
            "No Collateral: Unsecured loans don't require any collateral or security",
            "Quick Approval: Fast processing and approval within a few days",
            "Flexible Repayment: Choose a tenure that suits your repayment capacity",
            "Emergency Funding: Address unexpected financial needs with ease",
            "Low Minimum Income: Loans available for individuals with various income levels"
        ],
        "loanHighlights": {
            "InterestRate": "Competitive interest rates starting from 10.99% per annum (p.a.)",
            "LoanAmount": "Borrow amounts ranging from ₹50,000 to ₹20,00,000",
            "Tenure": "Repayment tenure options from 1 to 5 years",
            "ProcessingFees": "Processing fees applicable as per the lender's policy"
        },
        "eligibility": [
            "Age: 21 to 60 years (may vary by lender)",
            "Nationality: Indian Resident",
            "Income: Minimum monthly income of ₹15,000 (may vary by lender)",
            "Employment: Salaried or Self-employed"
        ],
        "documents": [
            "Proof of Identity: Aadhaar Card, Passport, Voter ID, or Driver's License",
            "Proof of Address: Aadhaar Card, Passport, Utility Bill, or Rental Agreement",
            "Income Proof: Salary Slips, Bank Statements, or ITR for self-employed",
            "Photographs: Passport-sized photographs",
            "Note: Additional documents may be required based on the lender's criteria."
        ],
        "feesAndCharges": [
            { "event": "Processing Fee", "charges": "Usually around 1% to 3% of the loan amount" },
            { "event": "Prepayment Charges", "charges": "Varies by lender; some lenders allow prepayment without charges" },
            { "event": "Late Payment Penalty", "charges": "2% to 4% of the overdue amount" },
            { "event": "EMI Bounce Charges", "charges": "Around ₹300 to ₹600 per bounce" }
        ],
        "repaymentOptions": {
            "description": "Personal loan repayment involves paying back the borrowed.",
            "options": [
                { "option": "Equated Monthly Installments (EMIs)", "optionDescription": "Fixed monthly payments comprising both principal and interest" },
                { "option": "Part Prepayment", "optionDescription": "Pay a lump sum to reduce the outstanding principal" },
                { "option": "Foreclosure", "optionDescription": "Close the loan by paying the entire outstanding amount" }
            ]
        },
        "tipsToGetLoan": "https://www.youtube.com/embed/your-video-id",
        "dosAndDonts": {
            "Dos": [
                "Check Your Credit Score: Ensure your credit score is in good shape",
                "Compare Lenders: Research and compare offers from multiple lenders",
                "Know Your EMI Affordability: Calculate how much EMI you can comfortably pay",
                "Read the Fine Print: Understand terms, charges, and repayment options",
                "Maintain a Stable Job Profile: Lenders prefer borrowers with a stable income",
                "Keep Documents Ready: Ensure you have all required documents"
            ],
            "Donts": [
                "Don't Borrow More Than Needed: Borrow only what you can repay",
                "Avoid Multiple Applications: Too many loan applications can impact your credit score",
                "Don't Hide Information: Provide accurate and complete information",
                "Avoid Missed Payments: Timely EMI payments are crucial for a good credit history",
                "Don't Fall for Unnecessary Add-ons: Avoid unnecessary insurance or add-ons",
                "Don't Rush Decision: Take your time to make an informed borrowing decision"
            ]
        },
        collateral: ["No collateral"],
        typesOfLoan: [
            "Instant Personal Loans: Fund a holiday or finance your child’s education. Now with a collateral-free loan. The easiest way to repurpose your finances is with an instant personal loan.",
            "Short-term Personal Loan: A short-term personal loan allows you to get money quickly without having to go through a lengthy application process. These loans usually have lower interest rates than other types.",
            "Travel Loan: Banks offer Personal loans for travel & are quick to disburse them. You can now book & plan your dream vacation in one go!",
            "Marriage Loan: A personal loan for marriage covers end-to-end expenses, including venue, catering, and other arrangements.",
            "Medical Loan: You can now get instant approval on a personal loan for a medical emergency. Cover medical bills & treatments during emergencies.",
            "Personal Loan for Car Purchases: Car loans have the best advantages over other forms of financing. With competitive interest rates & longer repayment terms, you can now get a car or refurbish one to your liking.",
            "Personal Loan for Education: Close your tuition fees on time or admit your child to the desired school; education loans are now available at recommended interest rates."
        ],
        applySteps: [

            "Step 1 Visit - TTKFinserv.com or Download the TTKFinserv app and compare features.",
            "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.",
            "Step 3 - Fill out the eligibility form.",
            "Step 4 - Cross verify the given information and submit the application."


        ],
        bankWiseLoanPresent: true,
        bankWiseLoan: [
            {
                "bankName": "SBI Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "HDFC Bank Personal Loan",
                "interestRate": "10.50% p.a. onwards"
            },
            {
                "bankName": "Kotak Mahindra Bank Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Axis Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Bank of Baroda Personal Loan",
                "interestRate": "10.90% onwards"
            },
            {
                "bankName": "PNB Personal Loan",
                "interestRate": "10.40% onwards"
            },
            {
                "bankName": "Canara Bank Personal Loan",
                "interestRate": "10.65% onwards"
            },
            {
                "bankName": "ICICI Bank Personal Loan",
                "interestRate": "10.50% onwards"
            },
            {
                "bankName": "IDFC Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Indian Overseas Bank Personal Loan",
                "interestRate": "12.50% onwards"
            },
            {
                "bankName": "IDBI Bank Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "Citi Bank Personal Loan",
                "interestRate": "11.49% onwards"
            },
            {
                "bankName": "Bank of India Personal Loan",
                "interestRate": "10.25% onwards"
            },
            {
                "bankName": "Federal Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "UCO Bank Personal Loan",
                "interestRate": "10.95% onwards"
            },
            {
                "bankName": "Union Bank of India Personal Loan",
                "interestRate": "10.40% onwards"
            },
            {
                "bankName": "Yes Bank Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Central Bank of India Personal Loan",
                "interestRate": "10.95% onwards"
            },
            {
                "bankName": "Induslnd Bank Ltd Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Indian Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Standard Chartered Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "RBL Bank Personal Loan",
                "interestRate": "17.50% onwards"
            },
            {
                "bankName": "Bandhan Bank Personal Loan",
                "interestRate": "10.50% onwards"
            }
        ],
        LoanFromNBFCs: [
            {
                "bankName": "SBI Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "HDFC Bank Personal Loan",
                "interestRate": "10.50% p.a. onwards"
            },
            {
                "bankName": "Kotak Mahindra Bank Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Axis Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Bank of Baroda Personal Loan",
                "interestRate": "10.90% onwards"
            },
            {
                "bankName": "PNB Personal Loan",
                "interestRate": "10.40% onwards"
            },
            {
                "bankName": "Canara Bank Personal Loan",
                "interestRate": "10.65% onwards"
            },
            {
                "bankName": "ICICI Bank Personal Loan",
                "interestRate": "10.50% onwards"
            },
            {
                "bankName": "IDFC Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Indian Overseas Bank Personal Loan",
                "interestRate": "12.50% onwards"
            },
            {
                "bankName": "IDBI Bank Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "Citi Bank Personal Loan",
                "interestRate": "11.49% onwards"
            },
            {
                "bankName": "Bank of India Personal Loan",
                "interestRate": "10.25% onwards"
            },
            {
                "bankName": "Federal Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "UCO Bank Personal Loan",
                "interestRate": "10.95% onwards"
            },
            {
                "bankName": "Union Bank of India Personal Loan",
                "interestRate": "10.40% onwards"
            },
            {
                "bankName": "Yes Bank Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Central Bank of India Personal Loan",
                "interestRate": "10.95% onwards"
            },
            {
                "bankName": "Induslnd Bank Ltd Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Indian Bank Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Standard Chartered Personal Loan",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "RBL Bank Personal Loan",
                "interestRate": "17.50% onwards"
            },
            {
                "bankName": "Bandhan Bank Personal Loan",
                "interestRate": "10.50% onwards"
            },
            {
                "bankName": "Paysense Personal Loan",
                "interestRate": "18.00% onwards"
            },
            {
                "bankName": "KreditBee Personal Loan",
                "interestRate": "12.24% onwards"
            },
            {
                "bankName": "Money View Personal Loan",
                "interestRate": "15.96% onwards"
            },
            {
                "bankName": "Muthoot Finance Personal Loan",
                "interestRate": "14% onwards"
            },
            {
                "bankName": "Tata Capital Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Bajaj Finance Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "Navi Personal Loan",
                "interestRate": "9.9% onwards"
            },
            {
                "bankName": "Dhani Personal Loan",
                "interestRate": "13.99% onwards"
            },
            {
                "bankName": "Faircent Personal Loan",
                "interestRate": "9.99% onwards"
            },
            {
                "bankName": "Cashe Personal Loan",
                "interestRate": "30% onwards"
            },
            {
                "bankName": "Fullerton Personal Loan",
                "interestRate": "11.99% onwards"
            },
            {
                "bankName": "Money Tap Personal Loan",
                "interestRate": "12.96% onwards"
            },
            {
                "bankName": "Fibe Personal Loan",
                "interestRate": "24% onwards"
            },
            {
                "bankName": "Flex Salary Personal Loan",
                "interestRate": "36% onwards"
            },
            {
                "bankName": "Incred Personal Loan",
                "interestRate": "16% onwards"
            },
            {
                "bankName": "Lendingkart Business Loan",
                "interestRate": "12% onwards"
            },
            {
                "bankName": "L&T Personal Loan",
                "interestRate": "12% onwards"
            },
            {
                "bankName": "Mcapital Business Loan",
                "interestRate": "24% onwards"
            },
            {
                "bankName": "Mpokket Personal Loan",
                "interestRate": "24% onwards"
            },
            {
                "bankName": "NIRA Personal Loan",
                "interestRate": "24% onwards"
            },
            {
                "bankName": "Prefr Personal Loan",
                "interestRate": "18% onwards"
            },
            {
                "bankName": "Upwards Personal Loan",
                "interestRate": "9% onwards"
            },
            {
                "bankName": "Zype Personal Loan",
                "interestRate": "9.5% onwards"
            },
            {
                "bankName": "Ambit Finvest Business Loan",
                "interestRate": "As per the business requirements"
            },
            {
                "bankName": "HDB Personal Loan",
                "interestRate": "12% onwards"
            },
            {
                "bankName": "LoanTap Personal Loans",
                "interestRate": "12% onwards"
            },
            {
                "bankName": "Loanbaba Personal Loan",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "Phocket Loan",
                "interestRate": "30% onwards"
            },
            {
                "bankName": "Pocketly loan",
                "interestRate": "12% onwards"
            },
            {
                "bankName": "Aditya Birla Capital Personal Loan",
                "interestRate": "11% onwards"
            },
            {
                "bankName": "IIFL Personal Loan",
                "interestRate": "13.50% onwards"
            }
        ],
        tipsToGetLoan: ["https://www.youtube.com/embed/VIIWUAdxv2c"],


    }

    ,
    // Home Loan
    {
        name: "HOME  loan",
        path: "homeLoan",
        imageLink: "https://img.freepik.com/free-photo/front-view-house-investments-elements-composition_23-2148793807.jpg?w=996&t=st=1691737279~exp=1691737879~hmac=83863870cdf151853c272ab4c85afc877609ea33b688e45ff6de43da3b634190",
        titleTagline: "Get Home Loans at Lowest Possible Interest Rate ",
        description: "Banks and Housing Finance Companies (HFCs) offer home loans up to 75-90% of the property's value, depending on the borrower's credit profile and subject to the lenders' and RBI's LTV caps. The loan term can be up to 30 years, depending on the borrower's age and repayment capacity. At TTKFinserv.com, we help you compare home loan interest rates and features from top banks and HFCs, and apply online for the best option for you.",
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        benifits: [
            "Affordable Repayment Options Get flexible repayment plans.",
            "Long Repayment Tenure Extended loan terms help lower EMI",
            "Tax Benefits Get tax deduction on your home loan",
            "Asset Creation Builds equity and creat a valuable asset.",
            "Investment Potential Real estate can lead to long-term financial gains",
            "Tax benefits to enhance affordability",
            "Minimal Documentaion Faster Documentaion process for existing customers"
        ],
        loanHighlights: {
            InterestRate: "Beginning at a competitive rate, starting from 8.50% per annum (p.a.).",
            LoanAmount: "Access a range of ₹10 Lakhs to ₹5 Crores to suit your housing needs.",
            Tenure: "Choose a comfortable loan tenure spanning from 5 to 30 years.",
            ProcessingFees: "Applicable processing fees of up to 1% of the loan amount, along with any relevant taxes."
        },
        price: "Up to ₹50 Lakh",
        interestRate: " 3% to 6% per annum",
        eligibility: [
            "Nationality: Indian Residents, Non-Resident Indians (NRIs), and Persons of Indian Origin (PIOs)",
            "Credit Score: Preferably 750 and above",
            "Age Limit: 18 – 70 years",
            "Business Continuity: At least 3 years (for self-employed)",
            "Minimum Salary: At least Rs. 25,000 per month (varies across lenders & locations)",
            "Loan Amount: Up to 90% of property value",
            "Work Experience: At least 2 years (for salaried)",
            "Note: Apart from the above parameters, your home loan eligibility also depends on the property you are buying and the location of the property."
        ],


        applySteps: ["At TTKFinserv.com, you can compare and apply for eligible home loan offers in three simple steps:", "Step 1- Share Your Details, Enter personal information as well as the details related to your loan requirements.", "Step 2- View Offers-As per the details shared, a list of eligible home loan offers will appear. Compare interest rate, processing fee, and eligible loan amount from the list of eligible home loan offers.", "Step 3- Submit the Application-Apply for the home loan offer that suits your loan requirements the best. Once your application is successfully submitted, you will get a confirmation of your home loan application along with a reference number for future reference. Next, our loan expert will get in touch within 24 hours to take this application forward."],

        typesOfLoan: ["Fixed-Rate Home Loan: In this type of loan, the interest rate remains fixed for the entire loan tenure. This provides predictability as your monthly payments remain constant, making it easier to budget.", "Adjustable-Rate (Variable) Home Loan: With an adjustable-rate home loan, the interest rate can change periodically based on market conditions or a benchmark rate. This could lead to fluctuations in your monthly payments.", "Floating Rate Home Loan: Similar to an adjustable-rate loan, a floating rate home loan's interest rate can change over time based on market conditions. The rate might change periodically, impacting your monthly payments.", "Balloon Payment Loan: This type of loan involves making smaller monthly payments initially and a large 'balloon' payment at the end of the loan term. It can be suitable for those who anticipate a large sum of money at the end of the loan period.", "Interest-Only Loan: In an interest-only loan, you only pay the interest for a certain period (usually the first few years), after which you start repaying the principal along with interest. This can help lower initial payments but may result in higher payments later.", "Combination (Split) Loan: This loan combines elements of fixed and variable interest rates. A portion of the loan might have a fixed rate, providing stability, while the remainder is subject to a variable rate.", "Home Loan with Step-Up/Step-Down EMI: Some lenders offer loans where your EMI gradually increases (step-up) or decreases (step-down) over time. This aligns with your expected income growth or retirement plans.", "Home Construction Loan: This type of loan is designed for those building a home. Funds are released in stages as construction progresses, and interest payments are usually applicable only on the amount disbursed.", "Home Improvement Loan: If you plan to renovate or upgrade your existing home, a home improvement loan can provide funds specifically for those purposes.", "Land Purchase Loan: If you're buying a piece of land to build a home later, a land purchase loan can help you finance the acquisition.", "NRI Home Loan: Non-Resident Indians (NRIs) can avail of special home loan schemes tailored to their needs."],

        tipsToGetLoan: "https://www.youtube.com/embed/D--xPcTEyIo",
        dosAndDonts: {
            Dos: ["Do Your Research: Thoroughly research different lenders, their home loan offerings, interest rates, and terms. Compare multiple options to find the best fit for your needs.", "Check Your Credit Score: Before applying, check your credit score. A higher credit score can help you qualify for better interest rates. If your score is low, work on improving it before applying for a loan.", "Understand Loan Terms: Read and understand all terms and conditions associated with the home loan, including interest rates, tenure, prepayment penalties, and processing fees. Ask questions if you're unsure about any aspect.  ", "Calculate Affordability: Determine how much you can afford to borrow based on your income, expenses, and financial goals. Avoid overextending yourself with a loan that might strain your finances.", "Get a Pre-Approval: Getting pre-approved for a loan can give you a clearer picture of the amount you qualify for, making your home search more focused. Pre-approval also demonstrates your seriousness to sellers."],
            Donts: ["Don't Overextend: Avoid borrowing more than you can comfortably repay. Taking on a home loan that strains your finances can lead to stress and financial instability.", "Don't Ignore Additional Costs: Consider all associated costs, such as processing fees, legal fees, stamp duty, and maintenance expenses. Factor these into your budget.", "Don't Change Your Financial Situation: Avoid making significant financial changes (e.g., changing jobs, taking on new debt) during the loan application process, as this could affect your eligibility.", "Don't Ignore the Fine Print: Read all loan documents carefully and don't skip over any fine print. Understand the terms and implications of late payments, prepayments, and foreclosure.", "Don't Rush Decisions: Buying a home is a major decision. Avoid rushing into a loan or a property purchase without proper due diligence. Take your time to make informed choices."]
        },

        documents: [
            "Proof of Identity: Copy of any one (PAN Card, Passport, Aadhaar Card, Voter’s ID Card, and Driving License)",
            "Proof of Age: Copy of any one (Aadhaar Card, PAN Card, Passport, Birth Certificate, 10th Class Mark-sheet, Bank Passbook, and Driving License)",
            "Proof of Residence: Copy of anyone (Bank Passbook, Voter’s ID, Ration Card. Passport, Utility Bills (Telephone Bill, Electricity Bill, Water Bill, Gas Bill) and LIC Policy Receipt",
            "Proof of Income for Salaried: Copy of Form 16, latest payslips, IT returns (ITR) of past 3 years, and investment proofs (if any)Proof of Income for Self Employed: Details of ITR  of last 3 years, Balance Sheet and Profit & Loss Account Statement of the Company/Firm, Business License Details, and Proof of Business Address",
            "Property-related Documents: NOC from Society/Builder, detailed estimate of the cost of construction of the house, registered sale deed, allotment letter, and an approved copy of the building plan.",
            "Identity proof: Voter ID, Aadhaar Card, Driving Licence, Passport",
            "Note: The above list is indicative and your lender might ask for additional documents.",
            "Also Check: The complete checklist of the home loan documents required"
        ],


        collateral: ["No collateral required"],
        feeAndCharges: [
            { event: "Processing Fee", charges: "	A fee of 1% to 2% will be charged based on the loan amount." },
            { event: "Foreclosure/Prepayment Charges", charges: "For floating rate: No charges apply. For fixed rate: 2% to 4% of the outstanding principal." },
            { event: "EMI Bounce Charges", charges: "Around Rs 400" },
            { event: "Overdue Charges on EMI", charges: "Late payment charges are 2% per month of the unpaid EMI amount." },
            { event: "Legal Fee", charges: "Actual legal fees will apply." }
        ],
        repaymentOptions: {
            description: "Repayment of a housing loan, also known as home loan repayment, involves fulfilling your financial obligation towards the loan. There are different options available for borrowers, including pre-payment and part-payment.",
            options: [
                {
                    option: " Equal Monthly Installments (EMIs):",
                    optionDescription: "This is the most common repayment method. The loan amount, interest, and tenure are divided into equal monthly installments. The EMI remains constant throughout the repayment period, making it easier to budget."
                },
                {
                    option: " Step-Up Repayment:",
                    optionDescription: "With this option, your initial EMIs are lower, gradually increasing over time. This is suitable if you expect your income to rise in the future."
                },
                {
                    option: " Step-Down Repayment:",

                    optionDescription: "In this approach, your initial EMIs are higher, decreasing over time. This might be helpful if you have a higher income at the beginning of the loan term."
                },
                {
                    option: " Balloon Repayment: ",

                    optionDescription: " With this method, you pay smaller EMIs throughout the loan tenure, and a larger 'balloon' payment is due at the end of the term. This can suit those with anticipated lump-sum funds in the future."
                },
                {
                    option: " Accelerated Repayment: ",

                    optionDescription: "Here, you make more frequent payments, such as fortnightly instead of monthly, which can help reduce the overall interest burden over time."
                },
                {
                    option: " Home Loan Overdraft: ",

                    optionDescription: "Some lenders offer a home loan overdraft facility, where you can deposit surplus funds into the loan account and withdraw them as needed. This can help save on interest while maintaining liquidity."
                },

            ]
        },


        typesOfLoan: [
            "Home Purchase Loan Buy properties, new or resale. Also, build on a plot within a given time frame.",
            "Composite Loan Plot purchase and house building. Disbursements follow construction stages.",
            "Home Construction Loan Funds house construction, stages determine disbursements.",
            "Home Renovation Loan Covers existing house renovation costs, often at the same interest rate as a home loan.",
            "Home Extension Loan Extends or adds space to existing houses, lenders offer 75%-90% of construction estimate.",
            "Bridge Loan Short-term loan for buying new house with proceeds from selling existing house.",
            "Interest Saver Loan Home loan overdraft linked to bank account, extra deposits reduce interest.",
            "Step Up Loan Lower EMIs initially, increase over time. Affordable for new professionals."
        ],
        taxBenefits: {
            Section_of_Income_Tax_Act: "Section 24(b), Section 80C",
            Nature_of_Home_Loan_Tax_Deduction: "	Interest paid, Principal (including stamp duty and registration fee)",
            Max_Tax_Deductible_Amt: "Rs. 2 lakh, 	Rs. 1.5 lakh",

        },
        faq: [
            { question: "1. Can I get a home loan for the entire property value?", answer: "Ans. No, you cannot get a home loan for the entire property value as the Reserve Bank of India (RBI) has capped the Loan-to-Value (LTV) ratio of housing loans. As per the RBI guidelines, the LTV ratio can go up to 90% of the property value for loan amounts up to Rs 30 lakh; for loan amounts above Rs 30 lakh and up to Rs 75 lakh, the LTV ratio limit is up to 80% of the property value and for loan amounts above Rs 75 lakh, the LTV ratio can go up to 75% of the property value. This implies that at least 10% of the remaining value must be shelled out by the borrower as down payment. Subject to the caps set by the RBI on LTV ratios, banks/HFCs further fix the LTV ratio on the basis of the risk assessment and credit profile of the loan applicant. Those with lower creditworthiness are usually offered lower LTV ratio." },
            { question: "2. How do lenders check the EMI affordability for their home loan applicants?", answer: "Ans. Lenders consider the repayment capacity of home loan applicants while evaluating their loan application and loan amount eligibility. house loan lenders usually prefer lending to home loan applicants having total EMIs, including EMI of the proposed home loan, to be within 50-60% of their monthly income. Hence, home loan applicants can use online home loan EMI calculator to find out the optimum house loan amount and tenure based on their repayment capacity." },
            { question: "3. How much credit score should I have to get a housing loan?", answer: "Ans. Lenders prefer sanctioning housing loans to applicants having credit scores of 750 and above as such high credit scores reflect responsible credit behaviour and reduce credit risk for lenders. This is also the reason why many lenders offer lower house loan interest rates to applicants having high credit scores. However, some lenders offer home loan to applicants having low credit score at higher interest rates. Therefore, one must check their credit scores at regular intervals. Individuals having no or low credit scores can build or rebuild their credit scores with the help of secured credit cards like TTKFinserv Step UP Credit Card." },
            { question: "4. Who can co-sign a home loan?", answer: "Ans. Your spouse or blood relatives such as your father, mother, siblings and children can co-sign a home loan with you. Also, all co-owners of the property must be co-applicants in housing loan." },
            { question: "5. Are there any prepayment charges in case of a house loan?", answer: "Ans. In the case of floating rate home loans, lenders don’t charge a pre-payment penalty as per RBI directives. However, lenders may levy prepayment penalty in case of prepayment of fixed rate home loans." },
            { question: "6. What is a home loan balance transfer?", answer: "Ans. Home loan balance transfer allows existing home loan borrowers to transfer their outstanding home loans to a new lender at lower interest rates and/or better loan terms. This facility is especially helpful for those borrowers who had availed housing loans at higher interest rates but are now eligible for lower interest rates due to their improved credit profile or reduction in market interest rates." },
            { question: "7. Can I take two home loans at the same time?", answer: "Ans. Yes, if the lender of your second home loan is satisfied with your repayment capacity, credit profile and the characteristics of the pledged property, you can avail a second house loan for another property." },
            { question: "8. How long does it take to get a home loan sanctioned?", answer: "Ans. Usually, it takes 1 to 2 weeks for lenders to sanction a housing loan. However, it may significantly vary depending on banks/HFCs loan approval process, credit profile of the applicant and the features of the property to be purchased/ constructed. Know more about your lender’s house loan eligibility requirements and documentation process so that you’re prepared in advance, which in turn can reduce the time taken for loan approval process." },
            { question: "9. What is the difference between a fixed rate and floating rate home loan?", answer: "Ans. In a fixed rate home loan, the rate of interest applicable at the time of loan disbursal remains same throughout the loan period. As the interest rates remain the same throughout the loan tenure, you will be shielded from interest rates increases during the loan tenure. However, at any time during the loan tenure, if the lending rates fall, the fixed interest rates will remain unchanged, giving you no benefit of the reduced EMIs. In case of floating rate home loans, the interest rates are subject to change as per the changes in the linked benchmark rates used by the lenders. Floating interest rate home loans are usually cheaper than the fixed interest rate home loans and the RBI mandates no prepayment or foreclosure charges for individuals borrowing a floating rate home loan." },
            { question: "10. Can I prepay my outstanding housing loan amount?", answer: "Ans. Yes, you can prepay your home loan. If you have floating rate home loans, no prepayment charges will be levied. However, in case of fixed rate home loans, lenders might levy around 2% to 4% of the prepayment charges." },
            { question: "11. Can I avail tax deductions on my home loan?", answer: "Ans. Yes. The repayment of principal amount would qualify for tax deductions under Section 80C of Income Tax Act. The repayment of interest component would qualify for tax deduction under Section 24(b) of the IT Act." },
            { question: "12. Can I switch from a fixed rate to a floating rate during my home loan tenure?", answer: "Ans. Yes, most lenders offering home loans at both fixed and floating rates allow their existing home loan borrowers to convert their fixed rate loans into floating rate loans and vice versa, on the payment of conversion or switching fee." },
            { question: "13. Which bank offers the best home loan interest rates?", answer: "Ans. To most consumer, lenders offering best home loan interest rates would be synonymous with the lender offering lowest interest. However, lenders set home loan interest rates for their loan applicants based on their credit risk assessment. As the credit risk assessment process followed by the lenders can vary widely, home loan applicants should compare the home loan offerings of as many lenders as possible to get the best possible home loan rates available on their credit profile." },
            { question: "14. Is it possible to avail a personal loan if I am currently servicing a home loan?", answer: "Ans. Yes, you can apply for personal loan even if you have an ongoing home loan. However, the odds of getting your personal loan application approved would depend on whether you have sufficient repayment capacity to pay off the proposed personal loan. Lenders usually prefer offering personal loans to applicants whose total loan repayment obligations, including EMI of the proposed personal loan, is within 50% to 55% of their monthly income. Alternatively, home loan borrowers can consider availing top-up home loans. Like personal loan, top-up home loan does not have any end use restriction. The interest rates offered on top-up home loans are usually the same or slightly higher than the underlying home loan interest rates, which makes it a cheaper alternative to personal loans. Moreover, personal loan tenures usually go up to 7 years whereas the tenure of top up home loans can go up to 15 years, depending on the residual tenure of the underlying home loan. Thus, longer loan tenure and lower interest rates of top-up home loans would allow lower EMIs than personal loans and thereby, increase the EMI affordability for the borrowers." }
        ],
        bankWiseLoanPresent: true,
        bankWiseLoan: [
            { bankName: "State Bank of India", interestRate: "9.15% p.a. onwards" },
            { bankName: "HDFC Ltd.", interestRate: "8.50% p.a. onwards" },
            { bankName: "ICICI Bank", interestRate: "9.00% p.a. onwards" },
            { bankName: "Bank of Baroda", interestRate: "9.15% p.a. onwards" },
            { bankName: "Union Bank of India", interestRate: "9.00% p.a. onwards" },
            { bankName: "Axis Bank", interestRate: "8.75% p.a. onwards" },
            { bankName: "Kotak Mahindra Bank", interestRate: "8.85% p.a. onwards" },
            { bankName: "LIC Housing Finance", interestRate: "8.60% p.a. onwards" },
            { bankName: "Federal Bank", interestRate: "10.15% p.a. onwards" },
            { bankName: "Bajaj Housing Finance", interestRate: "8.70% p.a. onwards" },
            { bankName: "IDFC First Bank", interestRate: "8.85% p.a. onwards" },
            { bankName: "PNB Housing Finance", interestRate: "8.75% p.a. onwards" },
            { bankName: "Tata Capital Housing Finance", interestRate: "9.15% p.a. onwards" },
            { bankName: "L&T Housing Finance", interestRate: "8.60% p.a. onwards" },
            { bankName: "Standard Chartered", interestRate: "8.75% p.a. onwards" },
        ],



        LoanFromNBFCs: [
            { bankName: "Muthoot Finance", interestRate: "14% onwards" },
            { bankName: "Tata Capital", interestRate: "10.99% onwards" },
            { bankName: "Bajaj Finserv", interestRate: "11% onwards" },
            { bankName: "StashFin", interestRate: "11.99% onwards" },
            { bankName: "Faircent", interestRate: "9.99% onwards" },
            { bankName: "Kreditbee", interestRate: "Up to 29.95%" },
            { bankName: "Navi Finserv", interestRate: "9.9% – 45%" },
            { bankName: "Money Tap", interestRate: "13% onwards" },
            { bankName: "Dhani Loans", interestRate: "13.99% onwards" },
            { bankName: "Money View", interestRate: "15.96%" },
            { bankName: "Pay Sense", interestRate: "16.80%-27.60%" },
            { bankName: "Early Salary", interestRate: "18% onwards" },
            { bankName: "Home Credit", interestRate: "24% onwards" },
            { bankName: "CASHe", interestRate: "30% onwards" },
            { bankName: "HDB Financial Services", interestRate: "Up to 36%" },
        ]


    },
    // business loan
    {
        name: "Business Loan",
        path: "businessLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        titleTagline: "Empowering Business Growth by partnering for Success.",
        description: "At TTK Finserv, we understand that every business needs capital to grow. Whether you are launching or expanding your business, we pledge to give you the best business loan offers. We have a wide range of business loan products to choose from, and we will work with you to find the perfect loan for your needs. We are committed to helping businesses succeed, and we believe that access to capital is essential for growth.",
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        benifits: [
            "Minimal Paperwork Get your loan approved within minimum documentaion.",
            "Low Interest Rates  Starting at 8.90% p.a.",
            "No CollateralGet the financial assets without risk.",
            "Start-ups and WomenLending privileges for women & assistance to start-ups",
            "Flexible Loan Amount Avail the loan amount you need to upscale your business",
            "Flexible Repayments Avail the repayment tenurethat suits you the best.",

        ],
        interestRate: "6% to 30% or more p.a. onwards",
        price: "Up to ₹50 Lakh",
        eligibility: [
            "Age:	Min 21 - Max 65 (at the time of loan maturity)",
            "Business Vintage:	Min 2 years or above",
            "Annual Turnover:	As per banks",
            "Credit Score:	750 or above",
            "Nationality: Indian",
        ],
        documents: [
            "Identity Proof: Passport, driver's licence, Aadhaar card, Voter ID",
            "Address Proof: Utility bill (electricity, water, or gas), bank statement",
            "Income Proof: Recent payslips, Income Tax Returns (IT returns)",
            "Photographs: Recent passport-sized photographs",

        ],


        applySteps: ["Step 1 - Visit - TTKFinserv.com  and compare features.", "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.", "Step 3 - Fill out the eligibility form and provide: Email Address, Employment Type, Company Type  Industry Type, Current Company Name , Company Address  Year of Employment, Net Income  Mode of Salary, Pan Card Details, Full Name (As Per PAN Card), Current Address with PIN Code, Mobile Number", "Step 4 - Cross verify the given information and submit the application"],
        tipsToGetLoan: "https://www.youtube.com/embed/CD2KyF8_vSg",


        loanHighlights: {
            InterestRate: "	10% - 21%",
            LoanAmount: "Rs. 50,000/- to Rs.15 Cr-",
            LoanProcessingFees: "Rs 1000 - 75000/-",
            Tenure: "Up to 10 years",
            PrepaymentCharges: "Up to 4% on the balance",
        },
        dosAndDonts: {
            Dos: ["Do Your Research: Thoroughly research different lenders and their business loan offerings. Compare interest rates, fees, terms, and repayment options to find the best fit for your business needs.", "Prepare a Solid Business Plan: A comprehensive business plan demonstrates to lenders that you have a clear strategy for utilizing the loan funds and repaying the loan. Include financial projections, growth plans, and a repayment strategy.", "Check Your Credit Score: Just like with personal loans, your credit score matters for business loans. Check your credit score and take steps to improve it if necessary before applying.", "Understand Loan Terms: Carefully read and understand all terms and conditions associated with the loan. Clarify any doubts you have with the lender before proceeding.", "Consider Loan Amount Carefully: Borrow only what you need and can realistically repay. Overborrowing can strain your business finances and make repayment difficult."],
            Donts: ["Don't Rush into a Decision: Avoid rushing into a loan agreement. Take your time to evaluate multiple options, review terms, and understand the implications.", "Don't Ignore Hidden Fees: Carefully review all fees associated with the loan, including processing fees, origination fees, prepayment penalties, and more. These can significantly impact the total cost of the loan.", "Don't Overstate Income or Assets: Be truthful and accurate when providing financial information to the lender. Overstating income or assets can lead to approval based on false information.", "Don't Overlook Repayment Capacity: Analyze your business's cash flow and determine how comfortably you can manage the loan repayments. Overestimating your repayment capacity can lead to financial strain.", "Don't Forget About Collateral: Depending on the type of loan, collateral might be required. Understand the implications of pledging assets as collateral for the loan."]

        },


        repaymentOptions: {
            description: "Business loans come with various repayment options to suit different business needs and financial situations. Here are some common repayment options for a business loan:",
            options: [
                {
                    option: "Term Loan with Equal Monthly Installments (EMIs):",
                    optionDescription: "This is the most straightforward repayment option. The loan amount, interest, and tenure are divided into equal monthly installments. EMIs remain constant throughout the repayment period."
                },
                {
                    option: "Flexible EMIs:",
                    optionDescription: "Some lenders offer flexibility in EMI payments. You might have the option to choose varying EMI amounts within a predefined range to adjust payments based on business cash flow."
                },
                {
                    option: "Balloon Payment:",
                    optionDescription: "With this option, you make smaller regular payments and a larger 'balloon' payment at the end of the loan term. This could be suitable if you anticipate a lump sum payment in the future."
                },
                {
                    option: "Bullet Repayment:",
                    optionDescription: "In bullet repayment, you pay interest during the loan term and repay the principal in a lump sum at the end. This can suit businesses with seasonal income or large future receivables."
                },
                {
                    option: "Line of Credit: ",
                    optionDescription: "Business lines of credit operate similarly to credit cards. You have a credit limit, and you can borrow and repay within that limit. Interest is charged only on the amount borrowed."
                },
                {
                    option: "Revolving Credit: ",
                    optionDescription: "This is another form of a credit line where you can borrow, repay, and borrow again as needed. Interest is charged on the outstanding balance."
                },
                {
                    option: "Interest-Only Repayment:",
                    optionDescription: "In this option, you pay only the interest component for a certain period, often at the beginning of the loan term. Afterward, you start repaying the principal along with interest."
                },
                {
                    option: "Accelerated Repayment:",
                    optionDescription: "This involves making more frequent payments, like weekly or bi-weekly, instead of monthly. This can help reduce the overall interest burden."
                },
            ]
        },
        collateral: ["No collateral required"],
        feeAndCharges: [
            { event: "Processing Fees: ", charges: "This is a one-time fee charged by the lender for processing your loan application. It covers the administrative costs of evaluating your application." },
            { event: "Origination Fees: ", charges: " Similar to processing fees, origination fees cover the cost of setting up the loan. These fees might be a percentage of the loan amount." },
            { event: "Underwriting Fees: ", charges: "Some lenders charge underwriting fees for evaluating your creditworthiness and the risk associated with your loan." },
            { event: "Appraisal Fees:", charges: "If your loan involves collateral (such as real estate or equipment), an appraisal might be required to determine the value of the collateral. The cost of the appraisal is typically borne by the borrower." },
            { event: "Late Payment Fees: ", charges: " If you miss a payment deadline, lenders can charge late payment fees. Make sure to understand the terms related to late payments." },
            { event: "Commitment Fees: ", charges: "For lines of credit or term loans with floating interest rates, lenders might charge commitment fees to hold a certain amount of credit available for you." },
            { event: "Annual Maintenance Fees:", charges: "These are annual fees charged to maintain your loan account, and they might be applicable for certain types of business loans." },

        ],
        typesOfLoan: [
            "1.Term Loan  For expansion, purchasing assets, or working capital needs.",
            "2.Working Capital Loan  Working capital loans fund day-to-day operations and bridge cash flow gaps for expenses like inventory, payroll, and supplier payments.",
            "3.Machinery and Equipment Loan  Finance purchasing or leasing machinery, equipment, or vehicles necessary for business operations.",
            "4.Small Business Administration (SBA) Loan  Small business loan backed by the government.",
            "5.Business Line of Credit Loan Provides a revolving credit facility, allowing businesses to withdraw funds as needed.",
            "6.Invoice Financing Loan This loan type enables businesses to access funds by selling their unpaid invoices to a lender, also known as accounts receivable financing.",
            "7.Trade Finance  Trade finance Loan facilitate international trade transactions by providing financing for import or export activities, including letters of credit, export financing, and supply chain financing.",
            "8.Startup Business Loan  Designed to support new businesses in their early stages.",
        ],

        faq: [
            { question: "How Can I Get A Business Loan Quickly?", answer: "Consider lenders who offer quick loan approvals. Fintech companies and online lenders may have streamlined processes and faster turnaround times than traditional banks." },
            { question: "What Is The Maximum Amount Of A Business Loan?", answer: "The maximum amount of a business loan can vary depending on the lender, borrower's eligibility, and other factors, but it can range from a few lakhs to crores of rupees." },
            { question: "Can A Fresher Get A Business Loan?", answer: "Getting a business loan as a fresher can be challenging due to the lack of established track record and financial stability. However, with a strong business plan and collateral or a co-signer, it's possible to increase the chances of obtaining a loan." },
            { question: "What Is The Interest Rate Of A Business Loan?", answer: "The interest rate of a business loan can vary depending on factors such as the borrower's creditworthiness, loan amount, loan tenure, and prevailing market conditions." },
            { question: "How Much Is The Business Loan Processing Fee?", answer: "The processing fee for a business loan can vary among lenders and is typically a percentage of the loan amount" },
            { question: "What Are The Documents Required For A Business Loan In SBI?", answer: "The specific documentation requirements for a business loan in State Bank of India (SBI) may vary based on the type of loan and the borrower's profile. However, the general list of documents i.e. required which can be referred from above." },
            { question: "Is The Business Loan Amount Taxable", answer: "The business loan amount itself is not taxable as it is considered a liability and not an income." },
            { question: "Disclaimer", answer: "Display of trademarks, tradenames, logos, and other subject matters of Intellectual Property displayed on this website belongs to their respective intellectual property owners & is not owned by Bvalue Services Pvt. Ltd. Display of such Intellectual Property and related product information does not imply, Bvalue Services Pvt. Ltd company’s partnership with the owner of the Intellectual Property or proprietor of such products. Please read the Terms & Conditions carefully as deemed & proceed at your discretion." }
        ],

        bankWiseLoanPresent: true,
        bankWiseLoan: [

            { bankName: "State Bank of India(SBI)", interestRate: "8.90% onwards" },
            { bankName: "hdfc", interestRate: "10.00% - 22.50% p.a." },
            { bankName: "ICICI Bank", interestRate: "	12.50% - 13.60% p.a." },
            { bankName: "Axis Bank", interestRate: "14.65% - 18.90% p.a%" },
            { bankName: "Kotak Mahindra Bank", interestRate: "16% - 26% p.a." },
            { bankName: "RBL Bank", interestRate: "	12.25% - 25% p.a." },
            { bankName: "Induslnd Bank", interestRate: "13% to 22%." },

        ],
        LoanFromNBFCs: [


            { bankName: "Bajaj Finserv", interestRate: "9.75% - 30% p.a." },
            { bankName: "IIFL Finance", interestRate: "11.25% - 33.75% p.a." },
            { bankName: "FlexiLoans", interestRate: "Starting at 12% p.a." },
            { bankName: "ZipLoan", interestRate: "12% - 18% p.a. (Flat ROI)" },
            { bankName: "Indifi Finance", interestRate: "15% - 24% p.a." },
            { bankName: "Lendingkart Finance", interestRate: "12% - 27% p.a." },
            { bankName: "Tata Capital Finance", interestRate: "Starting at 19% p.a." },
            { bankName: "NeoGrowth Finance", interestRate: "19% - 24% p.a." },
            { bankName: "Hero FinCorp", interestRate: "Up to 26% p.a." },
            { bankName: "Money View", interestRate: "Starting at 15.96% p.a." },
            { bankName: "KreditBee", interestRate: "12.24% - 29.88% p.a." },
            { bankName: "mCapital", interestRate: "Starting at 24% p.a." }
        ]
    },
    // Credit Card Takeover Loan
    {
        name: "Credit Card Takeover Loan",
        path: "creditCardTakeoverLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        titleTagline: "Unlocking Opportunities with Our Personalized Credit Card Loan.",
        description: "Now you can consolidate your credit card outstanding debts with our ‘Step Up’ repayment option and enjoy 3 months of interest only payments. With our Credit Card Takeover Loan, you will be free of long-pending credit card debt immediately and this unique personal loan will help build financial discipline. You can take Credit Card Takeover Loan for a total of one or multiple credit cards outstanding. Such prudent steps taken eventually help improve CIBIL score.",

        price: "Up to ₹50 Lakh",
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        interestRate: "7% to 25% p.a. onwards",
        eligibility: [
            "Age:	Min 18 - Max 60 ",
            "Income for Salaried: min Rs.20000 per month",
            "Income for self-empolyed: ITR> or equal to Rs.3 lakh per annum",
            "credit score: 700 & above",

        ],
        documents: [
            "Identity & Signature Proof:	Passport, PAN card, Driving License, Voter ID card, Aadhaar card, employee identity card in case of government employees (any 1)",
            "Address Proof:	Bank statement, Rent Agreement, Voter ID card, Ration card, Passport, Driving License, telephone/ electricity/ water/ Property tax (any 1)",
            "Age Proof:	Voter ID card, Secondary School Certificate (class 10), birth certificate, Passport, Aadhaar Card, pension payment order or Receipt of LIC policy (any 1)",
            "Income proof:	For Salaried: Latest 3 months’ salary slips, Salary account bank statement for six months For Self-employed: Latest IT Returns with computation of income and other certified financial documents along with business continuity proof",

        ],



        applySteps: ["step-1: Assess Your Debt: Take stock of your credit card debt. Calculate the total outstanding balance, interest rates, and minimum monthly payments for each card. This assessment will help you determine the amount you need for the takeover loan.", "step-2:Check Your Credit Score: Obtain a copy of your credit report and check your credit score. Lenders typically offer better terms to borrowers with good credit scores. If your score needs improvement, consider taking steps to enhance it before applying. ", "step-3: Research Lenders: Research various lenders that offer credit card takeover loans or debt consolidation loans. Look for lenders with competitive interest rates, favorable terms, and a reputation for good customer service. ", "step-4:Compare Loan Offers: Once you've identified potential lenders, request loan quotes from them. Compare interest rates, loan terms, processing fees, and any other associated costs. This step will help you select the best offer that suits your needs. ", "step-5: Apply and Provide Documentation: Once you've selected a lender, initiate the application process. You'll need to fill out an application form and provide necessary documents, which might include proof of identity, income documents, and information about your existing credit card debts. Be accurate and complete in providing the required information.",],


        loanHighlights: {
            InterestRate: " Starting from a competitive rate of 9.99% per annum (p.a.)ng from 9.99% p.a.",
            LoanAmount: "Access a range of ₹1 Lakh to ₹50 Lakh for consolidating your credit card debt.1 Lakh to ₹50 Lakh",
            Tenure: "Choose a convenient loan tenure spanning from 12 to 60 months.12 - 60 months",
            ProcessingFees: "Applicable processing fees of up to 2% of the loan amount, along with any relevant taxes.Up to 2% plus applicable taxes"
        },
        typesOfLoan:
            [
                "Unsecured Personal Loan for Debt Consolidation Common credit card takeover loan, borrow to clear balances. Unsecured, fixed repayments over a period.",
                "Secured Personal Loan Lenders offer secured credit card takeover loans. Collateral needed, lower rates compared to unsecured.",
                "Home Equity Loan or Line of Credit Homeowners use equity to consolidate card debt. Home value collateral, default risks foreclosure."
            ],
        tipsToGetLoan: "https://www.youtube.com/embed/e3RrPGwHfbE",
        dosAndDonts: {
            Dos: ["Calculate Costs: Calculate the total cost of transferring your credit card balance to a takeover loan. Consider interest rates, processing fees, and other charges to ensure it's financially beneficial.", "Compare Offers: Research and compare takeover loan offers from multiple lenders. Look for lower interest rates, favorable terms, and minimal additional charges.", "Read Terms and Conditions: Carefully read and understand the terms of the takeover loan, including interest rates, tenure, repayment schedule, and any hidden fees. Clarify any doubts with the lender.", "Have a Repayment Plan: Plan how you'll repay the takeover loan. Ensure that your monthly budget can accommodate the new loan payments to avoid further financial stress.", "Close or Limit Credit Cards: After transferring the balance to the takeover loan, consider closing or limiting the credit cards to prevent accumulating more debt. Responsible credit management is crucial.",],
            Donts: ["Don't Ignore Fees: Be aware of any processing fees, prepayment penalties, or other charges associated with the takeover loan. These fees can impact the overall cost of the loan.", "Don't Overspend: Avoid using your credit cards for new purchases after transferring the balance to a takeover loan. This can lead to accumulating more debt and defeating the purpose of the loan.", "Don't Rush into Decisions: Take your time to evaluate takeover loan options. Avoid making hasty decisions that might not be in your best financial interest.", "Don't Miss Payments: Timely repayment is crucial for takeover loans, just like any other loan. Missing payments can result in late fees, increased interest rates, and a negative impact on your credit score.", "Don't Rely Solely on the Loan: A takeover loan addresses existing credit card debt, but it's important to address the underlying reasons for accumulating debt. Create a budget, manage spending, and build a strong financial foundation.",]

        },
        repaymentOptions: {
            description: "A Credit Card Takeover Loan is a type of personal loan that allows you to transfer the outstanding balance from one or more credit cards to a single loan account with a potentially lower interest rate. This can help you consolidate your credit card debt and manage it more effectively. When considering repayment options for a Credit Card Takeover Loan, here are a few steps to keep in mind:",
            options: [
                {
                    option: "Fixed Monthly Payments:",
                    optionDescription: "With a Credit Card Takeover Loan, you'll typically have fixed monthly payments. This means that your loan amount will be divided into equal monthly installments, including both principal and interest. Fixed payments make it easier to budget and plan for repayment."
                },
                {
                    option: "Tenure Selection: ",
                    optionDescription: "Choose a loan tenure (the period over which you'll repay the loan) that aligns with your financial situation. A longer tenure might result in lower monthly payments but could lead to higher overall interest payments. A shorter tenure could mean higher monthly payments but lower overall interest."
                },
                {
                    option: "Automatic Payments:",
                    optionDescription: "Consider setting up automatic payments from your bank account. This helps ensure that you never miss a payment, reducing the risk of late fees and negative impacts on your credit score."
                },
                {
                    option: "Extra Payments:",
                    optionDescription: "Some lenders allow you to make extra payments or prepay the loan without penalty. If you have additional funds, consider using them to pay off the loan faster and reduce the overall interest you'll pay."
                },
                {
                    option: "Avoid Missed Payments:",
                    optionDescription: " Missing payments on your Credit Card Takeover Loan can have similar negative consequences as missing credit card payments. Late payments can lead to fees and damage your credit score."
                },
                {
                    option: "Early Repayment:",
                    optionDescription: "If you're financially able, consider repaying the loan early. This can save you money on interest payments in the long run. However, check with the lender to ensure there are no prepayment penalties."
                },
                {
                    option: "Check Interest Rate: ",
                    optionDescription: "While Credit Card Takeover Loans often offer lower interest rates compared to credit cards, make sure you understand the interest rate of the loan and how it compares to your current credit card rates."
                },
                {
                    option: "Regular Monitoring: ",
                    optionDescription: "Keep a close eye on your loan account, payments, and any correspondence from the lender. This helps you stay organized and aware of your loan's status."
                },

            ]
        },
        feeAndCharges: [
            { event: "Processing Fee:", charges: "" },
            { event: "", charges: "Lenders may charge a processing fee when you apply for the Credit Card Takeover Loan. This fee covers administrative costs related to processing your loan application." },
            { event: "Interest Rate: ", charges: " The interest rate on the loan is a significant factor affecting the total cost of borrowing. Credit Card Takeover Loans often have lower interest rates compared to credit card interest rates, but the specific rate can vary based on your creditworthiness and the lender's policies." },
            { event: "Late Payment Fees: ", charges: " If you miss a payment or make a payment after the due date, the lender may charge a late payment fee. This fee is meant to encourage timely payments." },
            { event: "Prepayment Penalty: ", charges: " Some lenders may charge a prepayment penalty if you decide to pay off the loan before the agreed-upon tenure. However, many lenders nowadays do not charge prepayment penalties, so be sure to confirm this with the lender." },
            { event: "Documentation Charges: ", charges: "There might be documentation charges associated with the loan application or the verification process." },
            { event: "Credit Report Charges: ", charges: "Lenders might charge a fee for obtaining your credit report and assessing your creditworthiness during the application process." },
            { event: "Loan Insurance: ", charges: "Some lenders might offer loan insurance or payment protection plans. These optional plans could cover your loan payments in case of unforeseen events such as disability, job loss, or critical illness. However, they come at an additional cost." },
            { event: "Stamp Duty and Other Taxes: ", charges: "Depending on your location and local regulations, you might be required to pay stamp duty or other applicable taxes on the loan amount." },
        ],
        collateral: ["No collateral required"],




        faq: [
            { question: "What are the benefits of Credit card Takeover Loan?", answer: "Credit cards are a most commonly used form of debt borrowing without much thought. It eases monthly finances by offering up to four to five times of usual salary. This gets you entwined in the vicious credit cycle payments. Minimum amount due seems easy but what you should fear is paying more than 25- 40% interest per annum on the outstanding. Now, working professionals who are entangled in this vicious cycle of credit card can set themselves free by availing credit card takeovers. But why should you take a loan to pay another? Credit Card Takeover Loan is a type of Personal Loan and are available at around 1.5 – 2% monthly interest versus 3-4% monthly interest applicable on outstanding credit card payments. They allow you to get over the vicious minimum payment due cycle and ensure you maintain good credit history." },
            { question: "What things should I take care of before applying for a debt consolidation loan?", answer: "You should aim at debt consolidation at one go. It immediately builds in financial discipline and helps in reducing monthly outflow." },
            { question: "To clear the outstanding of two credit cards, will I have to apply for this unique personal loan twice?", answer: "No, you have to apply only once. Loan will be sanctioned as per your eligibility and credit checks; you can consolidate multiple debts in one go." },
            { question: "What is the maximum value up to which I can get debt consolidation loan? And for what tenure?", answer: "LoanTap offers Credit Card Takeover Loans ranging from Rs 50,000 up to Rs 2 Lakhs; Tenure offered is 6-24 months." },
            { question: "What documents are required to apply for a Credit Card Takeover Loan?", answer: "We need below documents for processing your loan request- PAN, Address proof, 3 months’ pay slips, 6 month’s salary account bank statement, e-NACH mandate form. However, we might request for a few other documents if need arises during credit checks." },
            { question: "Is Credit Card Takeover Loan available in my city?", answer: "Credit Card Takeover Loan is available in cities where LoanTap is operational. Currently we are providing our loans in Bangalore, Hyderabad, Chennai, Coimbatore, Mumbai, Pune, Ahmedabad, Vadodara, Raipur, Bhopal, Indore, Jaipur, Delhi-NCR, Ajmer, Lucknow, Mysore, Visakhapatnam, Kolkata, Howrah, Bhubaneshwar, Patna, Nagpur, Nashik, Rajkot, Surat and Chandigarh locations." },
            { question: "How much is the processing fee for availing this loan?", answer: "Processing fee for this loan is 2%+applicable taxes; on the gross loan amount sanctioned." },
            { question: "Can I make part payments/higher payments towards my loan outstanding? Any charges applicable?", answer: "Yes, you can make higher payments towards your loan. No charges for higher payments or foreclosure are applicable post 6 months. Within 6 months you can make part payments or foreclosure-with minimal charges applicable." },
            { question: "What amount do I need to pay every month? What is the repayment pattern?", answer: "In Credit Card Takeover Loan, you get two repayment plans – Step Up Repayment Option (interest only payment for the initial 3 months, followed by fixed EMIs; and Term Repayment plan (Pay equal monthly installments throughout the tenure). The decision of the final repayment plan is taken on the basis of your eligibility and credit checks." },
            { question: "Can I prepay my loan?", answer: "Yes. There are no foreclosure charges for pre-paying after six months of loan disbursement. You can prepay your loan at nominal charges within six months of disbursement." },
            { question: "Can I make part payments/higher payments towards my loan outstanding? Any charges applicable?", answer: "Yes, you can make higher payments towards your loan. No charges for higher payments or foreclosure are applicable post 6 months. Within 6 months you can make part payments or foreclosure-with minimal charges applicable." },
            { question: "Do I have an option to pre-close the loan?", answer: "Yes, you can prepay the loan facility post 6 months of disbursement. No foreclosure charges are applicable post 6 months of loan completion, however within 6 months you can prepay your loan with minimal charges." },
        ],
        bankWiseLoanPresent: false,
    },
    // TWO WHEELER LOAN:
    {
        name: "TWO WHEELER LOAN  ",
        path: "twoWheelerLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        description: "Two-wheeler loans are a type of personal loan that can be used to purchase a new or used motorcycle, scooter, or moped. They are typically offered by banks and non-banking financial companies (NBFCs), and they can be used to finance the entire cost of the vehicle or a portion of it.",
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        titleTagline: "Fulfilling Your Two-Wheeler Dreams with Simple and Affordable Loan Solutions!",
        benifits: [
            "Flexible repayment terms: Two-wheeler loans typically have longer repayment terms than other types of loans, such as car loans. This can make them more affordable for borrowers with limited income. ",
            "Competitive interest rates: Interest rates on two-wheeler loans are typically lower than interest rates on other types of loans, such as personal loans",
            "Easy documentation: The documentation requirements for two-wheeler loans are typically less stringent than the documentation requirements for other types of loans. This can make it easier to qualify for a two-wheeler loan. ",
            "If you are looking to purchase a new or used two-wheeler, a two-wheeler loan can be a great option. TTKFinserv can help you compare two-wheeler loan offers from a variety of lenders and find the best deal for you.",
            "The down payment: Most lenders will require you to make a down payment on your two-wheeler loan. The amount of the down payment will vary depending on the lender and the type of vehicle you are financing.",
            "The interest rate: The interest rate on your two-wheeler loan will affect the total amount of interest you pay over the life of the loan. Shop around and compare interest rates from different lenders before you choose a loan.",
            "The repayment term: The repayment term is the length of time you have to repay your two-wheeler loan. The longer the repayment term, the lower your monthly payments will be, but you will pay more interest over the life of the loan.",
            "The fees: There are often fees associated with two-wheeler loans, such as application fees, processing fees, and late payment fees. Be sure to ask about all of the fees associated with a loan before you sign the paperwork.",


        ],



        price: "Up to ₹50 Lakh",


        interestRate: " 9% to 15% p.a. onwards",
        eligibility: [
            "AGE:	18 - 65 years",
            "Minimum salary:	₹10,000",
            "Employment duration:	Minimum 1 Year",
            "Employment status:	Salaried or self-employed",
            "Residence:	Reside in the city for at least a year",
        ],
        documents: [
            "Make sure to keep these documents in check and handy to avail an instant two-wheeler loan online with TTK Loan",
            "Documents Required: Income certificate",
            "Age proof : Birth Certificate, Driving licence",
            "ID Proof :Aadhaar Card, PAN Card",


        ],


        applySteps: ["Step 1 - Visit - TTKFinserv.com  and compare features.", "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.", "Step 3 - Fill out the eligibility form and provide: Email Address Employment Type, Company Type, Industry Type, Current Company Name, Address Proof, PAN Card Details, Full Name (As Per PAN Card), Current Address with PIN Code, Mobile Number, Income Certificate, Driving License", "Step 4 - Cross verify the given information and submit the application"],
        tipsToGetLoan: ["https://www.youtube.com/embed/ZZoyhWTBNWw",],

        repaymentOptions: {
            description: "Repayment options for a two-wheeler loan provide flexibility to borrowers when paying back the borrowed amount and interest. These options can vary depending on the lender and the terms of the loan agreement. Here are some common repayment options for a two-wheeler loan:",
            options: [
                {
                    option: "Equated Monthly Installments (EMIs):",
                    optionDescription: "This is the most common repayment option. The loan amount, interest, and tenure are divided into equal monthly installments. EMIs are convenient as they provide a predictable payment schedule"
                },

                {
                    option: "Step-Up EMIs:",
                    optionDescription: "The opposite of step-up EMIs, where the initial EMI is higher and gradually decreases. This might be chosen by borrowers who anticipate a reduction in income later in the loan tenure."
                },
                {
                    option: "Step-Down EMIs:",
                    optionDescription: "In this option, the EMI starts at a lower amount and gradually increases over time. This is suitable for borrowers who expect their income to rise in the future."
                },
                {
                    option: "Bullet Repayment: ",
                    optionDescription: "With this option, you pay a larger portion of the loan amount as a lump sum at the end of the loan tenure. It's suitable for borrowers with irregular income who can make a substantial payment at the end."
                },
                {
                    option: "Flexible EMIs: ",
                    optionDescription: "Some lenders offer flexibility in EMI payments. You might be allowed to pay larger amounts during months when you have extra funds and smaller amounts during lean months."

                },
                {
                    option: "Balloon Payment:",
                    optionDescription: " Similar to bullet repayment, but a larger amount is paid at regular intervals, not just at the end. This helps reduce the overall EMI burden."
                },
                {
                    option: "Extended Tenure: ",
                    optionDescription: " If you want to keep your EMI lower, you can opt for an extended loan tenure. However, keep in mind that this will result in paying more interest over the long term."
                },

            ]
        },

        loanHighlights: {
            InterestRate: "	0.99% p.a onwards",
            LoanAmount: "	Up to ₹3 lakhs",
            LoanProcessingFees: "Up to 4% plus applicable taxes",
            Tenure: "Up to 20 months",
            PrepaymentCharges: "Upto 3-5 Years (Depending on Bank)",
        },

        dosAndDonts: {
            Dos: ["Compare interest rates and terms..", "Check your credit score.", "Maintain a good repayment record.", "Opt for insurance coverage.", "Save for a down payment to reduce the loan amount and subsequent EMIs.", "Maintain a budget and allocate funds for timely loan repayments.",],
            Donts: ["Don't borrow more than you can afford to repay.", "Don't provide false information or documents to the lender.", "Don't miss or delay loan repayments.", "Don't ignore the loan prepayment and foreclosure.", "Don't apply for multiple loans simultaneously.", "Don't make impulsive buying decisions beyond your repayment capacity."]

        },
        feeAndCharges: [
            { event: "Processing Fee", charges: "This fee is charged for processing your loan application. It covers administrative costs related to verifying your documents and conducting necessary checks." },
            { event: "Interest Rate: ", charges: "The interest charged on the loan amount is a significant cost. It's important to know whether the interest rate is fixed or variable and to understand the impact it will have on your monthly payments and overall repayment." },
            { event: "EMI Bounce Charges:", charges: " If your EMI payment bounces due to insufficient funds or any other reason, lenders might charge a fee for the inconvenience and administrative work required." },
            { event: "Late Payment Charges: ", charges: " If you miss an EMI payment deadline, the lender may impose a late payment fee. This fee encourages timely payments and covers administrative costs related to tracking and processing late payments." },
            { event: "Prepayment or Foreclosure Charges: ", charges: "Some lenders might charge a fee if you decide to repay the loan before the designated tenure. This compensates the lender for the interest income they would have received if you had continued the loan as per the original schedule." },
            { event: "Documentation Charges:", charges: "There might be a fee associated with processing and verifying the necessary documentation for the loan application." },
            { event: "Insurance Charges:", charges: "If the lender requires you to take a two-wheeler insurance policy as a part of the loan agreement, the premium for the insurance will be an additional cost." },
        ],
        collateral: ["No collateral required"],

        faq: [
            { question: "What Is A Two-Wheeler Loan?", answer: "It is a loan used to finance a bike, scooter, or moped that can be repaid in monthly installments. An asset (usually the two wheeler itself) secures the loan." },
            { question: "How Much Loan Can I Get For A Two-Wheeler?", answer: "You can avail a loan up to Rs. 50 lakh, starting from Rs. 1 lakh, depending on your income, credit history, and other financial obligations." },
            { question: "Can I Get A Bike Loan Without Income Proof?", answer: "No, you cannot obtain a bike loan without income proof. However, you might need to keep a few documents handy, such as bank statements and checks, to claim a loan." },
            { question: "How Can I Buy A Two-Wheeler On EMI?", answer: "You can find different types of lenders who can help you with a Two-wheeler loan online that is quick, simple, and easy to obtain funds for your needs." },
            { question: "Can I Buy A Two-Wheeler Without A Downpayment?", answer: "Yes, you can buy a two-wheeler without a down payment. There are banks that offer 100% bike loan financing, so you can own a two-wheeler without paying anything upfront." },
            { question: "Which Bank Interest Rate Is Low?", answer: "The lowest bank interest rates available are offered by banks like Axis, IDFC, and Induslnd Bank at 10.49%." },
            { question: "Can I Pay My Two-Wheeler Loan Early?", answer: "Yes, depending on your position, you can prepay your bike loan in full or in part. To close the debt quickly, consider prepaying it." },
            { question: "Does A Two-Wheeler Loan Save Tax?", answer: "Yes, you are permitted to declare the two-wheeler as a business asset under the Indian Income Tax Act. You can use the interest that you'll pay on the loan as an exemption. Section 80C of the IT Act governs it, and the annual ceiling is Rs 1.5 lakh." },
            { question: "What Is The Two-Wheeler Loan Application Process?", answer: "Your financial and personal information will be requested. Additionally, you must include precise information on the vehicle, such as the RTO, the dealer's name, and the on-road cost. Your Two - wheeler loan application will then be processed." },
            { question: "How Much Finance Can I Avail For My Bike?", answer: "If you can't afford the initial down payment, you can acquire a loan for up to 100% of the bike's value. The interest rates for two-wheeler loans begin at just 11.20%*. Your needs will determine the interest rate you select." },
            { question: "Is A 2 Wheeler Loan Secured Or Unsecured?", answer: "Two-wheeler loans are unsecured. Some lenders also provide secured loans, which often have lower interest rates." },
            { question: "Disclaimer:", answer: "Display of trademarks, trade names, logos, and other subject matters of Intellectual Property displayed on this website belongs to their respective intellectual property owners & is not owned by Bvalue Services Pvt. Ltd. Display of such Intellectual Property and related product information does not imply Bvalue Services Pvt. Ltd company’s partnership with the owner of the Intellectual Property or proprietor of such products.  Please read the Terms & Conditions carefully as deemed & proceed at your own discretion." },


        ],
        bankWiseLoanPresent: false,

    },

    // Electric Bike loan
    {
        name: "Electric Bike loan",
        path: "electricBikeLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        titleTagline: "A Sustainable and Convenient Way to Travel",
        description: ["Shop for an electric two-wheeler and ride home with it today! Get an easy EMI loan from TTK, starting at INR 20,000 and up to INR 2,00,000. Quick sanction and disbursement within day"],
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        benifits: [
            "Shop for an electric two-wheeler: This means that you can choose from a variety of electric two-wheelers, such as scooters, motorcycles, and bicycles.    ",
            "Ride home within a day: This means that you can get the loan approved and the money disbursed quickly, so you can take your new electric two-wheeler home right away.",
            "Easy EMI loan: This means that you can repay the loan in easy monthly installments. ",
            "Starting at INR 20,000 and up to INR 2,00,000: This means that the loan amount is flexible and can be customized to your needs.",
            "Quick sanction and disbursement within days:* This means that the loan application process is fast and efficient, and the money can be disbursed within days of approval.",
            "LOAN AGAINST SECURITIES: ",
            "Loans against securities are a way for individuals or businesses to access funds by using their investment portfolio as collateral. This means that the lender will hold onto your securities until the loan is repaid. The interest rate for loans against securities is typically lower than the interest rate for other types of loans, such as personal loans or credit cards. Here are some of the benefits of loans against securities:  ",
            "Convenient: You can apply for a loan against securities online Processing Quick: The loan process is typically quick and easy, and you can receive the funds within 24-48 hours. Secure: Your securities are held by the lender as collateral, so you are not at risk of losing them if you default on the loan.",


        ],



        price: "Up to ₹50 Lakh",


        interestRate: "9.50% p.a. onwards",
        eligibility: [
            "Individuals with a minimum monthly income of INR 10,000",
            "Indian citizens/residents who are 21 years and above",

        ],
        documents: [
            "PAN Card",
            "Salary Slips (Last 3 months)",
            "Salary Account Bank Statement  (3/6 months)",
            "Address Proof",



        ],

        applySteps: ["Step 1 -Apply online: You can apply for a loan online or through our website.", "Step 2 -Get approved: We will review your application and share with our Banking and NBFC partners who will process loans approval within days.", "Step 3 - If you application is approved, you will receive the funds in your bank account immediately from Financial Institution We understand that everyone's financial needs are different, so we offer a variety of loan options to choose from. We also offer flexible repayment terms so that you can choose a payment plan that fits your budget.   Our goal is to provide you with a fast, flexible, and friendly customer experience. We are here to help you every step of the way, from application to approval to disbursement.  *The actual processing time may vary depending on the loan amount and the borrower's credit score.  ",],

        loanHighlights: {
            InterestRate: "Starting from 9.99% p.a.",
            LoanAmount: "₹1 Lakh to ₹50 Lakh",
            Tenure: "12 - 60 months",
            ProcessingFees: " Up to 2% of the loan amount, plus applicable taxes"
        },
        typesOfLoan: [
            "New Electric Bike Loan Funds for purchasing a new electric bike. Terms, rates depend on lender's policies and creditworthiness.",
            "Used Electric Bike Loan For pre-owned electric bike. Amount based on bike's value.",
            "Secured Electric Bike Loan Requires bike as collateral. Default may lead to bike ownership transfer."
        ],
        tipsToGetLoan: "https://www.youtube.com/embed/zEx0I4zmjX8",
        dosAndDonts: {
            Dos: ["Do Research and Compare: Research various lenders offering electric bike loans. Compare interest rates, loan terms, and additional charges to find the most suitable option for your financial situation.", "Do Check Your Credit Score: Before applying for a loan, check your credit score. A higher credit score can often lead to better loan terms and interest rates. If your score needs improvement, consider taking steps to enhance it before applying.", "Do Determine Your Budget: Calculate the total cost of the electric bike, including the loan amount, interest, and other associated expenses. Ensure that your monthly budget can comfortably accommodate the loan repayment without straining your finances.", "Do Understand the Loan Terms: Thoroughly read and understand the terms and conditions of the loan. Pay close attention to the interest rate, repayment tenure, prepayment penalties, and any hidden fees.", "Do Consider Additional Costs: Apart from the loan itself, remember to consider ongoing costs such as maintenance, insurance, and charging expenses. Budgeting for these costs ensures you can manage the electric bike's ownership comfortably.",],
            Donts: ["Don't Overextend Yourself: Avoid borrowing more than you can afford to repay. Take into account your current income, expenses, and financial commitments before determining the loan amount.", "Don't Overlook Hidden Charges: Be wary of hidden fees, such as processing fees, documentation charges, or prepayment penalties. These can significantly impact the overall cost of the loan.", "Don't Rush Into a Decision: Take your time to evaluate different loan offers and lenders. Rushing into a decision can lead to signing up for a loan that might not be the best fit for your financial needs.", "Don't Ignore Prepayment Options: Check if the lender allows prepayment of the loan without substantial penalties. This can help you save on interest if you're able to pay off the loan earlier than the designated tenure.", "Don't Share Sensitive Information Unsecurely: When applying for a loan online, make sure you're using a secure website and not sharing sensitive information over unsecured networks. Protect your personal and financial data from potential fraud or identity theft.",]

        },
        repaymentOptions: {
            description: "Repayment options for an electric bike loan determine how you will pay back the borrowed amount and interest to the lender. These options can vary based on the lender's policies and your financial situation. Here's a description of common repayment options for an electric bike loan:",
            options: [
                {
                    option: "Equal Monthly Installments (EMIs):",
                    optionDescription: "This is one of the most common repayment methods. The total loan amount, along with interest, is divided into equal monthly installments over the loan tenure. The EMI remains constant throughout the repayment period, making it easier to budget."
                },
                {
                    option: "Step-Up or Step-Down EMIs: ",
                    optionDescription: " In a step-up EMI plan, the EMIs start lower and gradually increase over time. This is helpful if you expect your income to increase in the future. Conversely, step-down EMIs start higher and decrease over time, suitable for those who anticipate a reduction in income."
                },
                {
                    option: "Balloon Payment: ",
                    optionDescription: " This involves paying smaller EMIs during the loan tenure and a larger final payment (balloon payment) at the end of the loan term. This option is suitable if you plan to have a lump sum available at the end of the loan period."
                },
                {
                    option: "Flexible EMIs: ",
                    optionDescription: "Some lenders offer the flexibility to choose EMIs based on your financial situation. You might be able to adjust the EMI amount annually or at certain intervals, allowing you to accommodate changes in your income."
                },
                {
                    option: "Bullet Repayment:",
                    optionDescription: "Under this option, you pay a large portion of the loan amount in one lump sum at the end of the loan tenure. This can be suitable if you expect a significant windfall or have a plan to accumulate funds over time."
                },
                {
                    option: "No Cost EMI:",
                    optionDescription: "Some lenders collaborate with manufacturers or dealers to offer no cost EMIs. While the interest cost is absorbed, the bike's cost might be slightly higher. Ensure you understand the terms and evaluate the total cost before opting for this."
                },
                {
                    option: "Online and Mobile App Payments:",
                    optionDescription: "Lenders often provide online portals and mobile apps where you can make EMI payments conveniently using various payment methods."
                },

            ]
        },
        feeAndCharges: [
            { event: "Processing Fees:", charges: "This is a one-time fee charged by the lender to process your loan application. It covers administrative costs, verification, and documentation. Processing fees are usually a percentage of the loan amount or a flat fee." },
            { event: "Interest Charges:", charges: "Interest is the cost of borrowing money and is typically charged as a percentage of the loan amount. The interest rate can vary based on factors like the lender, your creditworthiness, and the prevailing market rates." },
            { event: "Prepayment Penalty:", charges: "Some lenders charge a penalty if you choose to repay the loan before the completion of the loan tenure. This fee is to compensate the lender for potential interest income that they might lose due to early repayment." },
            { event: "Late Payment Charges:", charges: "If you fail to make your EMI payment on time, the lender might impose late payment charges. It's important to make timely payments to avoid these fees and maintain a good credit history." },
            { event: "Documentation Charges:", charges: "These are fees associated with preparing the loan documents and agreements. They can vary from lender to lender." },
            { event: "Secure Collateral Valuation Fees:", charges: "If the loan is secured by collateral (such as other assets), the lender might charge fees for assessing the value of the collateral." },
            { event: "Non-Utilization Charges:", charges: "If you're approved for a loan but don't utilize the funds within a certain period, the lender might charge non-utilization fees." },
            { event: "Swap Charges:", charges: ": If you decide to change the terms of the loan, such as adjusting the EMI schedule or tenure, the lender might impose swap or modification charges." },

        ],
        collateral: ["No collateral required"],




        faq: [
            { question: "What is the maximum loan value that I can get for an Electric Bike Loan?", answer: "LoanTap offers up to 80% of on road vehicle price. Final loan amount decision is done on the basis of your eligibility and credit checks." },
            { question: "What is the documentation required for getting an Electric Bike Loan?", answer: "PAN, Address proof, 3 months’ pay slips, 6 month’s salary account bank statement, e-NACH mandate form. However, we might request for a few other documents if need arises during credit checks." },
            { question: "Do I need to hypothecate the vehicle in case of Electric Scooter Loan?", answer: "Yes, the vehicle purchased needs to be hypothecated to partner NBFC." },
            { question: "How do I get to know the list of approved distributors for Two wheeler Loan?", answer: "There is no exclusive list of dealers. You may apply for almost any bike belonging to any dealership-in locations where LoanTap is operating." },
            { question: "Is Electric Bike Loan available in my city?", answer: "Electric Scooter Loan is available in cities where LoanTap is operational. Currently we are providing our loans in Bangalore, Hyderabad, Chennai, Coimbatore, Mumbai, Pune, Ahmedabad, Vadodara, Raipur, Bhopal, Indore, Jaipur, Delhi-NCR, Ajmer, Lucknow, Mysore, Visakhapatnam, Kolkata, Howrah, Bhubaneshwar, Patna, Nagpur, Nashik, Rajkot, Surat and Chandigarh locations." },
            { question: "How much is the processing fee for availing the Electric Bike loan?", answer: "Processing fee for this loan is 2%+applicable taxes; on the gross loan amount sanctioned." },
            { question: "Can I make part payments/higher payments towards my loan outstanding? Any charges applicable?", answer: "Yes, you can make higher payments towards your loan. No charges for higher payments or foreclosure are applicable post 6 months. Within 6 months you can make part payments or foreclosure-with minimal charges applicable." },
            { question: "What is the repayment pattern for the Electric Bike loan?", answer: "In this loan, interest amount needs to be paid every month and in every 6 months a bullet amount needs to be paid towards the outstanding principal amount. Interest amount is revised post bullet payment of principal amount." },
            { question: "Can I make part payments/higher payments towards my loan outstanding? Any charges applicable?", answer: "Yes, you can make higher payments towards your loan. No charges for higher payments or foreclosure are applicable post 6 months. Within 6 months you can make part payments or foreclosure-with minimal charges applicable." },
            { question: "Do I have an option to pre-close the Electric Bike loan?", answer: "Yes, you can prepay the loan facility post 6 months of disbursement. No foreclosure charges are applicable post 6 months of loan completion, however within 6 months you can prepay your loan with minimal charges." },



        ],
        bankWiseLoanPresent: false,

    },
    // CAR LOANS/4 Wheeler Loans
    {
        name: "CAR LOANS/4 Wheeler Loans",
        path: "carLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        titleTagline: "Fulfilling Your Automotive Dreams with Our Tailored Car Loan Solutions!",
        description: ["TTKFinserv.com is a financial services platform that helps you compare and apply for car loans online. We have partnered with top banks and NBFCs to offer you the best possible interest rates and terms. You can get a car loan of up to ₹30 lakhs with a tenure of up to 7 years."],
        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        benifits: [
            "Minimal Paperwork Get your loan approved within minimum documention",
            "Low Interest Rates Starting at 11.99% p. a.",
            "No Collateral Get the financial assets without risk.",
            "Plenty of options Financing available for various car modles nad brands.",
            "Flexible Loan Amount Avail the loan amount you need to upscale your business.",
            "Flexible Repayments Get the loan amount your business scales for the type of project",


        ],



        price: "Up to ₹50 Lakh",



        interestRate: "9.50% p.a. onwards",
        eligibility: [
            "Type of car:	Used Car loan/ New car loan",
            "Residence:	Urban/Rural/Semi-rural",
            "Age: 18 yrs - 75 yrs",
            "Employment type:	Salaried/Self",
            "Salary: Depends on the bank",
        ],
        documents: [
            "To apply for a car loan, provide some basic documents as proof of identity and income. Hence, here’s a list of car loan documents required:",
            "Identity Proof: Passport, driver's license, Aadhaar card, Voter ID",
            "Address Proof: Utility bill (electricity, water, or gas), bank statement",
            "Income Proof: Recent payslips, Income Tax Returns (IT returns)",
            "Photographs: Recent passport-sized photographs",


        ],


        applySteps: ["Step 1 - Visit - TTKFinserv.com  and compare features.", "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.", "Step 3 - Fill out the eligibility form and provide:Email Addres Employment Type, Company Type, Industry Type, Current Company Name, Company Address, Year of Employment, Net Income, Mode of Salary, Pan Card Details,Full Name (As Per PAN Card), Current Address with PIN Code, Mobile Number", "Step 4 - Cross verify the given information and submit the application"],




        loanHighlights: {
            InterestRate: "	New Car:8.90% p.a. onwards,	Used Car:	9.40% p.a. onwards",
            LoanAmount: "New Car:Up to 100% of the ex-showroom price,Used Car: Up to 80% of the car’s valuation	",
            LoanProcessingFees: "New Car:Starting from 0.20% of the loan amount,	Used Car:	Up to 2% of the loan amount",
            Tenure: "New Car:Up to 7 years,	Used Car:	Up to 5 years",

        },

        typesOfLoan: [
            "New Car Loan Funds new car purchase, full cost covered. Fixed repayment period with interest.",
            "Used Car Loan Finances pre-owned vehicle. Terms vary based on car's age, condition.",
            "Pre-Approved Car Loan Pre-approved loan amount, set budget, negotiate effectively.",
            "Secured Car Loan Car collateral, lower rates. Default leads to repossession.",
            "Unsecured Car Loan No collateral, higher rates due to risk.",
            "Balloon Payment Car Loan Lower monthly payments, larger final payment. Ideal for short-term car ownership.",
            "Joint Car Loan Shared responsibility with spouse or family. Equally accountable for repayment.",
            "Low-Interest Car Loan Promotional lower interest rates for limited time, securing an affordable car loan."
        ],

        dosAndDonts: {
            Dos: ["Maintain a good credit score", "Pay off existing debts and reduce outstanding balances.", "Save for a down payment.", "Research and compare lenders and loan terms.", "Keep a stable employment history.", "Gather necessary documents in advance.", "Provide accurate and complete information."],
            Donts: ["Don't miss or make late payments on existing debts.", "Don't apply for multiple loans simultaneously.", "Don't exceed your budget for the car purchase.", "Don't provide false information on your loan application.", "Don't change jobs frequently before applying.", "	Don't ignore your credit report and credit history.", "Don't co-sign a loan for someone with a poor credit history."]

        },
        tipsToGetLoan: "https://www.youtube.com/embed/TYEAW1cpr-E",


        repaymentOptions: {
            description: "When considering a car loan to finance the purchase of your dream vehicle, it's important to understand the repayment options available. These options allow you to choose a repayment plan that suits your financial situation and preferences. Here are some common repayment options for car loans:",
            options: [
                {
                    option: "Repayment Options for Car Loans:",
                    optionDescription: "When considering a car loan to finance the purchase of your dream vehicle, it's important to understand the repayment options available. These options allow you to choose a repayment plan that suits your financial situation and preferences. Here are some common repayment options for car loans:"
                },
                {
                    option: "Equal Monthly Installments (EMIs):",
                    optionDescription: "EMIs are the most straightforward and widely used repayment method for car loans. The total loan amount, including principal and interest, is divided into equal monthly installments. Each EMI payment remains constant throughout the loan tenure, making it easier to budget and plan your finances."
                },
                {
                    option: "Balloon Payment:",
                    optionDescription: "A balloon payment option involves smaller monthly payments throughout the loan term and a larger lump sum payment, known as a balloon payment, at the end of the tenure. This option can be suitable if you expect a significant increase in income or have the means to make a larger payment in the future."
                },
                {
                    option: "Step-Up or Step-Down EMIs:",
                    optionDescription: "Step-up EMIs start with lower payments that gradually increase over time. This option is useful if you anticipate your income to rise in the future. Conversely, step-down EMIs start with higher payments that decrease over time, accommodating a potential decrease in income."
                },
                {
                    option: "Flexible EMIs:",
                    optionDescription: "Some lenders offer flexible EMI options that allow you to adjust the EMI amount based on changes in your financial situation. You might have the flexibility to increase or decrease the EMI amount at certain intervals, providing more control over your repayments."
                },
                {
                    option: "Prepayment and Foreclosure:",
                    optionDescription: "Many car loan agreements allow you to make prepayments or even repay the entire loan amount before the tenure ends. This can help you save on interest and pay off the loan faster. However, be aware of any prepayment penalties or charges associated with early repayment."
                },
                {
                    option: "Bullet Repayment:",
                    optionDescription: "In this option, you pay smaller EMIs during the loan tenure and a larger final payment (bullet payment) at the end. It can be suitable if you expect a lump sum of money at the end of the loan period."
                },

            ]
        },
        feeAndCharges: [
            { event: "Processing Fee", charges: "This is a one-time fee charged by the lender to process your loan application. It covers administrative costs related to evaluating your application." },
            { event: "Prepayment or Foreclosure Charges:", charges: "If you decide to repay the loan before the end of the loan tenure, some lenders may impose prepayment or foreclosure charges. These charges are intended to compensate the lender for potential interest income they might lose due to early repayment." },
            { event: "Interest Charges: ", charges: "The interest charged on the loan is a significant component of the total cost. The interest rate, which is often expressed as an annual percentage rate (APR), determines the cost of borrowing over the loan tenure." },
            { event: "Late Payment Penalty:", charges: " If you miss or delay an EMI payment, the lender may impose a late payment penalty. This is a fee for not adhering to the agreed-upon repayment schedule." },
            { event: "Documentation Charges:", charges: "These are fees related to the paperwork and documentation involved in processing the loan. They cover the costs of generating legal documents and agreements." },
            { event: "EMI Bounce Charges: ", charges: " If a check or electronic payment for your EMI bounces due to insufficient funds or other reasons, the lender may impose a bounce charge." },
            { event: "Valuation Fee: ", charges: "In the case of a used car loan, some lenders may charge a fee for assessing the value of the car being financed." },
            { event: "Insurance Charges: ", charges: "While not directly a fee, many lenders require you to purchase car insurance as a condition of the loan. You'll need to pay the insurance premium, which can vary based on the car's value and the coverage you choose." },
        ],
        collateral: ["No collateral required"],

        faq: [
            { question: "What Is A Car Loan?", answer: "Car loan is a type of financing that helps you purchase a vehicle by borrowing funds from a lender and repaying the loan over a specified period with interest." },
            { question: "What Types Of Cars Can Be Financed With A Car Loan?", answer: "Car loans can be used to finance a wide range of vehicles, including new cars, used cars, luxury cars, SUVs, and other passenger vehicles, subject to the eligibility criteria set by the lenders." },
            { question: "What Is The Difference Between A Secured And Unsecured Car Loan?", answer: "Secured car loan requires collateral, such as the vehicle itself, which can be seized by the lender in case of default, while an unsecured car loan does not require collateral but typically comes with higher interest rates." },
            { question: "How Do I Apply For A Car Loan?", answer: "To apply for a car loan, you can typically visit a bank or financial institution, submit the required documents, including proof of income, identification, and vehicle details, and complete the application process, which may involve a credit check and evaluation of your eligibility." },
            { question: "What Documents Do I Need To Provide When Applying For A Car Loan?", answer: "When applying for a car loan, you typically need to provide documents such as proof of identity, address, income, employment details, bank statements, vehicle-related documents, and in some cases, a copy of your driving licence." },
            { question: "What Is The Minimum Credit Score Required To Get A Car Loan?", answer: "The minimum credit score required to get a car loan varies among lenders, but generally, a credit score of 650 or higher is considered favourable for obtaining competitive interest rates and loan terms." },
            { question: "What Is The Maximum Loan Amount I Can Get For A Car Loan?", answer: "The maximum loan amount for a car loan depends on various factors such as the lender's policies, your creditworthiness, income, and the value of the vehicle, but it is typically capped at a certain percentage of the car's on-road price or its appraised value." },
            { question: "How Long Does It Take To Get Approved For A Car Loan?", answer: "The time it takes to get approved for a car loan can vary depending on factors such as the lender's processes, documentation requirements, and your creditworthiness, but it typically ranges from a few hours to a few days." },
            { question: "What Is The Typical Interest Rate For A Car Loan?", answer: "The typical interest rate for a car loan in India ranges from around 7% to 15%, but it can vary depending on factors such as the lender, loan amount, loan term, your creditworthiness, and prevailing market conditions." },
            { question: "What Is The Repayment Period For A Car Loan?", answer: "The repayment period for a car loan usually ranges from 3 years to 7 years, depending on the lender's terms and conditions and the borrower's preferences." },
            { question: "Can I Pay Off My Car Loan Early?", answer: "Yes, it is generally possible to pay off your car loan early, but it is advisable to check with your lender regarding any prepayment penalties or charges that may apply." },
            { question: "What Happens If I Default On My Car Loan?", answer: "If you default on your car loan, the lender may take legal action, repossess the vehicle, and sell it to recover the outstanding loan amount, potentially impacting your credit score and making it more difficult to secure loans in the future." },
            { question: "Can I Refinance My Car Loan?", answer: "Yes, you can refinance your car loan by obtaining a new loan with better terms, such as lower interest rates or extended loan repayment period, to replace your existing car loan." },
            { question: "What Is The Difference Between A Dealer Loan And A Bank Loan For A Car?", answer: "Dealer loan is provided by the car dealership directly, while a bank loan is obtained from a financial institution, with the key difference being the source of financing." },
            { question: "How Do I Calculate The Monthly Payment For A Car Loan?", answer: "You can use a car loan emi calculator to calculate your monthly payment for a car loan." },
            { question: "What Is The Highest Amount Of New Car Loan That I Can Avail?", answer: "The highest amount of a new car loan that you can avail depends on various factors such as the lender's policies, your creditworthiness, income, and the value of the car." },
            { question: "Disclaimer", answer: "Display of trademarks, tradenames, logos, and other subject matters of Intellectual Property displayed on this website belongs to their respective intellectual property owners & is not owned by Bvalue Services Pvt. Ltd. Display of such Intellectual Property and related product information does not imply, Bvalue Services Pvt. Ltd company’s partnership with the owner of the Intellectual Property or proprietor of such products. Please read the Terms & Conditions carefully as deemed & proceed at your discretion." },


        ],
        bankWiseLoanPresent: false,

    },

    // Merchant Loan
    {
        name: "Merchant Loan",
        path: "merchantLoan",
        imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
        titleTagline: "Manage Your Merchant with Ease",
        description: ["Short-term business loan with flexible financing options. Get up to 5 lakhs for tenures of 7 to 30 days. Easy application process and higher credit line."],





        add: ["Highly customized personal loan with lowest interest rate", "Very flexible repayment plan and tenure ", "Get maximum principal amount",],
        benifits: [
            " Highly convenient: This means that the loan process is easy and straightforward. There are no long waiting periods or complicated paperwork.",
            "Easy financing option: This means that there are multiple ways to finance the loan, such as through a line of credit or a term loan. This gives borrowers more flexibility to choose the option that best suits their needs.",
            "Short-term credit line: This means that the loan is for a fixed period of time, typically 7 to 30 days. This is ideal for businesses that need temporary financing to cover unexpected expenses or cash flow gaps.",
            "Up to 5 lakhs: This is the maximum amount that can be borrowed. This amount is sufficient for most small businesses.",
            "Tenures of 7 to 30 days: This is the range of repayment terms available. This gives borrowers the flexibility to choose the term that best suits their needs.",
            "Easy onboarding: This means that the application process is simple and quick. Borrowers can typically get approved for a loan within a few days.",
            "Higher credit line: This means that borrowers can borrow more money than they would with a traditional bank loan. This gives businesses more flexibility to grow and expand."


        ],



        price: "Up to ₹50 Lakh",



        interestRate: "9.50% p.a. onwards",
        eligibility: [
            "Business Type: All types of businesses, including sole proprietorships, partnerships, and corporations, are eligible for the merchant loan.",
            "Residence:	Urban/Rural/Semi-rural",
            "Age: Applicants aged between 18 and 75 years are eligible to apply for the merchant loan.",
            "Employment type:	Both salaried individuals and self-employed entrepreneurs are eligible to apply for the merchant loan.",
            "Salary: Depends on the bank",
        ],
        documents: [
            "Bank Statements: Recent business bank statements can offer insights into your business's cash flow, transactions, and financial activities.",
            "Identity Proof: To verify your residence, you might need to provide documents such as utility bills (electricity, water, gas), rental agreements, property deeds, or bank statements showing your address.",
            "Proof of Identity and Address: Government-issued photo ID (e.g., passport, driver's license) Proof of residential address (e.g., utility bill, lease agreement)",
            "Tax Returns: Recent business tax returns, including income tax returns, can demonstrate your business's financial performance and stability.",
            "Photographs: Recent passport-sized photographs for identification purposes.",


        ],


        applySteps: ["Step 1 - Research and Choose a Lender: Start by researching and identifying reputable lenders that offer merchant loans. Compare their loan terms, interest rates, fees, and eligibility criteria. Choose a lender that aligns with your business needs and financial situation.", "Step 2 - Gather Documentation: Collect all the necessary documentation to support your loan application. This may include business financial statements, tax returns, bank statements, proof of business address, business plan, and any other documents required by the lender. Ensure that the documents are accurate, up-to-date, and well-organized., the tenure months, and contact details.", "Step 3 - Preparation of Loan Proposal: Create a compelling loan proposal that outlines the purpose of the loan, how you plan to use the funds, and how the loan will benefit your business. Include financial projections, repayment plans, and any other relevant information that showcases the viability of your business and your ability to repay the loan.", "Step 4 - Submit Application:  Complete the lender's application form and submit it along with the required documentation. Many lenders offer online application options, which can streamline the process. Double-check the application and attachments for accuracy before submitting."],




        loanHighlights: {
            InterestRate: "The interest rates for merchant loans vary based on factors such as the lender, your business's financial health, and the loan terms. Rates typically range from [provide an estimated range based on your research]",
            LoanAmount: "Merchant loans can cover a range of loan amounts depending on your business's financials and the lender's policies. Loan amounts are determined after assessing your business's revenue, creditworthiness, and financial stability.	",
            LoanProcessingFees: "Loan processing fees for merchant loans are usually a percentage of the loan amount. They can vary based on the lender's policies and the specific loan terms. Fees might range from [provide an estimated range based on your research].",
            LoanTenure: "Merchant loan tenures are flexible and can be customized to match your business's needs. Loan tenures typically range from [provide an estimated range based on your research], giving you ample time to repay the loan.",

        },

        typesOfLoan: ["Term Loan: A term loan provides a lump sum amount upfront, which the borrower repays over a predetermined period (the 'term') with regular installment payments. This type of loan is suitable for funding larger expenses such as business expansion, equipment purchase, or working capital needs.", "Line of Credit: A business line of credit gives you access to a predefined credit limit that you can draw upon as needed. You only pay interest on the amount you borrow. It's a flexible financing option for managing short-term cash flow fluctuations and unexpected expenses.", "Equipment Financing: Equipment financing helps you purchase or lease equipment needed for your business operations. The equipment being financed often serves as collateral for the loan, making it an attractive option for businesses in need of specific machinery or assets.", "Invoice Financing or Factoring: Invoice financing involves selling your outstanding invoices to a lender at a discount in exchange for immediate cash. This helps improve cash flow while waiting for customers to pay their invoices.", "Merchant Cash Advance:   In a merchant cash advance, you receive a lump sum payment in exchange for a percentage of your daily credit card sales. Repayments are tied to your daily sales volume, making it suitable for businesses with fluctuating revenue.", "Working Capital Loan: A working capital loan is used to cover day-to-day operational expenses, manage seasonal fluctuations, or fund short-term projects. It's designed to bridge gaps in cash flow and maintain smooth business operations.", "SBA Loans (Small Business Administration Loans): SBA loans are government-backed loans offered through approved lenders. They come with favorable terms and are designed to support small businesses with various financing needs.",],

        dosAndDonts: {
            Dos: ["Research different lenders, loan types, and terms to find the best fit for your business needs. Compare interest rates, fees, repayment options, and customer reviews.", "Clearly define why you need the loan and how the funds will be used. This will help you choose the right type of loan and ensure you borrow an appropriate amount.", "Gather all the necessary documents, including financial statements, tax returns, and business plans, well in advance. Having these ready will expedite the application process.", "Check your personal and business credit scores before applying. If there are any discrepancies or errors, address them to ensure accurate information is presented to the lender.", "Consult with financial advisors, accountants, or legal experts to ensure you understand the terms, implications, and risks associated with the loan.", "If you receive multiple loan offers, compare them in detail. Look beyond the interest rate and consider all fees, repayment terms, and conditions before making a decision.", "Maintain open communication with the lender throughout the process. If you encounter difficulties or changes in circumstances, inform the lender promptly."],
            Donts: ["Avoid borrowing more than necessary. Borrowing excess funds can lead to unnecessary interest payments and financial strain.", "Pay close attention to any hidden fees, early repayment penalties, or other costs associated with the loan. Be clear about the total cost of borrowing.", "Take your time to complete the application accurately and thoroughly. Rushing can lead to mistakes that might delay the approval process.", "Your credit score plays a significant role in loan approval and interest rates. Don't neglect it; work to improve it if needed.", "Read all terms and conditions carefully. Don't rely solely on verbal promises. Understand the details before committing to a loan.", "	For startups and small businesses, a solid business plan demonstrates your ability to manage the loan and use the funds effectively. Don't overlook its importance."]

        },
        tipsToGetLoan: "https://www.youtube.com/embed/VktfAsPNaSQ",


        repaymentOptions: {
            description: "Repayment options for merchant loans can vary based on the type of loan, the lender's policies, and the specific terms of the loan agreement. Here are some common repayment options that businesses might encounter when taking out a merchant loan:",
            options: [
                {
                    option: "Equal Monthly Installments:",
                    optionDescription: "This is one of the most common repayment methods. Borrowers make fixed monthly payments that include both principal and interest. The advantage is that it provides predictability and helps with budgeting."
                },
                {
                    option: "Interest-Only Payments:",
                    optionDescription: "With this option, borrowers initially pay only the interest portion of the loan for a specific period, usually the first few months. Afterward, regular payments that include principal and interest begin. This can help manage initial cash flow challenges."
                },
                {
                    option: "Balloon Payment:",
                    optionDescription: "A balloon payment involves making smaller regular payments throughout the loan term and then a larger final payment (the 'balloon') at the end of the term. This option might be suitable for businesses that anticipate a large influx of cash in the future."
                },
                {
                    option: "Flexible Payments:",
                    optionDescription: "Some lenders offer flexibility in repayment, allowing businesses to adjust their payment amount or frequency based on cash flow. This can be helpful during seasonal fluctuations or unexpected circumstances."
                },
                {
                    option: "Revolving Line of Credit:",
                    optionDescription: "If you have a business line of credit, you can borrow and repay funds as needed within your credit limit. Repayments are typically based on the amount borrowed and the interest rate."
                },
                {
                    option: "Accelerated Repayment:",
                    optionDescription: "This involves making larger payments than required, which can help you repay the loan faster and save on interest costs over time."
                },
                {
                    option: "Early Repayment or Prepayment:",
                    optionDescription: "Some loans allow you to make extra payments or pay off the entire loan before the scheduled end date. However, be aware of any prepayment penalties or fees associated with early repayment."
                },

            ]
        },
        feeAndCharges: [
            { event: "Interest Rate:", charges: "This is the cost of borrowing money and is typically expressed as an annual percentage rate (APR). The interest rate varies based on factors like the type of loan, your creditworthiness, and prevailing market rates." },
            { event: "Loan Processing Fee:", charges: "Lenders often charge a processing fee to cover administrative costs related to processing your loan application. This fee can vary and might be a flat amount or a percentage of the loan amount." },
            { event: "Origination Fee: ", charges: "Similar to a processing fee, an origination fee covers the costs of processing and underwriting the loan. It's typically a one-time fee deducted from the loan amount." },
            { event: "Application Fee:", charges: " Some lenders charge an upfront fee when you submit your loan application. This fee covers the cost of reviewing your application, regardless of whether the loan is approved." },
            { event: "Underwriting Fee:", charges: "This fee covers the lender's cost of evaluating your creditworthiness and determining the terms of the loan. It can be a flat fee or a percentage of the loan amount." },
            { event: "Late Payment Fee: ", charges: " If you miss a loan payment or make a payment after the due date, a late payment fee might be charged. This fee encourages timely payments and helps cover administrative costs." },
            { event: "Prepayment Penalty: ", charges: "Some loans carry prepayment penalties if you pay off the loan before the agreed-upon term. This fee compensates the lender for potential lost interest income." },
            { event: "Annual Fee: ", charges: "For certain types of loans, especially lines of credit, there might be an annual fee for maintaining the credit facility. This fee can be charged regardless of whether you use the credit line." },
        ],

        collateral: ["No collateral required"],

        faq: [
            { question: "What is a merchant loan?", answer: "A merchant loan is a type of business loan specifically designed to provide financial assistance to businesses for various purposes such as expansion, working capital, equipment purchase, and more." },
            { question: "What types of businesses are eligible for a merchant loan?", answer: "Various types of businesses, including sole proprietorships, partnerships, corporations, and LLCs, can be eligible for merchant loans. Eligibility criteria may vary by lender." },
            { question: " What can a merchant loan be used for?", answer: "Merchant loans can be used for a wide range of business needs, including expanding operations, purchasing inventory or equipment, managing cash flow, launching marketing campaigns, and more." },
            { question: "How do I apply for a merchant loan?", answer: "To apply for a merchant loan, you typically need to fill out an application provided by the lender. You'll need to provide business and personal financial information, business plans, and other relevant documentation." },
            { question: "What documentation is required for a merchant loan?", answer: "Documentation requirements vary by lender, but commonly required documents include financial statements, tax returns, business plans, proof of business address, and identification." },
            { question: "How long does it take to get approved for a merchant loan?", answer: "Approval timelines vary, but some lenders offer quick approval processes, while others might take several weeks. Having all required documentation ready can expedite the process." },
            { question: " What factors affect the interest rate of a merchant loan?", answer: "Factors that can influence your interest rate include your business's creditworthiness, industry, loan amount, loan term, and prevailing market rates." },
            { question: " Can I repay a merchant loan early without penalty?", answer: "Some loans have prepayment penalties, while others allow early repayment without penalties. Review the loan agreement and discuss this with the lender before committing." },
            { question: "What happens if I miss a loan payment?", answer: "Missing a loan payment could result in late payment fees and damage to your credit score. Contact your lender immediately if you're facing financial difficulties to discuss potential solutions." },
            { question: "10. Can I apply for multiple merchant loans at the same time?", answer: "While you can apply to multiple lenders, doing so might negatively impact your credit score. It's generally advisable to research and apply to lenders that align with your business's needs." },
            { question: "What are the repayment options for a merchant loan?", answer: "Repayment options vary by lender and can include equal monthly installments, interest-only payments, balloon payments, and more. Choose the option that best suits your cash flow and financial strategy." },
            { question: "How does my credit score affect my merchant loan application?", answer: "Your credit score is an important factor in determining your eligibility and the interest rate you're offered. A higher credit score generally leads to better loan terms." },
            { question: " Can I use a merchant loan to start a new business?", answer: "Yes, some lenders offer startup loans to help entrepreneurs launch new businesses. However, eligibility criteria might be stricter, and interest rates could be higher." },
            { question: "What should I do before applying for a merchant loan?", answer: "Before applying, assess your business's financial needs, review your credit report, gather all required documents, and research potential lenders to find the best fit for your business" },
            { question: "How can I find a reputable lender for a merchant loan?", answer: "Research online, ask for recommendations from fellow business owners, and read reviews. Look for lenders with transparent terms, good customer service, and a solid reputation." },



        ],
        bankWiseLoanPresent: false,

    },

    {
        name: "Education Loan",
        path: "educationLoan",
        imageLink: "https://www.hdfccredila.com/images/new/banner-front.png",
        titleTagline: "Turn Your Academic Aspirations into Reality",
        tipsToGetLoan: "https://www.youtube.com/embed/FyLRxU5mJPM",

        description: "Discover the perfect education loan scheme from our list of top banks and NBFCs. With competitive student loan interest rates and flexible terms, make your distant dream a fulfilled reality.",
        benifits: [
            "Competitive interest rates",
            "Flexible repayment options",
            "Range of lending options",
            "Swift financial aid based on creditworthiness",
            "Equal opportunities for students from diverse backgrounds",
            "Tax benefits to enhance affordability",
            "Collateral-free Loans: for eligible candidates"
        ],
        interestRate: "9.50% p.a. onwards",
        eligibility: [
            "Indian nationality",
            "Age: 18-35 years",
            "Proven good academic record",
            "Must be pursuing a graduate/postgraduate degree or a PG diploma"
        ],
        applySteps: [
            "Step 1 - Visit TTKFinserv.com  and compare features.",
            "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.",
            "Step 3 - Fill out the eligibility form and provide:\n\n- Email Address\n- Employment Type\n- Company Type\n- Industry Type\n- Current Company Name\n- Company Address\n- Year of Employment\n- Net Income\n- Mode of Salary\n- Pan Card Details\n- Full Name (As Per Pan Card)\n- Current Address with Pin Code\n- Mobile Number",
            "Step 4 - Cross verify the given information and submit the application."
        ]
        ,
        documents: [
            "Filled & signed application form with affixed photographs",
            "Two photographs",
            "Mark sheets of 10th/12th or latest education certificate",
            "Statement of the cost of the course",
            "Age proof: Aadhaar Card, Voter ID, Passport, Driving Licence",
            "Identity proof: Voter ID, Aadhaar Card, Driving Licence, Passport",
            "Residence proof: Rental agreement, Bank statement, Gas Book, Electricity Bill, Tel Bill",
            "Income proof: Salary slips/Form 16, Bank statement, ITR of two years"
        ],
        feeAndCharges: [
            { event: "Processing Fee", charges: "1% to 2% of loan amount" },
            { event: "Foreclosure/Prepayment Charges", charges: "0% to 5% of outstanding loan amount" },
            { event: "EMI Bounce Charges", charges: "Rs. 200 to Rs. 500 per instance" },
            { event: "Overdue Charges on EMI", charges: "1% to 3% per month on overdue installment amount" },
            { event: "Legal Fee", charges: "0.1% to 1% of loan amount" }
        ],
        dosAndDonts: {
            Dos: [
                "Check your credit score before applying",
                "Research and compare different lenders",
                "Understand loan terms and conditions",
                "Prepare required documents",
                "Maintain a good credit score",
                "Demonstrate a clear academic and career plan",
                "Have a well-defined repayment strategy",
                "Provide all necessary supporting documents",
                "Seek a co-applicant or guarantor, if required"
            ],
            Donts: [
                "Overlook your credit score",
                "Rush into borrowing from the first lender",
                "Sign the loan agreement without understanding its terms",
                "Forget to gather and organize required documents",
                "Default on previous loan repayments",
                "Submit incomplete or inaccurate application documents",
                "Underestimate the impact of parents' credit history on loan eligibility",
                "Provide false information about your educational background",
                "Apply for an excessive loan amount",
                "Hide any existing financial obligations",
                "Apply with multiple lenders simultaneously"
            ]
        },

        repaymentOptions: {
            description: "Choose from various convenient methods for education loan repayment.",
            options: [
                {
                    option: "Electronic Clearing Service (ECS)",
                    optionDescription: "Automatic deduction of monthly installments from the borrower's bank account."
                },
                {
                    option: "Post-Dated Checks (PDCs)",
                    optionDescription: "Providing post-dated checks to the lender for monthly repayments."
                },
                {
                    option: "Online Fund Transfer",
                    optionDescription: "Initiating online transfers from the borrower's bank account to the lender's account."
                },
                {
                    option: "Mobile Banking",
                    optionDescription: "Utilizing mobile banking applications to make loan repayments."
                },
                {
                    option: "NEFT/RTGS",
                    optionDescription: "Transferring funds electronically using National Electronic Funds Transfer (NEFT) or Real-Time Gross Settlement (RTGS) systems."
                }
            ]
        },

        loanHighlights: {
            InterestRate: "9.50% p.a. onwards",
            LoanAmount: "Up to full course cost",
            Tenure: "Flexible repayment tenure",
            ProcessingFees: "1% to 2% of loan amount"
        },
        loanFeatures: [
            "Competitive student loan interest rates",
            "Flexible repayment terms",
            "Swift financial aid based on creditworthiness",
            "Wide range of lending options",
            "Tax benefits to enhance affordability",
            "Collateral-free Loans: for eligible candidates"
        ],
        typesOfLoan: [
            "Domestic Education Loan",
            "Study Abroad Education Loan",
            "Graduate Education Loans",
            "Undergraduate Education Loans",
            "Location-based Education Loans"
        ],
        taxBenefits: {
            Section80E_Deduction: "Interest paid eligible for tax deduction under Section 80E",
            Duration_of_Deduction: "Max 8 years or until interest is fully repaid",
            Interest_deduction_Only: "Deduction applies to interest only, not principal",
            No_Cap_on_Courses_or_Streams: "Available for all courses and streams",
            Documentation_Requirement: "Maintaining proper documentation is important"
        },
        faq: [
            { question: "How Can I Apply For An Education Loan Online?", answer: "Answer" },
            { question: "Can A Student Apply For An Education Loan Without A Co-Applicant?", answer: "Answer" },
            { question: "What Is The Repayment Period For Education Loans?", answer: "Answer" },
            { question: "How Can One Calculate EMI For An Education Loan Online?", answer: "Answer" },
            { question: "Does TTk Loan Have Any Pre-Payment Charges To Disburse An Education Loan?", answer: "Answer" },
            { question: "Why Should Anyone Choose TTk Loan Education Loan?", answer: "Answer" },
            { question: "How Can One Get An Approval On An Education Loan Online?", answer: "Answer" }
        ],

        bankWiseLoanPresent: true,


        bankWiseLoan: [
            { bankName: "HDFC Bank Education Loan", interestRate: "9.50% p.a. onwards" },
            { bankName: "ICICI Bank Education Loan", interestRate: "9.85% p.a. onwards" },
            { bankName: "IndusInd Bank Education Loan", interestRate: "9% p.a. onwards" },
            { bankName: "Kotak Education Loan", interestRate: "Up to 16% p.a." },
            { bankName: "Standard Chartered Education Loan", interestRate: "11% p.a. onwards" },
            { bankName: "Cent Education Loan (Central Bank of India)", interestRate: "8.55% p.a. onwards" },
            { bankName: "Bandhan Bank Education Loan", interestRate: "10.25% p.a. onwards" },
            { bankName: "SBI Education Loan", interestRate: "8.50% p.a. onwards" },
        ],




        LoanFromNBFCs: [
            { bankName: "Aditya Birla Education Loan", interestRate: "10.99% p.a. onwards" },
            { bankName: "Tata Capital", interestRate: "10.99% p.a. onwards" },
            { bankName: "Money View", interestRate: "1.33% per month onwards" },
            { bankName: "IIFL", interestRate: "13.50% p.a. onwards" },
            { bankName: "Bajaj Finserv Education Loan", interestRate: "10.10% p.a. onwards" },
        ]

    },
    {
        name: "Marriage Loan",
        path: "weddingLoan",
        imageLink: "https://www.herofincorp.com/images/loans/personal-loan/marriage-loan/wedding-loan.webp",
        titleTagline: "Manage Your Wedding Expenses with Ease",
        description: "A marriage loan is a convenient way to handle the financial aspects of your wedding, making it easier to cover the expenses involved in planning a grand celebration.",
        benefits: ["Easy application process", "Flexible loan amounts", "Affordable interest rates", "No collateral required", "Convenient repayment options"],
        price: "Up to ₹50 Lakh",
        interestRate: "Starting from 9.99% p.a.",
        documents: [
            "Two photographs: PassPort size photographs needed.",
            "Age proof: Aadhaar Card, Voter ID, Passport, Driving Licence",
            "Identity proof: Voter ID, Aadhaar Card, Driving Licence, Passport",
            "Income proof: Salary slips/Form 16, Bank statement, ITR of two years"],
        eligibility: ["Age: 21 - 65", "Monthly Income: ₹15,000 - ₹25,000", "Employment Type: Salaried or Self-employed", "Credit Score: 700-900"],
        applySteps: ["Step 1 - Visit ttkfinserv.com ", "Step 2 - Fill out the application form with required details", "Step 3 - Fill out the eligibility online form and provide necessary information", "Step 4 - Cross verify the given information and submit the application"],
        loanHighlights: {
            InterestRate: "Starting from 9.99% p.a.",
            LoanAmount: "₹1 Lakh to ₹50 Lakh",
            Tenure: "12 - 60 months",
            ProcessingFees: "Up to 2% plus applicable taxes"
        },
        typesOfLoan: ["Standard Marriage Loan", "Personal Loan for Wedding Expenses"],
        tipsToGetLoan: "https://www.youtube.com/embed/f2KMLLI4d_4",
        dosAndDonts: {
            Dos: ["Maintain a good credit score", "Pay off existing debts", "Have a stable source of income", "Save for a down payment"],
            Donts: ["Neglect your credit score", "Accumulate more debt", "Lack a stable source of income", "Apply for a loan without a down payment"]
        },
        repaymentOptions: {
            description: "Choose from different repayment options for your convenience.",
            options: [
                {
                    option: "Pre-payment",
                    optionDescription: "Make an early lump sum repayment to reduce the principal and interest burden."
                },
                {
                    option: "Part-payment",
                    optionDescription: "Make partial repayments towards the principal to reduce overall interest paid and shorten the loan tenure."
                }
            ]
        },
        feeAndCharges: [
            { event: "Processing Fee", charges: "Up to 2% plus applicable taxes" },
            { event: "Prepayment/Foreclosure Charges", charges: "0% if paid from own sources" }
        ],
        collateral: ["No collateral required"],
        faq: [
            { question: "Which Loan Is Better For Marriage?", answer: "A marriage loan is specifically designed to cover wedding expenses and offers convenient terms for this purpose." },
            { question: "Is Marriage Loan Tax-Free?", answer: "Interest paid on a marriage loan may not be tax-deductible. Consult a tax professional for detailed information." },
            { question: "Is It Okay To Take A Loan For Marriage?", answer: "Taking a marriage loan can be a practical way to manage wedding expenses, but make sure you assess your financial situation." },
            { question: "What Is The Age Limit For A Marriage Loan?", answer: "The age limit for a marriage loan is generally between 21 to 65 years." },
            { question: "Who Can Avail A Wedding Loan?", answer: "Both salaried and self-employed individuals meeting the eligibility criteria can avail a wedding loan." },
            { question: "Does My Credit Score Affect My Ability To Get A Marriage Loan?", answer: "Yes, a good credit score increases your chances of loan approval and may lead to better interest rates." },
            { question: "What Is The Difference Between A Marriage Loan And A Personal Loan?", answer: "A marriage loan is a type of personal loan tailored for covering wedding expenses." }
        ],
        bankWiseLoanPresent: true,

        bankWiseLoan: [
            {
                "bankName": "HDFC Bank",
                "interestRate": "10.50% onwards",
                "maximumLoanAmount": "Rs.40 lakh"
            },
            {
                "bankName": "SBI Personal Loan",
                "interestRate": "10.60% onwards",
                "maximumLoanAmount": "Rs.25,000 to Rs.20 lakh"
            },
            {
                "bankName": "SBI Personal Loan for Pensioners",
                "interestRate": "9.75% p.a. - 10.25% p.a.",
                "maximumLoanAmount": "Rs.25,000 - Rs.14 lakh"
            },
            {
                "bankName": "PNB Personal Loan",
                "interestRate": "8.7% p.a. to 14.25% p.a.",
                "maximumLoanAmount": "Rs.50,000 to Rs.10 lakh"
            },
            {
                "bankName": "Axis Bank Personal Loan",
                "interestRate": "12% p.a. to 21% p.a",
                "maximumLoanAmount": "Up to Rs.50,000 to Rs.15 lakh"
            },
            {
                "bankName": "Canara Bank Personal Loan",
                "interestRate": "12.40% p.a.",
                "maximumLoanAmount": "Rs.3 lakh"
            },
            {
                "bankName": "Canara Bank Budget Personal Loan",
                "interestRate": "11.30% p.a. to 12.30% p.a",
                "maximumLoanAmount": "Rs.3 lakh"
            },
            {
                "bankName": "IDBI Bank Personal Loan",
                "interestRate": "8.15% - 10.90% p.a.",
                "maximumLoanAmount": "Rs.25,000 to Rs.5 lakh"
            },
            {
                "bankName": "Indian Overseas Bank Personal Loan",
                "interestRate": "10.80% p.a.",
                "maximumLoanAmount": "Up to Rs.5 lakh"
            }
        ],
        LoanFromNBFCs: [
            {
                "bankName": "Muthoot Finance",
                "interestRate": "14% onwards",
                "processingFee": "Up to 3.5%"
            },
            {
                "bankName": "Tata Capital",
                "interestRate": "10.99% onwards",
                "processingFee": "Up to 2.75%"
            },
            {
                "bankName": "Bajaj Finserv",
                "interestRate": "11% onwards",
                "processingFee": "Up to 3.93%"
            },
            {
                "bankName": "StashFin",
                "interestRate": "11.99% onwards",
                "processingFee": "Up to 10%"
            },
            {
                "bankName": "Faircent",
                "interestRate": "9.99% onwards",
                "processingFee": "Up to 8%"
            },
            {
                "bankName": "KreditBee",
                "interestRate": "Up to 29.95%",
                "processingFee": "Up to 6%"
            },
            {
                "bankName": "Navi Finserv",
                "interestRate": "9.9% – 45%",
                "processingFee": "Nil"
            },
            {
                "bankName": "Money Tap",
                "interestRate": "13% onwards",
                "processingFee": "Nil"
            },
            {
                "bankName": "Dhani Loans",
                "interestRate": "13.99% onwards",
                "processingFee": "3% onwards"
            },
            {
                "bankName": "Money View",
                "interestRate": "15.96%",
                "processingFee": "Starting from 2%"
            },
            {
                "bankName": "Pay Sense",
                "interestRate": "16.80% - 27.60%",
                "processingFee": "Up to 2.5%"
            },
            {
                "bankName": "Fibe (Early Salary)",
                "interestRate": "18% onwards",
                "processingFee": "2%"
            },
            {
                "bankName": "Home Credit",
                "interestRate": "24% onwards",
                "processingFee": "Up to 5%"
            },
            {
                "bankName": "CASHe",
                "interestRate": "30% onwards",
                "processingFee": "Up to 3% or Rs 1,200"
            },
            {
                "bankName": "HDB Financial Services",
                "interestRate": "Up to 36%",
                "processingFee": "Up to 3%"
            },
            {
                "bankName": "Mahindra Finance Personal Loan",
                "interestRate": "Up to 26% p.a",
                "processingFee": "Up to Rs.3 lakh"
            }
        ]


    },
    {
        name: "Travel Loan",
        path: "travelLoan",
        imageLink: "https://www.herofincorp.com/images/loans/personal-loan/travel-loan/travel-loan-online.webp",
        titleTagline: "Turn Your Travel Dreams Into Reality",
        description: "Embark on your dream vacation effortlessly with TTk Loan! Discover the best travel Loans: in India with flexible repayment tenures and competitive interest rates.",
        benefits: ["Hassle-free loan process", "Flexible repayment tenures", "Interest rates as low as 11.99% p.a.", "No collateral required", "Quick loan disbursal"],
        price: "Up to your travel expenses",
        interestRate: "Starting from 11.99% p.a.",
        documents: [
            "Two photographs: PassPort size photographs needed.",
            "Age proof: Aadhaar Card, Voter ID, Passport, Driving Licence",
            "Identity proof: Voter ID, Aadhaar Card, Driving Licence, Passport",
            "Income proof: Salary slips/Form 16, Bank statement, ITR of two years"],
        eligibility: ["Age: 21 - 65", "Nationality: Indian", "Employment Type: Salaried or Self-employed", "Employment status: Employed at least 1 year or in business for at least 2 years"],
        applySteps: ["Step 1 - Visit ttkfinserv.com ", "Step 2 - Fill out the application form with required details", "Step 3 - Fill out the eligibility online form and provide necessary information", "Step 4 - Cross verify the given information and submit the application"],
        loanHighlights: {
            InterestRate: "Starting from 11.99% p.a.",
            LoanAmount: "Up to your travel expenses",
            Tenure: "Up to 5 years",
            ProcessingFees: "Up to 2% of the loan amount"
        },
        typesOfLoan: ["International Travel Loan", "Domestic Travel Loan"],
        tipsToGetLoan: "https://www.youtube.com/embed/gzgXzgUXWWw",
        dosAndDonts: {
            Dos: ["Maintain a good credit score", "Demonstrate a stable income", "Pay off existing debts", "Research and compare lenders"],
            Donts: ["Apply for multiple Loans: simultaneously", "Provide incomplete or inaccurate information", "Exceed your repayment capacity", "Neglect to check eligibility requirements"]
        },
        repaymentOptions: {
            description: "Choose from different modes of repayment for your convenience.",
            options: [
                {
                    option: "EMI (Equated Monthly Installment)",
                    optionDescription: "Repay the loan amount and interest in fixed monthly installments over the tenure."
                },
                {
                    option: "Online Payment",
                    optionDescription: "Make loan repayments through online platforms using net banking, debit cards, or mobile wallets."
                },
                {
                    option: "Cheque or Demand Draft",
                    optionDescription: "Repay the travel loan by issuing a cheque or demand draft in favor of the lender."
                }
            ]
        },
        feeAndCharges: [
            { event: "Processing Fee", charges: "Up to 2% of the loan amount" },
            { event: "Overdue EMI interest", charges: "2% p.m. on EMI or principal overdue" },
            { event: "Prepayment/Foreclosure Charges", charges: "0% if paid from own sources" },
            { event: "Late Payment Charges", charges: "2% per month" },
            { event: "Stamp Duty", charges: "As per applicable laws of the state" }
        ],
        collateral: ["No collateral required"],
        faq: [
            { question: "What Is A Travel Loan?", answer: "A travel loan is a type of personal loan that helps you finance your travel expenses." },
            { question: "What Are The Benefits Of A Travel Loan?", answer: "Travel Loans: provide funds for your vacation without depleting your savings." },
            { question: "How Can I Get Money For Traveling?", answer: "You can get money for traveling by applying for a travel loan from lenders like TTk Loan." },
            { question: "Can I Finance My Trip?", answer: "Yes, you can finance your trip through a travel loan." },
            { question: "Can You Get A Loan From The Bank For Travel?", answer: "Yes, many banks and NBFCs offer travel Loans: for your vacation expenses." },
            { question: "Can I Get A Travel Loan With No Credit?", answer: "A good credit score may improve your chances of getting a travel loan, but eligibility criteria vary." },
            { question: "How Can I Get EMI For Travel?", answer: "You can calculate the EMI for your travel loan using the EMI calculator provided by lenders." },
            { question: "How Can I Get A Travel Loan In India?", answer: "You can apply for a travel loan in India through TTk Loan's online application process." },
            { question: "Can You Get A Loan For Traveling Abroad?", answer: "Yes, you can get a travel loan for both domestic and international travel." }
        ],
        bankWiseLoanPresent: false,

    },
    {
        name: "Medical Loan",
        path: "medicalLoan",
        imageLink: "https://www.omozing.com/wp-content/uploads/2021/06/shutterstock_86451016-1.jpg",
        titleTagline: "Financial Support for Your Healthcare Needs",
        description: "Medical Loans: offer a way to cover healthcare expenses with competitive interest rates and flexible repayment options.",
        benefits: ["Quick and convenient application process", "Lower interest rates starting from 11.99% p.a.", "No collateral required", "Immediate access to healthcare services", "Flexible loan amounts and repayment plans"],
        price: "Up to your medical expenses",
        interestRate: "Starting from 11.99% p.a.",
        documents: ["Two photographs: PassPort size photographs needed.",
            "Age proof: Aadhaar Card, Voter ID, Passport, Driving Licence",
            "Identity proof: Voter ID, Aadhaar Card, Driving Licence, Passport",
            "Income proof: Salary slips/Form 16, Bank statement, ITR of two years", "Medical treatment/expenses documentation: Documents issued from respective medical firm."],
        eligibility: ["Nationality: Indian", "Age: 21 - 60", "Minimum Monthly Income: Rs 20,000", "CIBIL Score: 750 above", "Employment: Salaried or Self-employed"],
        applySteps: ["Step 1 - Visit ttkfinserv.com ", "Step 2 - Fill out the application form with required details", "Step 3 - Fill out the eligibility online form and provide necessary information", "Step 4 - Cross verify the given information and submit the application"],
        loanHighlights: {
            InterestRate: "Starting from 11.99% p.a.",
            LoanAmount: "Up to your medical expenses",
            Tenure: "Flexible tenure",
            ProcessingFees: "Up to 6% of the loan amount"
        },
        typesOfLoan: ["Emergency Medical Loan", "Medical Equipment Loan"],
        tipsToGetLoan: "https://www.youtube.com/embed/jNoC_zC63Ow",
        dosAndDonts: {
            Dos: ["Monitor and maintain a good credit score", "Research and compare lenders for competitive rates", "Provide accurate and complete information in the application form", "Double check all details before submission"],
            Donts: ["Apply for multiple Loans: at once", "Borrow more than you can afford", "Hide existing financial obligations", "Overlook alternative financing options such as insurance or government schemes"]
        },
        repaymentOptions: {
            description: "Choose from different modes of repayment for your convenience.",
            options: [
                {
                    option: "Prepayment",
                    optionDescription: "Pay the outstanding loan amount before the scheduled repayment tenure ends."
                },
                {
                    option: "Part Payment",
                    optionDescription: "Repay a small portion of the outstanding loan to reduce the balance along with EMI and tenure."
                }
            ]
        },
        feeAndCharges: [
            { event: "Interest Rate", charges: "8.90% to 25% p.a." },
            { event: "Processing Fees", charges: "1% to 6%" },
            { event: "Prepayment or Foreclosure Charges", charges: "2% to 5%" },
            { event: "Late Payment Charges", charges: "1% to 5% of EMI" },
            { event: "Other Charges", charges: "May include documentation charges, loan cancellation charges, or loan conversion charges." }
        ],
        collateral: ["No collateral required"],
        faq: [
            { question: "What Is A Medical Loan?", answer: "A medical loan is a type of personal loan used to cover healthcare expenses." },
            { question: "Who Is Eligible For A Medical Loan?", answer: "Indian citizens aged 21 - 60, with a minimum monthly income of Rs 20,000 and a CIBIL score of 750 above." },
            { question: "Can I Take A Loan For Medical Treatment?", answer: "Yes, medical Loans: can be used for various medical treatments and procedures." },
            { question: "What Is The Medical Loan Interest Rate?", answer: "Interest rates vary from 8.90% to 25% p.a. based on various factors." },
            { question: "How To Get An Instant Health Loan?", answer: "Follow the application process with TTk Loan for quick and convenient access to funds." },
            { question: "What Is The Medical Loan Policy?", answer: "Medical loan policies include eligibility criteria, interest rates, repayment options, and associated charges." }
        ],
        bankWiseLoanPresent: true,

        bankWiseLoan: [
            {
                "bankName": "HDFC Bank",
                "interestRate": "10.50% onwards"
            },
            {
                "bankName": "State Bank of India",
                "interestRate": "11.00% - 15%"
            },
            {
                "bankName": "ICICI Bank",
                "interestRate": "10.75% onwards"
            },
            {
                "bankName": "Axis Bank",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Kotak Mahindra Bank",
                "interestRate": "10.99% onwards"
            },
            {
                "bankName": "IndusInd Bank",
                "interestRate": "10.49% onwards"
            },
            {
                "bankName": "Bank of Baroda",
                "interestRate": "10.50% - 12.50%"
            },
            {
                "bankName": "Punjab National Bank",
                "interestRate": "8.90% - 14.45%"
            },
            {
                "bankName": "Canara Bank",
                "interestRate": "13% - 14.15%"
            }
        ],
        LoanFromNBFCs: [
            {
                "bankName": "Paysense",
                "interestRate": "16% - 36%",
                "processingFees": "Up to 3%"
            },
            {
                "bankName": "Bajaj Finserv",
                "interestRate": "13% onwards",
                "processingFees": "Up to 4.13%"
            },
            {
                "bankName": "Tata Capital",
                "interestRate": "10.99% - 19.75%",
                "processingFees": "Up to 3%"
            },
            {
                "bankName": "Indiabulls Dhani",
                "interestRate": "11.99% onwards",
                "processingFees": "Up to 3%"
            },
            {
                "bankName": "Fullerton India",
                "interestRate": "11.99%",
                "processingFees": "Up to 6.5%"
            },
            {
                "bankName": "Money Tap",
                "interestRate": "13% onwards",
                "processingFees": "2% on loan amount of Rs 25,000 above"
            },
            {
                "bankName": "Paisa Bazaar",
                "interestRate": "10.49%",
                "processingFees": "Up to 3%"
            }
        ]


    },
    {
        name: "Gold Loan",
        path: "goldLoan",
        imageLink: "https://akm-img-a-in.tosshub.com/businesstoday/images/story/202310/golgg-sixteen_nine.jpg?size=948:533",
        titleTagline: "Unlock the Power of Your Gold Assets",
        description: "TTk Loan offers hassle-free gold loan solutions with attractive interest rates starting from 11.99% p.a.",
        benefits: ["Quick approval within minutes", "Easy repayment options", "Flexibility for various purposes", "Lower interest rates starting from 11.99% p.a.", "Larger repayment tenure up to 60 months", "No prepayment penalties"],
        price: "Based on the value of your gold assets",
        interestRate: "Starting from 11.99% p.a.",
        documents: ["Identity Proof: Passport, driver's licence, Aadhaar card, Voter ID",
            "Address Proof: Utility bill (electricity, water, or gas), bank statement",
            "Income Proof: Recent payslips, Income Tax Returns (IT returns)",
        ],
        eligibility: ["Age: 18 to 75 years", "Profession: Salaried, Self-employed, Businessman", "Gold Articles: Gold jewellery, ornaments, coins", "Gold Purity: Minimum 18 carats & above", "Loan To Value (LTV): 90%"],
        applySteps: ["Step 1 - Visit ttkfinserv.com ", "Step 2 - Fill out the application form with required details", "Step 3 - Fill out the eligibility online form and provide necessary information", "Step 4 - Cross verify the given information and submit the application"],
        loanHighlights: {
            InterestRate: "Starting from 11.99% p.a.",
            LoanAmount: "Based on the value of your gold assets",
            Tenure: "Flexible tenure up to 60 months",
            ProcessingFees: "Processing fee of 0.5% of the loan amount"
        },
        typesOfLoan: ["Emergency Gold Loan", "Gold Ornaments Loan", "Gold Coins Loan"],
        tipsToGetLoan: "https://www.youtube.com/embed/B8vIl_2l2PM",
        dosAndDonts: {
            Dos: ["Check your credit score before applying", "Research and compare different lenders", "Understand loan terms and conditions", "Prepare required documents"],
            Donts: ["Overlook your credit score", "Rush into borrowing from the first lender", "Sign the loan agreement without understanding its terms", "Forget to gather and organize required documents"]
        },
        repaymentOptions: {
            description: "Choose from different modes of repayment for your convenience.",
            options: [
                { option: "Pre-payment", optionDescription: "Early repayment of a lump sum amount to reduce principal and interest burden" },
                { option: "Part-payment", optionDescription: "Partial repayments towards principal amount to lower overall interest paid and shorten tenure" }
            ]
        },
        feeAndCharges: [
            { event: "Processing Fee", charges: "0.5% of the loan amount" },
            { event: "Valuation Fee", charges: "₹500 for up to ₹2 lakh, ₹750 for loan amounts above ₹2 lakh" },
            { event: "Penal Charge", charges: "2% per month" },
            { event: "Foreclosure Charges", charges: "2% of Gold Loan amount if closed within 90 days of disbursal" },
            { event: "Stamp Duty Charges", charges: "As per state norms" },
            { event: "Late Payment Charges", charges: "24% per annum on overdue amount or ₹300, whichever is higher during tenure" }
        ],
        collateral: ["Gold jewellery", "Ornaments", "Coins"],
        faq: [
            { question: "Is Taking A Gold Loan Safe?", answer: "Yes, gold Loans: are secured by collateral and are considered safe." },
            { question: "Who Needs A Gold Loan?", answer: "Individuals in need of quick financial assistance can benefit from a gold loan." },
            { question: "Is A Gold Loan Easy To Get?", answer: "Yes, gold Loans: are relatively easier to get due to the collateral provided." },
            { question: "How To Check The Status Of A Personal Loan?", answer: "You can check the loan status through the lender's website or customer service." },
            { question: "Is PAN Card Required While Applying For Gold Loan?", answer: "Yes, PAN card is required for gold loan application." },
            { question: "Is Gold Loan Profitable?", answer: "Gold Loans: provide quick access to funds, but interest rates should be considered." },
            { question: "Which Bank Is Best For A Gold Loan?", answer: "Several banks offer gold loans, compare their terms and choose accordingly." },
            { question: "Which Is The Lowest Gold Loan?", answer: "Interest rates vary, so compare lenders to find the lowest gold loan." }
        ],
        bankWiseLoanPresent: false,

    }


]

export const insuranceData = [
    {
        "name": "TermLifeInsurance",
        "path": "termLifeInsurance",
        "Description": "Term Life insurance provides coverage for a fixed period of time at a fixed premium rate.In case of untimely death of the life insured during the policy term , the nominee of the life insured gets the Total Payout/Benefit. The benefit can be paid out as a lump sum payout or a combination of Lump sum & Monthly payout or only as a Monthly payout.Therefore Term insurance plans are said to be pure protection plans which ensure financial stability of the dependants in case of untimely death of the life insured.",
        "Benefits": [
            "Lump sum amount to take care of immediate financial liabilities.",
            "Monthly income to sustain the family lifestyle.",
            "Tax Benefit: The premiums paid for Term Life Insurance.",
            "Rider Benefits: Riders are an important addition.",
            "Accidental Death Benefit rider offers an additional sum assured.",
            "Accidental Disability rider offers an immediate lump sum payment.",
            "Critical Illness rider offers an additional sum assured.",
            "Waiver of Premium rider offers the waiver of all policy premiums.",
            "Option to increase Death benefit: Certain plans have offerings."
        ],
        "KeyTerms": [
            "Total Payout of each plan",
            "Premium amount paid for desired Total Payout",
            "Policy term offered",
            "High claim settlement ratio",
            "Riders offered with the plan"
        ],
        "Prices": [
            {
                "Description": "Starting price for a 1 crore term life insurance.",
                "Profile": "18 year-old Female, non-smoker.",
                "CoverDetails": "Cover up to 38 years of age"
            },
            {
                "Description": "Starting price for a 1 crore term life insurance.",
                "Profile": "18 year-old male, non-smoker.",
                "CoverDetails": "Cover up to 30 years of age"
            }
        ],
        "ReturnOfPremium": {
            "Description": "This plan provides an option for 'Return of Premium'.",
            "Note": "Full refund of the premium (excluding GST & premium paid for additional optional benefits)."
        },
        "ClaimSupport": {
            "Description": "We will respond in the first instance within 30 minutes.",
            "Note": "The 30-minute claim support is subject to our operations not being impacted by a system failure."
        },
        "SourceAndContact": {
            "Description": "Source: Google Review Rating available on.",
            "CompanyDetails": {
                "CompanyName": "Policybazaar Insurance Brokers Private Limited",
                "CIN": "U74999HR2014PTC053454",
                "RegisteredOffice": "Plot No.119, Sector - 44, Gurgaon, Haryana – 122001",
                "RegistrationNo": "742, Valid till 09/06/2024, License category- Direct Broker (Life & General)",
                "ContactDetails": {
                    "ClaimsSupport": "24x7 Claims Support Helpline can be reached out at 1800-258-5881"
                }
            },
        },
        "Disclaimer": { "Description": "Visitors are hereby informed that their information submitted on the website may be shared with insurers. Product information is authentic." }
    },
    {
        "name": "HealthInsurance",
        "path": "healthInsurance",
        "Description": "Health insurance takes care of your medical expenses and ensures that out-of-pocket expenses are curtailed up to the Sum insured",
        "Benefits": [
            "Health Coverage plans have enhanced offerings to cover a wide spectrum of requirements, like a family health plan offers complete cover to all members of a family under a single umbrella.",
            "Medical Bills: Coverage against medicinal expenses incurred, including pre and post hospitalization",
            "Pre-existing Diseases: Coverage for any pre-existing disease is provided to you after a certain waiting period.",
            "Claim Reimbursement: Coverage for expenses incurred for hospitalization due to a medical",
            "Tax Rebate: Annual premium paid for health coverage are subject to tax exemption u/s 80D of ITA, 1961. Tax exemption ranges from Rs. 25,000 to Rs. 75,000.",
            "Other Benefits: As an innovative feature, OPD expenses are now covered under few Insurer plans and don’t require hospitalization for minimum 24 hours for claim reimbursement. Standalone OPD plans are also available in the market."
        ],
        "KeyTerms": [
            "Sum Insured Amount",
            "Policy premium to be paid to avail the coverage benefits",
            "List of network hospitals and Claim Settlement Ratio",
            "Sub-limits (if any) and Waiting Period (for PEDs)",
            "Co-payment clause"
        ],
        "Prices": [
            {
                "Description": "Starting price for a 1 crore health insurance.",
                "Profile": "18-year-old male, with no pre-existing diseases.",
                "Note": "Discount on renewal premium is subject to the number of wellness points earned in the health insurance policy. For more details about the plans, please read the sale brochure carefully to get upto 100% discount on renewal premium."
            },
            {
                "Description": "Starting price for ₹ 5 lakh Health insurance.",
                "Profile": "30-year-old male & 29 years old female, living in Delhi with no pre-existing diseases"
            },
            {
                "Description": "Starting price for ₹ 10 lakh Health insurance.",
                "Profile": "30-year-old male & 29 years old female, living in Delhi with no pre-existing diseases"
            }
        ],
        "MedicalTestsNote": "No medical tests are required unless requested by the insurer’s underwriter. In-case of pre-existing diseases relevant medical proof would be required as per the terms and condition of the policy opted.",
        "CoverageDoubling": "Coverage up to double the amount of Sum Insured is available on certain covers for a minimum plan of Rs. 5 Lakh on the first claim only to an individual of upto 45 years of age with no pre-existing diseases. The benefit is available with or without extra cost depending on the plan chosen.",
        "SourceAndContact": {
            "Description": "Source: Google Review Rating available on.",
            "CompanyDetails": {
                "CompanyName": "Policybazaar Insurance Brokers Private Limited",
                "CIN": "U74999HR2014PTC053454",
                "RegisteredOffice": "Plot No.119, Sector - 44, Gurgaon, Haryana – 122001",
                "RegistrationNo": "742, Valid till 09/06/2024, License category- Direct Broker (Life & General)",
                "ContactDetails": {
                    "ClaimsSupport": "24x7 Claims Support Helpline can be reached out at 1800-258-5881"
                }
            },
            "Disclaimer": "Visitors are hereby informed that their information submitted on the website may be shared with insurers. Product information is authentic."
        }
    },
    {
        "name": "MutualFunds",
        "path": "mutualFunds",
        "Description": "Mutual funds provide a way for investors to pool their money.",
        "Benefits": [
            "Diversification: Mutual funds invest in a variety of assets.",
            "Professional Management: Fund managers are experts.",
            "Liquidity: Mutual funds are generally liquid investments.",
            "Affordability: Investors can start with a small amount.",
            "Transparency: Mutual funds provide regular reports.",
            "Flexibility: There are different types of mutual funds.",
            "Potential Returns: Depending on the fund's objective.",
            "Regulation: Mutual funds are regulated by.",
            "Convenience: Investors don't need to actively manage.",
            "Tax Benefits: Certain mutual funds offer tax benefits."
        ],
        "KeyTerms": [
            "Investment Objective: Define your financial goals.",
            "Risk Tolerance: Understand your risk tolerance.",
            "Investment Horizon: Determine how long you plan.",
            "Expense Ratio: Compare the expense ratios.",
            "Fund Performance: Analyze the historical performance.",
            "Exit Load: Check if there are any exit loads."
        ],
        "Disclaimer": {
            "Description": "Mutual fund investments are subject to market risks.",
            "Note": "All the information provided here is for educational and informational purposes only. It is not intended as investment, financial, or legal advice."
        },
        "SourceAndContact": {
            "Description": "Source: XYZ Financial Services",
            "ContactDetails": {
                "Website": "https://www.xyzfinancialservices.com",
                "Phone": "+123-456-7890",
                "Email": "info@xyzfinancialservices.com"
            }
        }
    }
];


// export default data;
