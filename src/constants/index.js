export const data = [
  {
    name: "Personal loan",
    path: "personalLoan",
    imageLink: "https://img.freepik.com/free-photo/this-is-your-booking-trip-young-man-planning-vacation-about-sign-purchase-agreement-with-travel-agent_662251-2163.jpg?w=996&t=st=1691737096~exp=1691737696~hmac=d018c93735eda16c9f6a7ba17d6f5efd486e74069158ddb30bfa828ec265d8ba",
    title: "Personal Loans For All Your Financial Needs",
    description:
      "Whether, you want to meet lifestyle goals or any personal exigency. If requirement is immediate, so should be the loan options. We have unique personal loan products to match your perfect requirement. Customized loan offering Higher loan amount and tenure Flexible repayment plans",
    benifits: [
      "Collateral-free loan",
      "No end-use restriction",
      "Interest rate usually starts from 10.49% p.a.",
      "Loan amount of up to Rs. 40 lakh, which can exceed depending on lenders’ discretion",
      "Repayment tenure up to 60 months, which may exceed on case-by-case basis",
      "Top up loans for additional requirements are offered by many lenders",
      "Minimal documentation",
      "Quick disbursals",
    ],
    documents: [
      "Identity Proof: Passport/PAN Card/ Voter’s ID/ Aadhaar Card/ Driving License",
      "Address Proof: Passport/ Aadhaar Card/ Lease/ Property Purchase Agreement/ Utility Bills (not more than 3 months old)/ Passport/ Driving License",
      "Income Proof For Salaried Individuals: Salary Slips/ Bank Account Statement/ Form 16",
      "Income Proof For Self-employed Professionals: Previous Years’ ITR/ P&L Statement and Balance Sheet/ Bank Account Statement",
      "Business Proof For Self-employed Individuals: Business Incorporation Certificate/ Professional Degree/ Certificate of Practice/ Partnership Deed/ GST Registration and Filing Documents/ MOA & AOA/ Shop Act License",
    ],

    typesofLoan: [
      "Instant Personal Loans are usually pre-approved loans with instant disbursals. These loans are usually offered by banks and NBFCs to their select customers on the basis of their credit profiles.",
      "Short-term Personal Loans have short repayment periods ranging from a few days to 12 months.",
      "Pre-approved Personal Loans are usually offered by banks and NBFCs to their existing customers on the basis of their credit history, income, employer’s profile, etc.",
      "Consumer Durable Loans can be used to purchase any consumer durable items like smartphones, furniture, microwave, etc. The purchase amount gets divided into EMIs and can be repaid within the tenure decided. Some products may require a down payment or a processing fee while others may not.",
      "Personal Loan Balance Transfer facility allows borrowers to transfer their outstanding personal loan to a new lender for lower interest rates or better loan terms. However, opt for this facility only when the savings made through the transfer outweighs the cost of the loan transfer.",
      "Personal Loan Top Up is offered to existing personal loan borrowers who need additional funds to meet their financial requirements. This loan facility is usually offered to select borrowers having satisfactory loan repayment history and/or have completed a specified number of EMIs.",
      "Personal Loan for Education is for individuals requiring funds for pursuing higher education in India and abroad, vocational courses, etc., and are unable to get a conventional education loan.",
    ],

    eligibility: [
      "Age: 18 – 60 years",
      "Salary: At least Rs 15,000 per month for salaried customers",
      "Income: At least Rs 5 lakh p.a. for self-employed customers",
      "Credit Score: Preferably 750 and above as having higher credit scores improve chances of loan approval",
      "Employment Stability: Usually 1 year with some lenders requiring longer work experience",
      "Business Continuity: Usually 3 years of business continuity with some lenders requiring longer period of business continuity",
      "Employment Type: Salaried employees working with reputed organisations, MNCs, Private and Public Limited Companies, Govt. organisations, PSUs, and large enterprises",

    ],
    applySteps: [
      "Enter your mobile number.",
      "Enter personal details like desired loan amount, employment type, net monthly income, current residing city, bank where you receive your salary and company name.",
      "Provide OTP for verification and check for available pre-approved offers.",
      "For viewing other offers, provide more details like total EMI you pay currently, PAN, total work experience, tenure in current organisation, etc.",
      "Compare various offers and apply for the one that best suits your requirements."


    ],
    price: "20 lahks",
    interestRate: "20%",
  },
  {
    name: "EMI loan",
    path: "emiLoan",
    imageLink: "https://img.freepik.com/free-photo/closeup-shot-person-thinking-buying-selling-house_181624-24672.jpg?w=996&t=st=1691737247~exp=1691737847~hmac=71e9528d8852bcc3e1e1e01419c012610b0274a878ba25c62bf94cbd275125de",
    title: "EMI Loans For All Your Financial Needs",
    description:
      "Enhanced PLoan: After taking a personal loan, wants the least impact on the salary outgo by Lower Monthly Outflow by 40%, Choose Interest-Only Payments, and Benefit from Bullet Principal Payments towards principal every 3/6 months. Lower monthly installments, Higher loan amount and tenure, Free part-payments",
    benifits: [
      "Lower interest rates than other types of personal loans",
      "Shorter repayment terms",
      "Easy application process",
      "Quick approval",
      "Flexible repayment options",
    ],
    documents: [
      "Proof of identity (passport, driver's license, or Aadhaar card)",
      "Proof of address (recent utility bill or bank statement)",
      "Proof of income (salary slips or tax returns)",
      "Credit report",
    ],
    price: "20 lahks",
    interestRate: "20%",
  },
  {
    name: "Home loan",
    path: "homeLoan",
    imageLink: "https://img.freepik.com/free-photo/front-view-house-investments-elements-composition_23-2148793807.jpg?w=996&t=st=1691737279~exp=1691737879~hmac=83863870cdf151853c272ab4c85afc877609ea33b688e45ff6de43da3b634190",
    title: "Home Loans For All Your Financial Needs",
    description:
      "Banks and Housing Finance Companies (HFCs) offer home loans up to 75-90% of the property's value, depending on the borrower's credit profile and subject to the lenders' and RBI's LTV caps. The loan term can be up to 30 years, depending on the borrower's age and repayment capacity. At TTKFinserv.com, we help you compare home loan interest rates and features from top banks and HFCs, and apply online for the best option for you.",
    benifits: [
      "House loan borrowers can avail tax benefits under various sections of the Income Tax Act. These home loan tax benefits help borrowers save a substantial amount of money every year. Below are the tax benefits that you can get on your home loan EMI payments:",
      "Section of Income Tax Act:Section 24(b), Nature of Home Loan Tax Deduction:Interest paid, 	Max. Tax Deductible Amt:	Rs. 2 lakh",
      "Section of Income Tax Act:Section 80C, Nature of Home Loan Tax Deduction:Principal (including stamp duty and registration fee), 	Max. Tax Deductible Amt:	Rs. 1.5 lakh",

    ],
    documents: [
      "Proof of Identity: Copy of any one (PAN Card, Passport, Aadhaar Card, Voter’s ID Card, and Driving License)",
      "Proof of Age: Copy of any one (Aadhaar Card, PAN Card, Passport, Birth Certificate, 10th Class Mark-sheet, Bank Passbook, and Driving License)",
      "Proof of Residence: Copy of anyone (Bank Passbook, Voter’s ID, Ration Card. Passport, Utility Bills (Telephone Bill, Electricity Bill, Water Bill, Gas Bill) and LIC Policy Receipt",
      "Proof of Income for Salaried: Copy of Form 16, latest payslips, IT returns (ITR) of past 3 years, and investment proofs (if any)Proof of Income for Self Employed: Details of ITR  of last 3 years, Balance Sheet and Profit & Loss Account Statement of the Company/Firm, Business License Details, and Proof of Business Address",
      "Property-related Documents: NOC from Society/Builder, detailed estimate of the cost of construction of the house, registered sale deed, allotment letter, and an approved copy of the building plan.",
      "Note: The above list is indicative and your lender might ask for additional documents.",
      "Also Check: The complete checklist of the home loan documents required",
    ],

    typesofLoan: [
      "Home Purchase Loan is offered for buying ready-to-move-in properties, under-construction properties and pre-owned homes/resale properties. helps you buy a residential plot and build a house on it within a given time frame",
      "Composite Loan can be availed for buying a plot and building a house on it. The first disbursement in composite loan is made towards plot purchase. Subsequent payments depend on the stages of construction of the house.",
      "Home Construction Loan is offered to individuals for house construction. The disbursements depend on the stages of construction of the house.",
      "Home Renovation/ Improvement Loan is for meeting home renovation costs of an existing house. The interest rate for a home renovation/improvement loan and a regular home loan are usually same.",
      "Home Extension Loan is for those requiring funds to extend or add more space to their existing house. In this, lenders usually lend 75% – 90% of the construction estimate, depending on the loan amount and LTV ratio.",
      "Bridge Loan is a short-term home loan suitable for individuals planning to buy a new house with the sale proceeds of their existing house.",
      "Interest Saver Loan is a home loan overdraft wherein the borrowers’ home loan account is linked to their bank account. Any amount deposited in the bank account over and above the EMI is considered as a prepayment towards the loan, thus, saving on the interest amount.",
      "Step Up Loan allows borrowers to pay lower EMIs during the initial years of the loan tenure and have the provision of increasing the EMI amount over time. This makes the loan affordable for young professionals who have just started their career.",

    ],
    eligibility: [
      "Home loan eligibility differs across lending institutions and loan schemes. However, a common set of housing loan eligibility criteria is given below:",
      "Nationality: Indian Residents, Non-Resident Indians (NRIs), and Persons of Indian Origin (PIOs)",
      "Credit Score: Preferably 750 and above",
      "Age Limit: 18 – 70 years",
      "Work Experience: At least 2 years (for salaried)",
      "Business Continuity: At least 3 years (for self-employed)",
      "Minimum Salary: At least Rs. 25,000 per month (varies across lenders & locations)",
      "Loan Amount: Up to 90% of property value",
      "Note: Apart from the above parameters, your home loan eligibility also depends on the property you are buying and the location of the property.",

    ],
    applySteps: [
      "Step 1- Share Your Details: Enter personal information as well as the details related to your loan requirements.",
      "Step 2- View Offers: As per the details shared, a list of eligible home loan offers will appear. Compare interest rate, processing fee, and eligible loan amount from the list of eligible home loan offers.",
      "Step 3- Submit the Application: Apply for the home loan offer that suits your loan requirements the best. Once your application is successfully submitted, you will get a confirmation of your home loan application along with a reference number for future reference. Next, our loan expert will get in touch within 24 hours to take this application forward.",


    ],
    price: "20 lahks",
    loanAmount: "Up to 75-90% of property's value",
    interestRate: "Varies based on lender and borrower's profile",
    loanTerm: "Up to 30 years, depending on borrower's age and capacity"

  },
  {
    name: "Business Loan",
    path: "businessLoan",
    imageLink: "https://img.freepik.com/free-photo/young-happy-couple-making-agreement-with-their-financial-advisor-home-men-are-shaking-hands_637285-3839.jpg?w=996&t=st=1691737319~exp=1691737919~hmac=dd003763bb3c083b9f1bf9f842c4213f74d3fc5b74545f7e037f17333fdec1bd",
    title: "Loans For All Your Financial Needs",
    description:
      "At TTK Finserv, we understand that every business needs capital to grow. Whether you are launching or expanding your business, we pledge to give you the best business loan offers. We have a wide range of business loan products to choose from, and we will work with you to find the perfect loan for your needs. We are committed to helping businesses succeed, and we believe that access to capital is essential for growth.",
    benifits: [
      "Business loans in India help fuel growth, offer flexibility, and aid cash flow management for businesses. Timely repayments enhance credit history and future financing options",
      "Minimal Paperwork Get your loan approved within minimum  documentaion.",
      "Low Interest Rates Starting at 8.90% p.a.",
      "No Collateral Get the financial assets  without risk.",
      "Start-ups and Women Lending privileges for women & assistance to start-ups",
      "Flexible Loan AmountAvail the loan amount you need to upscale your business",
      "Flexible Repayments Avail the repayment tenure that suits you the best.",

    ],
    documents: [
      "Here are some basic documents that you need to get while getting started with a Business Loan -",
      "Identity Proof: Passport, driver's licence, Aadhaar card, Voter ID",
      "Address Proof: Utility bill (electricity, water, or gas), bank statement",
      "Income Proof: Recent payslips, Income Tax Returns (IT returns)",
      "Photographs: Recent passport-sized photographs",

    ],


    eligibility: [
      "To get a business loan approved, you must meet a set of eligibility criteria. Refer to the table below to know whether you are eligible for a personal loan.",
      "Age: Min 21 - Max 65 (at the time of loan maturity)",
      "Business Vintage: 	Min 2 years or above",
      "Annual Turnover: 	As per banks",
      "Credit Score: 	750 or above",
      "Nationality: Indian",
    ],

    typesofloan: [
      "In India, several business loans are available to cater to different financial needs. Here are some common types of business loans offered:",
      "1.Term Loans - For expansion, purchasing assets, or working capital needs.",
      "2.Working Capital Loans - Working capital loans fund day-to-day operations and bridge cash flow gaps for expenses like inventory, payroll, and supplier payments.",
      "3.Machinery and Equipment Loans - Finance purchasing or leasing machinery, equipment, or vehicles necessary for business operations.",
      "4.Small Business Administration (SBA) Loans -Small business loan backed by the government.",
      "5.Business Line of Credit - Provides a revolving credit facility, allowing businesses to withdraw funds as needed.",
      "6.Invoice Financing - This loan type enables businesses to access funds by selling their unpaid invoices to a lender, also known as accounts receivable financing.",
      "7.Trade Finance - Trade finance loans facilitate international trade transactions by providing financing for import or export activities, including letters of credit, export financing, and supply chain financing.",
      "8.Startup Business Loan - Designed to support new businesses in their early stages.",
    ],

    applySteps: [
      "Step 1 - Visit - TTKFinserv.com or Download the TTK Finserv app and compare features.",
      "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.",
      "Step 3 - Fill out the eligibility form and provide: Email Address, Employment Type, Company Type,Industry Type, Current Company Name , Company Address,  Year of Employment,  Net Income,  Mode of Salary, Pan Card Details, Full Name (As Per PAN Card),  Current Address with PIN Code,  Mobile Number",
      "Step 4 - Cross verify the given information and submit the application",

    ],




    price: "20 lahks",
    loanAmount: "Varies based on business needs",
    interestRate: "Depends on lender and business factors",
    loanTerm: "Flexible repayment terms"
  },
  {
    name: "Two Wheeler loan",
    path: "twoWheelerLoan",
    imageLink: "https://img.freepik.com/free-photo/attractive-smiling-woman-riding-motorbike-street-summer-style-outfit-wearing-white-dress-red-hat-traveling-vacation-taking-pictures-vintage-photo-camera_285396-4567.jpg?w=996&t=st=1691737702~exp=1691738302~hmac=e4e84830e08d0c12a3d2fdb6722bd0c897d68580aa4fa25bbcb36efa0d98ddb6",
    title: "Two Wheeler Loans For All Your Financial Needs",
    description:
      "Two-wheeler loans are a type of personal loan that can be used to purchase a new or used motorcycle, scooter, or moped. They are typically offered by banks and non-banking financial companies (NBFCs), and they can be used to finance the entire cost of the vehicle or a portion of it.",
    benifits: [
      "Two-wheeler loans offer a number of benefits, including:",
      "Flexible repayment terms: Two-wheeler loans typically have longer repayment terms than other types of loans, such as car loans. This can make them more affordable for borrowers with limited income. ",
      "Competitive interest rates: Interest rates on two-wheeler loans are typically lower than interest rates on other types of loans, such as personal loans.",
      "Easy documentation: The documentation requirements for two-wheeler loans are typically less stringent than the documentation requirements for other types of loans. This can make it easier to qualify for a two-wheeler loan.",
      "If you are looking to purchase a new or used two-wheeler, a two-wheeler loan can be a great option. TTKFinserv can help you compare two-wheeler loan offers from a variety of lenders and find the best deal for you ",


    ],

    documents: [
      "Make sure to keep these documents in check and handy to avail an instant two-wheeler loan online with TTK Finserv",
      "Documents Required:",
      "Income certificate",
      "Age proof : Birth Certificate, Driving licence",
      "ID Proof :Aadhaar Card, PAN Card",




    ],

    eligibility: [
      "AGE: 18 - 65 years",
      "Minimum salary:₹10,000",
      "Employment duration: Minimum 1 Year",
      "Employment status: Salaried or self-employed",
      "Residence: Reside in the city for at least a year ",
    ],

    additionalThings: [
      "Here are some additional things to consider when taking out a two-wheeler loan:",
      "The down payment: Most lenders will require you to make a down payment on your two-wheeler loan. The amount of the down payment will vary depending on the lender and the type of vehicle you are financing. ",
      "The interest rate: The interest rate on your two-wheeler loan will affect the total amount of interest you pay over the life of the loan. Shop around and compare interest rates from different lenders before you choose a loan.",
      "The repayment term: The repayment term is the length of time you have to repay your two-wheeler loan. The longer the repayment term, the lower your monthly payments will be, but you will pay more interest over the life of the loa",
      "The fees: There are often fees associated with two-wheeler loans, such as application fees, processing fees, and late payment fees. Be sure to ask about all of the fees associated with a loan before you sign the paperwork.",
    ],

    applySteps: [
      "Here’s the step-by-step process that you can follow to apply for a two-wheeler loan on TTK Finserv:",
      "Step 1 - Visit - TTKFinserv.com or Download the TTK Finserv app and compare features.",
      "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.",
      "Step 3 - Fill out the eligibility form and provide: Email AddressEmployment Type, Company Type, Industry Type, Current Company Name, Address Proof, PAN Card Details, Full Name (As Per PAN Card), Current Address with PIN Code, Mobile Number, Income Certificate,  Driving License",
      "Step 4 - Cross verify the given information and submit the application.",
    ],
    price: "20 lahks", loanAmount: "Varies based on vehicle cost",
    interestRate: "Competitive rates, lower than personal loans",
    loanTerm: "Flexible repayment terms"
  },
  {
    name: "Electric Bike loan",
    path: "electricBikeLoan",
    imageLink: "https://img.freepik.com/free-photo/full-length-picture-happy-african-couple-rides-modern-motorbike-street_171337-11628.jpg?w=996&t=st=1691737773~exp=1691738373~hmac=5036be7861a0f61f21daa89adafc05abdf43e0effa74000a7f173ed4862451cd",
    title: "A Sustainable and Convenient Way to Travel",
    description:
      "Getting your own cost effective and environment friendly electric bike just got easier! Ebikes bring multiple benefits such as Low maintenance, Zero emission, Zero petrol consumption and Easy & Convenient Charging Options. Our Electric bike loans are pre-approved loans that help you fund your ebike purchase without any hassle. We give up to 80% value of the vehicle’s on-road price at affordable repayment tenures. Ride home your Favorite ebike today!",
    benifits: [
      "Electric bikes are becoming increasingly popular as a more sustainable and convenient way to travel. They offer a number of advantages over traditional petrol-powered bikes, including:",
      "Lower running costs: Electric bikes are much cheaper to run than petrol-powered bikes, as you don't need to pay for fuel.",
      "No emissions: Electric bikes produce zero emissions, making them a more environmentally friendly option.",
      "Quieter operation: Electric bikes are much quieter than petrol-powered bikes, making them ideal for use in urban areas.",
      "Easier maintenance: Electric bikes require less maintenance than petrol-powered bikes.",
      "If you're thinking of buying an electric bike, you may be wondering how to finance it. An electric bike loan can be a great way to get the money you need to purchase your new bike. TTK Finserv offers a variety of electric bike loans to suit your needs, with competitive interest rates and flexible repayment terms.",
      "To apply for an electric bike loan with TTK Finserv, simply fill out our online application form. We'll assess your eligibility and get back to you with a decision within 24 hours. If your application is approved, we'll transfer the funds to your lender within 48 hours.",
    ],
    documents: [
      "Basic Documents required for availing Electric Bike Loan with TTK Finserv",
      "PAN Card",
      "Salary Slips (Last 3 months)",
      "Salary Account Bank Statement  (3/6 months)",
      "Address Proof",
    ],
    eligibility: [
      "Basic Eligibility Criteria for availing Electric Bike Loans with TTK Finserv",
      "Individuals with a minimum monthly income of INR 30,000",
      "Indian citizens/residents who are 21 years and above",

    ],
    price: "20 lahks", loanAmount: "Varies based on electric bike cost",
    interestRate: "Competitive rates, tailored for electric bikes",
    loanTerm: "Flexible repayment terms"
  },
  {
    name: "Car Loans/4 Wheeler Loan",
    path: "carLoan",
    imageLink: "https://img.freepik.com/free-photo/young-family-buying-car-car-showroom_1303-16351.jpg?w=996&t=st=1691737860~exp=1691738460~hmac=2f0097d014e3249c3f31b86ef06af0afd943c299265cac61cdb6ba8a8f6057ad",
    title: "A Sustainable and Convenient Way to Travel",
    description:
      "TTKFinserv.com is a financial services platform that helps you compare and apply for car loans online. We have partnered with top banks and NBFCs to offer you the best possible interest rates and terms. You can get a car loan of up to ₹30 lakhs with a tenure of up to 7 years. Our process is simple and hassle-free, and you can get your loan approved in just a few days. Here are some key takeaways from the introduction content: TTKFinserv is a financial services platform that helps you compare and apply for car loans online, TTKFinserv has partnered with top banks and NBFCs to offer you the best possible interest rates and terms, You can get a car loan of up to ₹30 lakhs with a tenure of up to 7 years, TTKFinserv's process is simple and hassle-free, and you can get your loan approved in just a few days.",

    eligibility: [
      "Type of car: Used Car loan/ New car loan",
      "Residence: Urban/Rural/Semi-rural",
      "Age: 18 yrs - 75 yrs",
      "Employment type: Salaried/Self",
      "Salary: Depends on the bank",

    ],
    benifits: [
      "Car loans offer flexible amounts and tenure options, competitive rates based on credit history, quick approval, and financing for various car models, with convenient repayment through EMIs:",
      "Minimal Paperwor Get your loan approved within minimum documention",
      "Low Interest Rate Starting at 11.99% p. a.",
      "No Collateral Get the financial assets without risk.",
      "Plenty of option Financing available for various car modles nad brands.",
      "Flexible Loan Amount Avail the loan amount you need to upscale your business.",
      "Flexible Repayments Get the loan amount your business scales for the type of project",
    ],
    documents: [
      "To apply for a car loan, provide some basic documents as proof of identity and income. Hence, here’s a list of car loan documents required:",
      "Identity Proof: Passport, driver's license, Aadhaar card, Voter ID",
      "Address Proof: Utility bill (electricity, water, or gas), bank statement",
      "Income Proof: Recent payslips, Income Tax Returns (IT returns)",
      "Photographs: Recent passport-sized photographs",

    ],
    applySteps: [
      "Step 1 - Visit - TTKFinserv.com or Download the TTK Finserv app and compare features.",
      "Step 2 - Fill out the application form including the loan amount, the tenure months, and contact details.",
      "Step 3 - Fill out the eligibility online form and provide: Email Address Employment Type, Company Type, Industry Type, Current Company Name,  Company Address, Year of Employment, Net Income, Mode of Salary, Pan Card Details, Full Name (As Per PAN Card),  Current Address with PIN Code,  Mobile Number",
      "Step 4 - Cross verify the given information and submit the application.",

    ],

    CarLoanForeclosure: [
      "Car loan foreclosure allows early repayment of the outstanding amount, saving on interest and eliminating debt.",
      "Lenders may charge foreclosure fees, and a minimum lock-in period could apply.",
      "Contact the lender for specific requirements and documentation.",
      "Foreclosure can be done through a lump sum or increased monthly EMI.",
      "After foreclosure, the vehicle's hypothecation is released, establishing full ownership.",
      "Car loan foreclosure charges may be different depending on the bank.",
    ],
    CarLoanForeclosure: [
      "Car loan refinancing replaces an existing loan with a new one from a different lender, offering improved terms, lower rates, and potential savings on payments and interest. Evaluate options, compare lenders, and consider fees for a beneficial refinancing decision.",
      "Here are some reasons to refinance car loan",
      "Lower Interest Rates",
      "Monthly Payment Reduction",
      "Flexible Loan Terms",
      "Cash-out Option",
      "Multiple Lenders",
    ],
    price: "20 lahks", loanAmount: "Up to ₹30 lakhs",
    interestRate: "Varies based on lender and borrower's profile",
    loanTerm: "Up to 7 years"
  },



  {
    name: "Education Loan",
    path: "educationLoan",
    imageLink: "https://www.hdfccredila.com/images/new/banner-front.png",
    title: "Education Loans for Financing Your Studies",
    description: "Education loans are a great way to finance your studies, but they can be difficult to get approved for. TTKFinserv makes it easy to get the education loan you need, with no collateral and a quick approval process. We offer loans up to ₹20 lakhs with repayment terms of up to 10 years.A student education loan is a type of loan that is specifically designed to help students pay for their education. Student education loans can be used to pay for tuition, fees, books, and living expenses. TTKFinserv is a platform that allows you to apply for a student education loan online.",
    benefits: [
      "No collateral required",
      "Quick approval process",
      "Loan amount up to ₹20 lakhs",
      "Repayment terms of up to 15 years",
      "Competitive interest rates",
      "Flexible repayment options"
    ],
    documents: [
      "Proof of identity (Aadhaar card, PAN card, passport)",
      "Proof of address (electricity bill, water bill, bank statement)",
      "Income proof (salary slip, tax returns)",
      "Marksheets (for current and previous academic years)",
      "Letter of admission from the college/university"
    ],
    eligibility: [
      "Be a citizen of India",
      "Enrolled in a full-time undergraduate or postgraduate program at a recognized university or college",
      "Have a valid offer letter from the university or college",
      "Good academic record",
      "Co-signer (optional)"
    ],
    applySteps: [
      "Visit the TTKFinserv website or app.",
      "Provide basic information about yourself.",
      "Submit required documents including proof of income and identity.",
      "Wait for loan application approval, typically within 24 hours.",
      "Receive loan amount directly to your bank account."
    ],
    loanAmount: "Up to ₹20 lakhs",
    interestRate: "Starting from 10.99% p.a.",
    loanTerm: "Up to 15 years",
    questions: [
      {
        question: "What is the loan amount offered for education loans?",
        answer: "TTKFinserv offers student education loans up to ₹20 lakhs."
      },
      {
        question: "What is the loan tenure for education loans?",
        answer: "The loan tenure for student education loans from TTKFinserv can be up to 15 years."
      },
      {
        question: "What is the starting interest rate for education loans?",
        answer: "The interest rate on student education loans from TTKFinserv starts from 10.99% p.a."
      },
    ]
  },
  {
    name: "Wedding Loan",
    path: "weddingLoan",
    imageLink: "https://www.herofincorp.com/images/loans/personal-loan/marriage-loan/wedding-loan.webp",
    title: "Marriage Loans for Financing Your Dream Wedding",
    description: "A marriage loan is a type of personal loan that is specifically designed to help couples finance their wedding. Marriage loans typically have lower interest rates and shorter repayment terms than other types of personal loans, making them a more affordable option for couples who are looking to borrow money for their wedding.",
    benefits: [
      "Lower interest rates than other personal loans",
      "Shorter repayment terms",
      "Easy application process",
      "Quick approval",
      "Flexible repayment options"
    ],
    documents: [
      "Proof of identity (passport, driver's license, or Aadhaar card)",
      "Proof of address (recent utility bill or bank statement)",
      "Proof of income (salary slips or tax returns)",
      "Credit report"
    ],
    eligibility: [
      "Minimum age of 21 years",
      "Maximum age of 60 years",
      "Minimum monthly income of ₹15,000",
      "Good credit score"
    ],
    applySteps: [
      "Gather required documents.",
      "Apply online or through a bank/NBFC.",
      "Await loan approval.",
      "Receive funds within a few days."
    ],
    loanAmount: "₹10,000 to ₹15 lakhs",
    interestRate: "10.99% to 18.99%",
    loanTerm: "12 to 60 months",
    questions: [
      {
        question: "What is the loan amount range for wedding loans?",
        answer: "Wedding loans are available from ₹10,000 to ₹15 lakhs."
      },
      {
        question: "What is the minimum age requirement for wedding loan applicants?",
        answer: "Applicants must be at least 21 years old to be eligible."
      },
      {
        question: "What are the benefits of a marriage loan?",
        answer: "Benefits include lower interest rates, shorter repayment terms, easy application process, quick approval, and flexible repayment options."
      },
    ]
  },
  {
    name: "Travel Loan",
    path: "travelLoan",
    imageLink: "https://www.herofincorp.com/images/loans/personal-loan/travel-loan/travel-loan-online.webp",
    title: "Travel Loans for Fulfilling Your Vacation Dreams",
    description: "Planning a trip is exciting, but it can also be expensive. If you need help financing your travel, a travel loan can be a great option. Travel loans are designed to help you cover the cost of your trip, whether you're going on a once-in-a-lifetime vacation or a regular getaway.",
    benefits: [
      "Flexible repayment terms",
      "No collateral required",
      "Quick approval process"
    ],
    documents: [
      "Personal information (income, employment history, credit score)"
    ],
    applySteps: [
      "Apply with a lender online.",
      "Provide personal information and required documents.",
      "Await loan approval.",
      "Receive funds within a few days."
    ],
    loanAmount: "₹10,000 to ₹15 lakhs",
    interestRate: "10% to 20%",
    loanTerm: "3 to 5 years",
    questions: [
      {
        question: "What is the typical loan amount range for travel loans?",
        answer: "Travel loans typically range from ₹10,000 to ₹15 lakhs."
      },
      {
        question: "What are the benefits of a travel loan?",
        answer: "Benefits include flexible repayment terms, no collateral required, quick approval process, and the ability to cover travel expenses."
      },
      {
        question: "What is the typical repayment term for travel loans?",
        answer: "Travel loans typically have repayment terms of 3 to 5 years."
      },
    ]
  },
  {
    name: "Medical Loan",
    path: "medicalLoan",
    imageLink: "https://www.omozing.com/wp-content/uploads/2021/06/shutterstock_86451016-1.jpg",
    title: "Medical Loans for Unexpected Medical Expenses",
    description: "Medical emergencies can happen at any time, and they can be financially devastating. If you have to take out a medical loan to cover the costs of unexpected medical bills, you need to make sure you get the best possible deal.TTKFinserv is a leading online lender that offers medical loans to borrowers across India. We offer competitive interest rates and flexible repayment terms, so you can get the money you need when you need it. We also have a quick and easy application process, so you can get approved for a loan fast.",
    benefits: [
      "Competitive interest rates",
      "Flexible repayment terms",
      "Quick and easy application process"
    ],
    documents: [
      "Proof of identity (Aadhaar card, PAN card, passport)",
      "Proof of address (electricity bill, water bill, bank statement)",
      "Income proof"
    ],
    eligibility: [
      "Resident of India",
      "Employed with a monthly income of at least ₹25,000",
      "Good credit score"
    ],
    applySteps: [
      "Visit TTKFinserv website and fill out the online application form.",
      "Wait for application review and approval within 24 hours.",
      "Receive funds directly to your bank account within 2-3 business days."
    ],
    loanAmount: "Up to ₹50 lakhs",
    interestRate: "Starting from 10.99% p.a.",
    loanTerm: "Up to 60 months",
    questions: [
      {
        question: "What is the maximum loan amount offered for medical loans?",
        answer: "TTKFinserv offers medical loans of up to ₹50 lakhs."
      },
      {
        question: "What is the starting interest rate for medical loans?",
        answer: "Interest rates start at 10.99% p.a. for medical loans from TTKFinserv."
      },
      {
        question: "What are the eligibility criteria for medical loans?",
        answer: "Applicants must be Indian residents, employed with a monthly income of at least ₹25,000, and have a good credit score."
      },
    ]
  },
  {
    name: "Gold Loan",
    path: "goldLoan",
    imageLink: "https://www.TTKFinserv.com/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2018/06/Gold-Loan.jpg.webp",
    title: "Gold Loans for Quick Cash with Gold Collateral",
    description: "Gold loans are a type of secured loan that uses gold as collateral. This means that if you default on your loan payments, the lender can take your gold. Gold loans are a popular option for people who need quick cash, as they can be approved quickly and easily. TTK Finserv is a platform that allows you to apply for a gold loan online.",
    benefits: [
      "No collateral required",
      "Quick approval process",
      "Loan amount up to ₹50 lakhs",
      "Loan tenure of up to 36 months",
      "Competitive interest rates",
      "Flexible repayment options"
    ],
    documents: [
      "Proof of identity (Aadhaar card, PAN card, passport)",
      "Proof of address (electricity bill, water bill, bank statement)",
      "Gold valuation report"
    ],
    applySteps: [
      "Apply for a gold loan online through TTK Finserv.",
      "Provide required documents.",
      "Wait for loan approval.",
      "Receive funds within as little as 24 hours."
    ],
    loanAmount: "Up to ₹50 lakhs",
    interestRate: "Starting from 10.99% p.a.",
    loanTerm: "Up to 36 months",
    questions: [
      {
        question: "What is the loan amount range for wedding loans?",
        answer: "Wedding loans are available from ₹10,000 to ₹15 lakhs."
      },
      {
        question: "What is the minimum age requirement for wedding loan applicants?",
        answer: "Applicants must be at least 21 years old to be eligible."
      },
      {
        question: "What are the benefits of a marriage loan?",
        answer: "Benefits include lower interest rates, shorter repayment terms, easy application process, quick approval, and flexible repayment options."
      },
    ]

  },





  // personal loan

  {
    hdfcBank: [
      {
        bankName: 'HDFC Bank',
        interestRate: '	10.50% p.a. onwards',
        loanAmount: 'Up to Rs 40 lakh',
        tenure: 'Up to 6 years',

        minimumMonthlySalary: '	Rs 25,000',
        age: '21-60 years',
        creditScore: 'At least 650',
        processingFee: 'Up to Rs 4,999',

      }
    ]

  },
  {
    ICICIBank: [
      {
        bankName: ' ICICI Bank',
        interestRate: '	10.50% p.a. onwards',
        loanAmount: 'Up to Rs 50 lakh',
        tenure: '	1-6 years',

        minimumMonthlySalary: 'Rs 30,000',
        age: 'Salaried: 23-58 years , Self-employed: 25-65 years',
        creditScore: 'At least 650',
        processingFee: '	Up to 2.50% of loan amount',

      }
    ]



  },
  {
    axisBank: [
      {
        bankName: ' Axis Bank',
        interestRate: '		10.49% p.a. onwards',
        loanAmount: '	Rs 50,000- Rs 40 lakh',
        tenure: '	1-5 years',

        minimumMonthlySalary: 'Rs 15,000',
        age: '21-60 years',

        processingFee: '	1.5%-2%',

      }
    ]



  },
  {
    kotakMahindraBank: [
      {
        bankName: 'Kotak Mahindra Bank',
        interestRate: '	10.99% p.a. onwards',
        loanAmount: '		Up to Rs 40 lakh',
        tenure: '		Up to 6 years',
        creditScore: '	At least 700',
        minimumMonthlySalary: [
          "For Corporate Borrowers- Rs 25,000",
          "For Non-corporate Salary Borrowers- Rs 30,000",
          "Employee of Kotak Mahindra Bank- Rs 20,000",

        ],
        age: '21-60 years',

        processingFee: '	Up to 3% of loan amount',

      }
    ]



  },
  {
    indusIndBank: [
      {
        bankName: ' IndusInd Bank',
        interestRate: '	10.49% p.a. onwards',
        loanAmount: 'Rs 30,000-Rs 50 lakh',
        tenure: '	1 to 5 years',

        minimumMonthlySalary: 'Rs 25,000',
        age: 'Salaried: 21-60  years , Self-employed: 25-65 years',
        creditScore: 'At least 650',
        processingFee: 'Up to 3% of the sanctioned loan amount',

      }
    ]



  },
  {
    federalBank: [
      {
        bankName: 'Federal Bank',
        interestRate: '	11.49% p.a. onwards',
        loanAmount: '	Up to Rs 25 lakh',
        tenure: '	up to 5 years',

        minimumMonthlySalary: 'Rs 25,000',
        age: '60 years at the time of loan maturity',

        processingFee: '	Up to 3%',

      }
    ]



  },
  {
    idfcFirstBank: [
      {
        bankName: 'IDFC First Bank',
        interestRate: '		10.49% p.a. onwards',
        loanAmount: '	Up to Rs 1 crore',
        tenure: '	up to 5 years',


        age: 'Salaried: 21-60  years , Self-employed: 25-65 years',
        creditScore: 'At least 700',

        processingFee: '		Up to 3.5% of the loan amount',

      }
    ]



  },
  {
    bajajFinance: [
      {
        bankName: 'Bajaj Finance',
        interestRate: '		11% p.a. onwards',
        loanAmount: '	Up to Rs 50 lakh',
        tenure: '	Up to 8 years',

        creditScore: '	685 or higher',
        age: 'Salaried: 21-60  years , Self-employed: 25-65 years',

        minimumMonthlySalary: 'Rs 25,001',
        processingFee: '	Up to 3.93% of loan amount',

      }
    ]



  },
  {
    tataCapital: [
      {
        bankName: 'Tata Capital',
        interestRate: '		10.99% p.a. onwards',
        loanAmount: ['	Up to Rs 50 lakh',
          "Women- Rs 2 lakh onwards",
          "Others- Up to Rs 35 lakh",



        ],
        tenure: '		Up to 6 years',

        age: '21-58 years, For Home Renovation: 24-65 years, For Education: 16-26 years',
        creditScore: 'At least 700',
        minimumMonthlySalary: 'Rs 25,001',
        processingFee: '	Up to 3.93% of loan amount',

      }
    ]



  },
  {
    piramalFinance: [
      {
        bankName: 'Piramal Finance',

        interestRate: '12.99% p.a. onwards',
        loanAmount: 'Rs 1 lakh to Rs 10 lakh',
        loanTenure: 'Up to 5 years',
        processingFees: 'Up to 4% of total loan amount + applicable taxes (payable before loan disbursement)'
      }]
  },
  {

    landtFinance: [
      {
        bankName: 'L&T Finance',
        loanType: 'Consumer Loan',
        interestRate: 'Consumer Loan: 12% p.a. onwards, Micro Loan: 24% p.a.',
        loanAmount: 'Consumer Loan: Rs 50,000 to Rs 7 lakh, Micro Loan: Rs 35,000 to Rs 1.1 lakh',
        repaymentTenure: 'Consumer Loan: 1-4 years, Micro Loan: 2-3 years',
        processingFee: 'Consumer Loan: Up to 2% of principal outstanding plus applicable taxes, Micro Loan: 1% of loan amount plus applicable taxes'
      },
    ]
  },
  {
    DMIFinance: [
      {
        bankName: 'DMI Finance',

        interestRate: '12% p.a. onwards',
        maxInterestRate: '36% p.a.',
        loanAmount: 'Rs 1,000 to Rs 25 lakhs',
        repaymentTenure: '2 months to 5 years',
        processingFee: 'Up to 4% of the loan amount'
      }
    ],

  },
  {
    moneyTap: [
      {
        bankName: 'MoneyTap',
        tenure: "2 months to 3 years",
        interestRate: '1.08 % per month (12.96% p.a.) onwards',
        maxInterestRate: '36% p.a.',
        loanAmount: '	Rs 3,000 to Rs 5 lakh',

        age: '		23-55 years',
        minimumMonthlyIncome: 'Rs 30,000',
        processingFee: 'For loan amounts between:Rs 3,000 and Rs 5,000: Rs 199, Rs 5,000 and Rs 10,000: Rs 399,  Rs 10,000 and Rs 25,000: Rs 499, Rs 25,000 and above: 2% of the total amount'
      }
    ],

  },
  {
    moneyView: [
      {
        bankName: 'Money View',
        tenure: "	Up to 5 years",
        interestRate: '	1.33% per month onwards',

        loanAmount: 'Rs 5,000 to Rs 5 lakh',
        loanProcessingCharges: '2% – 8% of approved loan amount',
        age: '	21-57 years',
        minimumMonthlyIncome: 'For Salaried: Rs 13,500, For Self-employed: Rs 15,000',
        creditScore: 'CIBIL score of at least 600 or Experian score of at least 650',

      }
    ],

  },
  {
    adityaBirlaFinanceLtd: [
      {
        bankName: 'Aditya Birla Finance Ltd.',
        tenure: "		Up to 7 years",
        interestRate: '13% p.a. onwards',

        loanAmount: '		Up to Rs 50 lakh',

        age: '		23-60 years',

        processingFee: '	Up to 3% of the loan amount'
      }
    ],

  },
  {
    cashe: [
      {
        bankName: 'CASHe',
        tenure: "90 days to 540 days",
        interestRate: '1.08 % per month (12.96% p.a.) onwards',

        loanAmount: '	Rs 5,000 to Rs 5 lakh',

        age: '		23-58 years',

        processingFee: '	Up to 3% of the loan amount'
      }
    ],

  },
  {
    kreditbee: [
      {
        bankName: 'Kreditbee',
        tenure: "62 days to 2 years",
        interestRate: '	12.25% p.a. onwards',

        loanAmount: 'Rs 1,000 to Rs 4 lakh',
        minimumNetMonthlyIncome: 'Flexi Personal Loans: More than Rs 10,000, Personal Loans for Salaried: More than Rs 10,000',

        age: 'Flexi Personal Loan & Personal Loan for Salaried: 21-45 years, Online Purchase Loan/E-Voucher Loan: Above 21 years',

        processingFee: 'Flexi Personal Loan: Rs 85 to Rs 1,250,For Salaried (for loan amount from Rs 10,000 to Rs 2 lakh): Rs 500 to up to 6% of loan amount, Online Purchase Loan/E-Voucher Loan: Up to 5% of loan amount'
      }
    ],

  },
  {
    personalLoanInterestRates: [
      {
        institution: "Axis Bank",
        interestRate: "10.49% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "IndusInd Bank",
        interestRate: "10.49% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "IDFC First Bank",
        interestRate: "10.49% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "HDFC Bank",
        interestRate: "10.50% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "ICICI Bank",
        interestRate: "10.50% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Kotak Mahindra Bank",
        interestRate: "10.99% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Tata Capital",
        interestRate: "10.99% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Bajaj Finserv",
        interestRate: "11.00% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Federal Bank",
        interestRate: "11.49% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "DMI Finance",
        interestRate: "12% - 36%",
        applyLink: "Apply Now"
      },
      {
        institution: "L&T Finance",
        interestRate: "12.00% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Kreditbee",
        interestRate: "12.25% - 30%",
        applyLink: "Apply Now"
      },
      {
        institution: "MoneyTap",
        interestRate: "12.96% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Piramal Finance",
        interestRate: "12.99% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Aditya Birla",
        interestRate: "13% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "MoneyView",
        interestRate: "15.96% onwards",
        applyLink: "Apply Now"
      },
      {
        institution: "Cashe",
        interestRate: "27.00% onwards",
        applyLink: "Apply Now"
      }
    ]


  },
  {
    feesAndCharges: [
      {
        Particulars: 'Loan Processing Fees',
        Charges: "	0.5% to 4% of loan amount",

      },
      {
        Particulars: 'Pre-payment/Part-payment/Foreclosure Charges',
        Charges: "	For Floating Rate – Nil  For Fixed Rate – Usually around 2% – 5% on the principal outstanding",

      },
      {
        Particulars: 'Loan Cancellation',
        Charges: "		Usually around Rs 3,000",

      },
      {
        Particulars: 'Stamp Duty Charges',
        Charges: "	As per actuals",

      },
      {
        Particulars: 'Legal Fees',
        Charges: "	As per actuals",

      },
      {
        Particulars: 'Penal Charges',
        Charges: "	Usually @ 2% per month; 24% p.a.",

      },
      {
        Particulars: 'EMI/Cheque Bounce',
        Charges: "	Around Rs 400 per bounce",

      },


    ]


  },




  // HOmeLoan
  {

    loanOfferedbyTopBanks: [
      {
        name: 'State Bank of India',
        interestRate: '8.50% – 10.15%',
        applyLink: 'Apply Now'
      },
      {
        name: 'HDFC Bank',
        interestRate: '8.50% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'ICICI Bank',
        interestRate: '9.00% – 10.05%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Bank of Baroda',
        interestRate: '8.60% – 10.90%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Union Bank of India',
        interestRate: '8.50% – 10.95%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Axis Bank',
        interestRate: '9.00% – 13.30%',
        applyLink: ''
      },
      {
        name: 'Kotak Mahindra Bank',
        interestRate: '8.75% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'LIC Housing Finance',
        interestRate: '8.50% – 10.75%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Federal Bank',
        interestRate: '8.80% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'Bajaj Housing Finance',
        interestRate: '8.50% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'IDFC First Bank',
        interestRate: '8.85% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'PNB Housing Finance',
        interestRate: '8.75% – 14.50%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Tata Capital Housing Finance',
        interestRate: '8.70% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'L&T Finance Limited',
        interestRate: '8.60% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'Standard Chartered',
        interestRate: '8.75% onwards',
        applyLink: 'Apply Now'
      }
    ]
  },
  {

    offeredbyOtherBanks: [
      {
        name: 'Punjab National Bank',
        interestRate: '8.50% – 10.25%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Canara Bank',
        interestRate: '8.55% – 11.25%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Bank of India',
        interestRate: '8.50% – 10.75%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Indian Overseas Bank',
        interestRate: '8.85% onwards',
        applyLink: ''
      },
      {
        name: 'Bank of Maharashtra',
        interestRate: '8.60% – 11.05%',
        applyLink: 'Apply Now'
      },
      {
        name: 'UCO Bank',
        interestRate: '8.45% – 10.30%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Bandhan Bank',
        interestRate: '9.15% – 15.00%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Punjab & Sind Bank',
        interestRate: '8.85% – 9.95%',
        applyLink: 'Apply Now'
      },
      {
        name: 'South Indian Bank',
        interestRate: '9.57% – 11.42%',
        applyLink: 'Apply Now'
      },
      {
        name: 'RBL Bank',
        interestRate: '9.10% – 11.55%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Karnataka Bank',
        interestRate: '8.75% – 10.43%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Indiabulls Housing Finance',
        interestRate: '8.75% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'Karur Vysya Bank',
        interestRate: '9.23% – 10.73%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Dhanlaxmi Bank',
        interestRate: '9.35% – 10.50%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Tamilnad Mercantile Bank',
        interestRate: '9.45% – 9.95%',
        applyLink: 'Apply Now'
      },
      {
        name: 'Repco Home Finance',
        interestRate: '9.80% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'GIC Housing Finance',
        interestRate: '8.80% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'Aditya Birla Capital',
        interestRate: '8.80% – 14.75%',
        applyLink: 'Apply Now'
      },
      {
        name: 'ICICI Home Finance',
        interestRate: '9.20% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'Godrej Housing Finance',
        interestRate: '8.64% onwards',
        applyLink: 'Apply Now'
      },
      {
        name: 'HSBC Bank',
        interestRate: '8.60% onwards',
        applyLink: 'Apply Now'
      }
    ]
  },
  {
    relatedtoHomeLoan: {
      processingFee: '1% – 2% of loan amount',
      foreclosureChargesFloating: 'Nil (for floating rate)',
      foreclosureChargesFixed: 'Around 2% – 4% on the principal outstanding (for fixed rate)',
      overdueChargesEMI: '2% per month of the unpaid EMI',
      emiBounceCharges: 'Around Rs 400',
      legalFee: 'As per Actuals'
    }

  },




  // Business Loan
  {
    bankWiseBusinessLoan: [
      {
        name: 'State Bank of India(SBI)',
        interestRate: '	8.90% onwards',

      },
      {
        name: 'HDFC Bank',
        interestRate: '10.00% - 22.50% p.a.',

      },
      {
        name: 'ICICI Bank',
        interestRate: '12.50% - 13.60% p.a.',

      },
      {
        name: 'Axis Bank',
        interestRate: '	14.65% - 18.90% p.a.',

      },
    ]

  },
  {
    businessLoanFromNBFCs: [
      {
        name: 'Bajaj Finserv',
        interestRate: '		9.75% - 30% p.a.',

      },
      {
        name: 'IIFL Finance',
        interestRate: '	11.25% - 33.75% p.a.',

      },
      {
        name: 'FlexiLoans',
        interestRate: 'Starting at 12% p.a.',

      },
      {
        name: 'ZipLoan',
        interestRate: '	12% - 18% p.a. (Flat ROI)',

      },
    ]

  },
  {
    businessLoanInterestRatesCharges: [
      {
        desscr: "Business loan fees and charges are important to consider while you weigh your options on competitive interest rates. It impacts borrowing costs and affordability. Understanding their calculation and implications is crucial for borrowers. Rates can vary, affecting the overall borrowing cost.",
        order: [
          "Fixed - Remains constant throughout the loan term.",
          "Variable - Fluctuates based on market conditions, offering lower initial rates that increase over time.",
        ]


      },
      {

        features: 'Interest Rates',
        rate: '	10% - 21%',

      },
      {

        features: 'Loan Amount',
        rate: 'Rs. 50,000/- to Rs.50,00,000/-',

      },
      {

        features: 'Loan Processing Fees',
        rate: '	Rs 1000 - 75000/-',

      },
      {

        features: 'Loan Tenure',
        rate: '	Up to 10 years',

      },
      {

        features: 'Prepayment Charges',
        rate: 'Up to 4% on the balance',

      },

    ]

  },
  {
    governmentSchemes: [
      {
        pmmy: "Pradhan Mantri Mudra Yojana (PMMY)",
        description: "Provides financial assistance to small and micro enterprises through loans up to Rs. 10 lakh, promoting entrepreneurship and job creation."
      },
      {
        cgtmse: "Credit Guarantee Fund Scheme (CGTMSE)",
        description: "Provides credit guarantees to financial institutions to extend collateral-free loans to small and medium-sized enterprises (SMEs), facilitating their access to credit and promoting entrepreneurship."
      },
      {
        standup: "Stand-Up India Scheme",
        description: "Supports women and SC/ST entrepreneurs by providing bank loans between ₹10 lakh and ₹1 crore for setting up greenfield enterprises in various sectors."
      },
      {
        nsic: "National Small Industries Corporation (NSIC)",
        description: "Supports the growth of MSMEs in India through marketing, technology assistance, credit facilitation, and training programs."
      },
      {
        startup: "Startup India Scheme",
        description: "Fosters entrepreneurship by providing funding, tax benefits, and simplified regulations to support the growth of startups."
      },
      {
        mudra: "Make In India Soft Loan Fund (MUDRA)",
        description: "Provides financial assistance to small businesses in India, promoting entrepreneurship and sectoral growth."
      },
      {
        ecgc: "Export Credit Guarantee Corporation (ECGC)",
        description: "Provides export credit insurance services to exporters and banks, mitigating risks associated with exporting goods and services."
      },
      {
        smile: "SIDBI Make In India Loan For Enterprises (SMILE)",
        description: "Provides financial assistance and support to small and medium enterprises (SMEs) under the Make in India initiative."
      },
      {
        womenEnter: "Stand-Up India For Women Entrepreneurs",
        description: "Empowers women entrepreneurs by providing financial support and resources to start and grow their businesses."
      },
      {
        nabard: "National Bank For Agriculture And Rural Development (NABARD)",
        description: "Focuses on agricultural and rural development, providing credit, loans, and support to farmers, rural communities, and agricultural enterprises."
      }
    ]

  },






  // TWO WHEELER LOAN:
  {

    bikeLoanDetails: [
      {
        lender: "Axis Bank",
        interestRates: "10.80% - 28.30% p.a",
        processingFees: "Up to 2.5% of the loan amount"
      },
      {
        lender: "Bajaj Auto Finance",
        interestRates: "Starting at 8.50% p.a",
        processingFees: "3% of loan amount"
      },
      {
        lender: "Bank of India",
        interestRates: "6.85% - 8.55% p.a",
        processingFees: "Rs. 500 - Rs. 10,000"
      },
      {
        lender: "HDFC Bank",
        interestRates: "20.90% p.a",
        processingFees: "Up to 2.5% of the loan amount"
      },
      {
        lender: "Punjab National Bank",
        interestRates: "8.65% - 10.00% p.a",
        processingFees: "0.5% of the loan amount"
      },
      {
        lender: "State Bank of India",
        interestRates: "16.25% - 18.00% p.a",
        processingFees: "2% of the loan amount + GST"
      },
      {
        lender: "Union Bank of India",
        interestRates: "9.90% - 10.00% p.a",
        processingFees: "0.5% of the loan amount"
      },
      {
        lender: "L&T Finance",
        interestRates: "7.99% - 15.00% p.a",
        processingFees: "At L&T discretion"
      }
    ]







  },
  {
    twoWheelerLoanInterestRatesCharges: [
      {
        title: "Two-Wheeler Loan Interest Rates & Charges",
        desscr: "Financing your scooter loan has now become easier! Refer to the table below to learn everything about instant bike loans interest rates to loan tenure.",


      },
      {



        twowheelerloaninterestRates: 'Min 21 - Max 65 (at the time of loan maturity)',
        loanAmount: '	Up to ₹3 lakhs',
        loanProcessingFees: 'Up to 4% plus applicable taxes',
        loanTenure: '	Up to 20 months',
        repaymentTenure: '	Upto 3-5 Years (Depending on Bank)',


      },


    ]

  },
  {

    loanApplicationTips: {
      dos: [
        "Compare interest rates and terms.",
        "Check your credit score.",
        "Maintain a good repayment record.",
        "Opt for insurance coverage.",
        "Save for a down payment to reduce the loan amount and subsequent EMIs.",
        "Maintain a budget and allocate funds for timely loan repayments."
      ],
      donts: [
        "Don't borrow more than you can afford to repay.",
        "Don't provide false information or documents to the lender.",
        "Don't miss or delay loan repayments.",
        "Don't ignore the loan prepayment and foreclosure.",
        "Don't apply for multiple loans simultaneously.",
        "Don't make impulsive buying decisions beyond your repayment capacity."
      ]
    }




  },
  {

    loanApprovalTips: [
      "Check and improve your credit score: You can do this by paying your bills on time and clearing outstanding debts. A higher credit score enhances your loan eligibility.",
      "Save for a down payment: Having a substantial down payment reduces the loan amount and shows your commitment, making lenders more likely to approve your loan application.",
      "Compare lenders: Research and compare loan options from various lenders to find the best interest rates and terms that suit your financial situation.",
      "Maintain stable employment: A steady income and employment history increase your credibility as a borrower, boosting your chances of loan approval.",
      "Provide accurate information: Ensure all application details and supporting documents are accurate and up to date to avoid rejection.",
      "Minimize existing debt: Reduce your debt-to-income ratio by paying off existing loans or credit card debts, as this improves your overall financial profile.",
      "Consider a co-applicant: Adding a co-applicant with a strong credit profile can improve your chances of loan approval and potentially secure better loan terms.",
      "Demonstrate financial stability: Maintain a consistent financial record, including a regular savings pattern, to showcase your ability to handle loan repayments."
    ]
  },
  {
    reasonsTakeTwoWheelerLoan: [
      {
        title: "Reasons To Take A Two-Wheeler Loan",
        desscr: "Here are five reasons why you should consider taking a two-wheeler loan.",


        subContent: ["Purchasing the Bike You Desire:  If you need a convenient way to travel and are captivated by a bike with a stunning design and premium features, acquiring it through a bike loan can be an ideal solution.",
          "Tax Benefits and Savings:  By taking out a two-wheeler loan, you become eligible for tax benefits. Specifically, you can enjoy an income tax deduction on the interest paid towards the loan.",
          "Fostering Financial Discipline & Effective Financial Management: A sense of responsibility can help you become more mindful of your spending habits and make wiser choices, ultimately leading to better savings.",
          "Favourable Interest Rates: The demand for bikes continues to rise, prompting lenders to offer competitive interest rates on bike loans. Currently, it is possible to secure a loan with an interest rate as low as 10% per year",
          "Building a Strong Credit Profile:  Making timely EMI payments can enhance your credit score. Improving your creditworthiness opens doors for better loan options in the future, such as business or home loans.",


        ],



      },


    ]
  },
  {
    loanComparison: {
      personalLoan: {
        purpose: "Can be used for various personal expenses, such as for two-wheeler purchase, for car purchase, emergency loan, etc.",
        collateralRequirement: "Unsecured loans",
        loanAmount: "Typically, higher loan amounts.",
        interestRates: "Interest rates are generally higher.",
        repaymentFlexibility: "Flexible in terms of repayment."
      },
      twoWheelerLoan: {
        purpose: "Specifically designed to finance the purchase of a two-wheeler.",
        collateralRequirement: "Secured loans",
        loanAmount: "The loan amount is directly linked to the cost of the two-wheeler, generally covering a significant portion of the vehicle's on-road price.",
        interestRates: "Interest rates are comparatively lower.",
        repaymentFlexibility: "Fixed repayment tenures."
      }
    }
  },
  {
    twoWheelerLoanFromBanksNBFCs: [
      {
        title: "Two-Wheeler Loan From Banks & NBFCs",
        desscr: "Here are the bike loan interest rates offered by different Banks. Refer to the table below and compare to choose the right lender for you!",


      },
      {

        personalLoanbyBankNBFC: 'Axis Bank',
        interestRates: '10.80% - 28.30% p.a',
        processingFees: 'Up to 2.5% of the loan amount',

      },
      {

        personalLoanbyBankNBFC: 'Bajaj Auto Finance',
        interestRates: 'Starting at 8.50% p.a',
        processingFees: '	3% of loan amount',

      },
      {

        personalLoanbyBankNBFC: 'Bank of India',
        interestRates: '	6.85% - 8.55% p.a',
        processingFees: '	Rs. 500 - Rs. 10,000',

      },
      {

        personalLoanbyBankNBFC: 'HDFC Bank',
        interestRates: '	20.90% p.a',
        processingFees: 'Up to 2.5% of the loan amount',

      },
      {

        personalLoanbyBankNBFC: 'Punjab National Bank',
        interestRates: '	8.65% - 10.00% p.a',
        processingFees: '0.5% of the loan amount',

      },
      {

        personalLoanbyBankNBFC: 'State Bank of India',
        interestRates: '16.25% - 18.00% p.a',
        processingFees: '2% of the loan amount + GST',

      },
      {

        personalLoanbyBankNBFC: 'Union Bank of India',
        interestRates: '	9.90% - 10.00% p.a',
        processingFees: '	0.5% of the loan amount',

      },
      {

        personalLoanbyBankNBFC: 'L&T Finance',
        interestRates: '	7.99% - 15.00% p.a',
        processingFees: '	At L&T discretion',

      },


    ]

  },




  // Car Loan
  {
    carLoanInterestRates: [
      {
        title: "Best Car Loans From Banks",
        description: "Car loan interest rates can vary based on different factors and different banks. We curated a set of competitive interest rates that are available for you. G to the bank-wise interest rates, now.",
      },

      {
        bank: "State Bank of India(SBI)",
        interestRate: "8.60% p.a. onwards"
      },
      {
        bank: "HDFC Bank",
        interestRate: "7.90% - 11.01% p.a."
      },
      {
        bank: "ICICI Bank",
        interestRate: "10.00% onwards (12-35 months)"
      },
      {
        bank: "Axis Bank",
        interestRate: "8.95% - 13.80% p.a."
      },
      {
        bank: "Kotak Mahindra Bank",
        interestRate: "7.70% - 25% p.a."
      },
      {
        bank: "RBL Bank",
        interestRate: "12% - 14% p.a."
      },
      {
        bank: "Canara Bank",
        interestRate: "9.15% p.a. onwards"
      },
      {
        bank: "Federal Bank",
        interestRate: "11.00% p.a. onwards"
      },
      {
        bank: "Bank of Baroda",
        interestRate: "8.85% p.a. onwards"
      }
    ]
  },
  {

    carLoanDetails:
    {
      title: "Car Loan Interest Rates, Fees & Charges",
      description: "Car loan fees and charges are important considerations to settle. They include processing fees, documentation charges, prepayment penalties, and late payment fees. They prove to be of great benefit as it is crucial for effective budgeting. The rates below provided are a compilation of the various charges and rates that are applicable while applying for a car loan.",
    },

    newCar: {
      interestRate: "8.90% p.a. onwards",
      loanAmount: "Up to 100% of the ex-showroom price",
      loanProcessingFees: "Starting from 0.20% of the loan amount",
      loanTenure: "Up to 7 years",
      prepaymentCharges: "0.5% - 5% on the outstanding principal or interest for the remaining term of the loan, whichever is lower."
    },
    usedCar: {
      interestRate: "9.40% p.a. onwards",
      loanAmount: "Up to 80% of the car’s valuation",
      loanProcessingFees: "Up to 2% of the loan amount",
      loanTenure: "Up to 5 years",
      prepaymentCharges: "0.5% - 5% on the outstanding principal or interest for the remaining term of the loan, whichever is lower."
    },
    otherCharges: {
      title: "Other Charges  ",
      description: "Prepayment/Foreclosure Charges 0.5% - 5% on the outstanding principal or interest for the remaining term of the loan, whichever is lower."
    }


  },
  {

    carLoanApprovalTips: {
      title: "Improve Chances Of Getting Car Loan",
      description: "Besides meeting basic eligibility getting a car loan requires specific guidelines for approval. ",
      dos: [
        "Maintain a good credit score",
        "Pay off existing debts and reduce outstanding balances.",
        "Save for a down payment.",
        "Research and compare lenders and loan terms.",
        "Keep a stable employment history.",
        "Gather necessary documents in advance.",
        "Provide accurate and complete information."
      ],
      donts: [
        "Don't miss or make late payments on existing debts.",
        "Don't apply for multiple loans simultaneously.",
        "Don't exceed your budget for the car purchase.",
        "Don't provide false information on your loan application.",
        "Don't change jobs frequently before applying.",
        "Don't ignore your credit report and credit history.",
        "Don't co-sign a loan for someone with a poor credit history."
      ]
    }

  },
  {
    loanDifferences: {

      title: "Personal Loans For Car Vs. Car Loans",
      description: "Personal loans for purchasing a car and car loans serve differently. It is subjective to your purpose of availing a loan. Since personal loans are unsecured loans it is possible to to buy a car, while on the other hand car loans specifically cater to purchasing a vehicle. But there are other criteria that make them different, here are some of them-",
    },


    personalLoanForCar: {
      purpose: "Can be used for various personal expenses.",
      collateral: "Typically unsecured loans.",
      loanAmount: "Generally lower loan amounts.",
      interestRates: "Interest rates may be higher.",
      loanTerm: "Loan terms can vary from a few months to years.",
      repayment: "Fixed monthly installments.",
      flexibility: "Funds can be used for various car re-purposes.",
      eligibility: "Based on credit history, income, and other factors.",
      ownership: "No impact on ownership of assets."
    },
    carLoan: {
      purpose: "Specifically designed for purchasing a vehicle.",
      collateral: "The car itself acts as collateral.",
      loanAmount: "Higher loan amounts depending on the vehicle's value.",
      interestRates: "Lower interest rates due to the collateral.",
      loanTerm: "Loan terms are usually shorter, often up to 7 years.",
      repayment: "Fixed monthly installments.",
      flexibility: "Funds are dedicated to car purchase only.",
      eligibility: "Creditworthiness, income, and vehicle details are considered.",
      ownership: "Ownership of the car upon successful loan repayment."
    }
  },
]
