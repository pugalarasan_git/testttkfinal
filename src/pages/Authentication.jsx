import { SignIn } from "@clerk/clerk-react";
import React from "react";

const Authentication = () => {
  return (
    <div className="flex items-center justify-center h-screen w-screen">
      <SignIn />
    </div>
  );
};

export default Authentication;
