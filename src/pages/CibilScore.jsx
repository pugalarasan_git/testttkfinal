import React from 'react'

const CibilScore = () => {
    const credit_score_info = [
        {
            "question": "Why is your Credit Score important?",
            "answer": "Your credit score is important because it is one of the first things that lenders consider when evaluating your loan or credit card application. A good credit score increases the chances of your application being approved and can also lead to lower interest rates on loans."
        },
        {
            "question": "How is your Credit Score calculated?",
            "answer": "Your credit score is calculated based on factors like your loan repayment history, duration of credit history, number of hard inquiries, credit utilization, credit mix, and other factors. These factors reflect your past credit behavior and are reported to lenders every time you apply for credit."
        },
        {
            "question": "What is considered a good credit score?",
            "answer": "Most lenders consider a credit score of 750 and above as a good credit score. Having a higher credit score makes it easier to get loan or credit card approvals and access better offers."
        },
        {
            "question": "What are the benefits of maintaining a High Credit Score?",
            "answer": "Maintaining a high credit score comes with benefits such as greater chances of loan approval, lower interest rates, quick approval for credit applications, access to pre-approved loans, higher credit card limits, and discounts on fees."
        },
        {
            "question": "What are the reasons for a low credit score?",
            "answer": "A low credit score can result from missed or late payments, high credit utilization, errors in your credit report, frequent hard inquiries, closing old credit accounts, and settling accounts instead of paying in full."
        },
        {
            "question": "What should you do if your credit score is low?",
            "answer": "To improve your low credit score, you should start making timely payments, reduce credit utilization, rectify errors in your credit report, avoid frequent credit applications, maintain old credit accounts, and seek expert advice if needed."
        },
        {
            "question": "Why check your CIBIL Score from TTKFinserv?",
            "answer": "TTKFinserv offers free access to your credit score and provides the ability to check and compare scores from multiple credit bureaus. It also offers customized loan and credit card offers based on your credit profile and provides credit advisory services to help improve your score."
        }
    ]
    const additional_credit_info = [
        {
            "question": "Why does my CIBIL score keep changing?",
            "answer": "Your CIBIL score can change due to various factors, including changes in credit behavior like late payments, new loans, credit utilization ratio, and more. Even small changes can impact your score."
        },
        {
            "question": "I have a high salary and low credit utilization ratio but I was denied the loan. Why?",
            "answer": "While salary and credit utilization are important, lenders primarily consider your credit score. If your credit score is low, it can lead to loan application rejections even if other aspects are strong."
        },
        {
            "question": "I have an 800+ credit score and low credit utilization, but I was denied a new credit card. Was this because I took a personal loan last month?",
            "answer": "Yes, applying for credit frequently can be seen as credit-hungry behavior, which might lead to rejections even with a high credit score. Lenders look at overall credit behavior, not just the score."
        },
        {
            "question": "What is the minimum CIBIL score to get a personal loan?",
            "answer": "The minimum CIBIL score required varies among lenders, but generally a score of 750 or above is preferred for personal loan approval. Some lenders might consider scores around 700 as well."
        },
        {
            "question": "Can everyone access my CIBIL Score?",
            "answer": "No, your CIBIL score is confidential. Only authorized entities like banks and financial institutions can access your credit score when you apply for credit. You can also access it for free."
        },
        {
            "question": "How often can I get an updated credit score?",
            "answer": "You can access your credit score anytime, but significant changes in your credit behavior might take about 30-45 days to reflect in your updated score due to reporting cycles."
        },
        {
            "question": "What happens if my CIBIL Report has errors?",
            "answer": "If you find errors in your CIBIL report, you can dispute them through CIBIL's Consumer Dispute Resolution procedure. Errors can impact your credit score, so it's important to get them corrected."
        },
        {
            "question": "What is the difference between credit rating and credit report?",
            "answer": "A credit report is for individuals and includes your credit history and score from bureaus like CIBIL. Credit rating is for companies and is provided by agencies like CRISIL, ICRA, etc."
        },
        {
            "question": "How to download CIBIL Report?",
            "answer": "You can download your CIBIL report from TTKFinserv's dashboard when you check your credit score. Your CIBIL score report acts as a detailed credit report, and you can use your birthdate as the password."
        },
        {
            "question": "Can I check my Credit Score through Aadhar Card?",
            "answer": "While you can provide Aadhar Card as an ID proof, you still need to provide your PAN card to check your credit score. Your credit score is linked to your PAN."
        },
        {
            "question": "How can I check my credit score for free through my mobile number?",
            "answer": "You can check your credit score for free through TTKFinserv.com by providing your mobile number. You'll receive an OTP for verification and access your score, credit report, and pre-approved offers."
        }
    ]
    
    
  return (
    <div className='w-screen min-h-screen max-h-full py-20 flex flex-col '>
        
    </div>
  )
}

export default CibilScore